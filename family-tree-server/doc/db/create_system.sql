USE family_tree;

DROP TABLE IF EXISTS `SYS_USER`;
CREATE TABLE `SYS_USER` (
  `ID` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
  `USERNAME` VARCHAR(50) NOT NULL COMMENT '用户名',
  `PASSWORD` VARCHAR(200) NOT NULL COMMENT '密码',
  `NICKNAME` VARCHAR(50) NOT NULL COMMENT '昵称',
  `IS_SYSTEM` TINYINT(1) NOT NULL COMMENT '是否系统账号',

  `IS_DELETED` TINYINT(1) NOT NULL COMMENT '是否删除',
  `REMARKS` VARCHAR(255) DEFAULT NULL COMMENT '备注信息',
  `CREATE_BY` INT UNSIGNED DEFAULT NULL COMMENT '创建者',
  `CREATE_DATE` DATETIME DEFAULT NULL COMMENT '创建时间',
  `UPDATE_BY` INT UNSIGNED DEFAULT NULL COMMENT '更新者',
  `UPDATE_DATE` DATETIME DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`ID`)
) COMMENT '后台用户'
  ENGINE = InnoDB
  DEFAULT CHARSET= UTF8;

INSERT INTO `SYS_USER` (ID, USERNAME, PASSWORD, NICKNAME, IS_SYSTEM, IS_DELETED) VALUES
  (1, 'admin', '9cbf1b0c4fdb935ecd47f32429f8c45e5cc20264053237b3e8b08a71', '超级管理员', 1, 0);

/**
 */
DROP TABLE IF EXISTS `SYS_MENU`;
CREATE TABLE `SYS_MENU` (
  `ID` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '编号',
  `PARENT_ID` INT UNSIGNED NOT NULL COMMENT '父级编号',
  `PARENT_IDS` VARCHAR(2000) NOT NULL COMMENT '所有父级编号',
  `NAME` VARCHAR(100) NOT NULL COMMENT '名称',
  `LEVEL` INT UNSIGNED NOT NULL COMMENT '层级',
  `SORT` INT NOT NULL COMMENT '排序',
  `HREF` VARCHAR(2000) DEFAULT NULL COMMENT '链接',
  `ICON` VARCHAR(100) DEFAULT NULL COMMENT '图标',
  `IS_SHOW` TINYINT(1) NOT NULL COMMENT '是否在菜单中显示',
  `PERMISSION` VARCHAR(200) DEFAULT NULL COMMENT '权限标识',

  `IS_DELETED` TINYINT(1) NOT NULL COMMENT '删除标记',
  `REMARKS` VARCHAR(255) DEFAULT NULL COMMENT '备注信息',
  `CREATE_BY` INT UNSIGNED DEFAULT NULL COMMENT '创建者',
  `CREATE_DATE` DATETIME DEFAULT NULL COMMENT '创建时间',
  `UPDATE_BY` INT UNSIGNED DEFAULT NULL COMMENT '更新者',
  `UPDATE_DATE` DATETIME DEFAULT NULL COMMENT '更新时间',

  PRIMARY KEY (`ID`)
) COMMENT='菜单表'
  ENGINE=InnoDB
  DEFAULT CHARSET=utf8;

INSERT INTO `SYS_MENU`(ID, PARENT_ID, PARENT_IDS, NAME, LEVEL, SORT, HREF, ICON, IS_SHOW, PERMISSION, IS_DELETED)
VALUES
       (1, 0, ',0,', '系统管理', 1, 30, '',  '', 1, '', 0),
          (2, 1, ',0,1,', '管理员管理', 2, 1, '/admin/manager/list',  '', 1, '', 0),
              (3, 2, ',0,1,2,', '查看', 3, 1, '',  '', 0, 'sys:user:view', 0),
              (4, 2, ',0,1,2,', '新增', 3, 2, '',  '', 0, 'sys:user:create', 0),
              (5, 2, ',0,1,2,', '修改', 3, 3, '',  '', 0, 'sys:user:edit', 0),
              (6, 2, ',0,1,2,', '删除', 3, 4, '',  '', 0, 'sys:user:delete', 0),
              (7, 2, ',0,1,2,', '导出', 3, 5, '',  '', 0, 'sys:user:export', 0),
          (8, 1, ',0,1,', '角色管理', 2, 2, '/admin/role/list',  '', 1, '', 0),
              (9, 8, ',0,1,8,', '查看', 3, 1, '',  '', 0, 'sys:role:view', 0),
              (10, 8, ',0,1,8,', '新增', 3, 2, '',  '', 0, 'sys:role:create', 0),
              (11, 8, ',0,1,8,', '修改', 3, 3, '',  '', 0, 'sys:role:edit', 0),
              (12, 8, ',0,1,8,', '删除', 3, 4, '',  '', 0, 'sys:role:delete', 0),
          (13, 1, ',0,1,', '菜单管理', 2, 3, '/admin/menu/list',  '', 1, '', 0),
              (14, 13, ',0,1,13,', '查看', 3, 1, '',  '', 0, 'sys:menu:view', 0),
              (15, 13, ',0,1,13,', '新增', 3, 2, '',  '', 0, 'sys:menu:create', 0),
              (16, 13, ',0,1,13,', '修改', 3, 3, '',  '', 0, 'sys:menu:edit', 0),
              (17, 13, ',0,1,13,', '删除', 3, 4, '',  '', 0, 'sys:menu:delete', 0),
          (18, 1, ',0,1,', '参数管理', 2, 3, '/admin/param/list',  '', 1, '', 0),
              (19, 18, ',0,1,18,', '查看', 3, 1, '',  '', 0, 'sys:param:view', 0),
              (20, 18, ',0,1,18,', '新增', 3, 2, '',  '', 0, 'sys:param:create', 0),
              (21, 18, ',0,1,18,', '修改', 3, 3, '',  '', 0, 'sys:param:edit', 0),
              (22, 18, ',0,1,18,', '删除', 3, 4, '',  '', 0, 'sys:param:delete', 0)
;


/**
 */
DROP TABLE IF EXISTS `SYS_ROLE`;
CREATE TABLE `SYS_ROLE` (
  `ID` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '编号',
  `CODE` VARCHAR(255) DEFAULT NULL COMMENT '角色编码',
  `NAME` VARCHAR(100) NOT NULL COMMENT '角色名称',
  `IS_SYSTEM` TINYINT(1) NOT NULL COMMENT '是否系统',

  `IS_DELETED` TINYINT(1) NOT NULL COMMENT '删除标记',
  `REMARKS` VARCHAR(255) DEFAULT NULL COMMENT '备注信息',
  `CREATE_BY` INT UNSIGNED DEFAULT NULL COMMENT '创建者',
  `CREATE_DATE` DATETIME DEFAULT NULL COMMENT '创建时间',
  `UPDATE_BY`  INT UNSIGNED DEFAULT NULL COMMENT '更新者',
  `UPDATE_DATE` DATETIME DEFAULT NULL COMMENT '更新时间',

  PRIMARY KEY (`ID`)
) COMMENT='角色表' ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `SYS_ROLE` (ID, CODE, NAME, IS_SYSTEM, IS_DELETED)
VALUES (1, 'admin', '超级管理员', 0, 0),
        (2, 'test', '测试管理员', 0, 0);

/**
 */
DROP TABLE IF EXISTS `SYS_ROLE_MENU`;
CREATE TABLE `SYS_ROLE_MENU` (
  `ROLE_ID` INT UNSIGNED NOT NULL COMMENT '角色编号',
  `MENU_ID` INT UNSIGNED NOT NULL COMMENT '菜单编号',
  PRIMARY KEY (`ROLE_ID`,`MENU_ID`)
) COMMENT='角色-菜单' ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `SYS_ROLE_MENU` (ROLE_ID, MENU_ID)
VALUES (1, 1),
      (1, 2),(1, 3),(1, 4),(1, 5),(1, 6),(1, 7),
      (1, 8),(1, 9),(1, 10), (1, 11),(1, 12),
      (1, 13),(1, 14),(1, 15),(1, 16),(1, 17),
      (1, 18),(1, 19),(1, 20),(1, 21),(1, 22);

INSERT INTO `SYS_ROLE_MENU` (ROLE_ID, MENU_ID)
VALUES (2, 1),
        (2, 8),(2, 9),(2, 10), (2, 11),(2, 12),
        (2, 13),(2, 14),(2, 15),(2, 16),(2, 17);


/**
 */
DROP TABLE IF EXISTS `SYS_USER_ROLE`;
CREATE TABLE `SYS_USER_ROLE` (
  `USER_ID` INT UNSIGNED NOT NULL COMMENT '用户编号',
  `ROLE_ID` INT UNSIGNED NOT NULL COMMENT '角色编号',
  PRIMARY KEY (`USER_ID`,`ROLE_ID`)
)  COMMENT='用户-角色' ENGINE=InnoDB DEFAULT CHARSET=UTF8;

INSERT INTO `SYS_USER_ROLE`(USER_ID, ROLE_ID) VALUES (1, 1);
