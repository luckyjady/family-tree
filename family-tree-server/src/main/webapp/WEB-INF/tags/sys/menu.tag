<%@ tag import="static genesis.familytree.server.common.security.SystemAuthorizingRealm.*" %>
<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>

<%@ attribute name="items" type="java.util.List" required="false" description="列表"%>
<%@ attribute name="level" type="java.lang.Integer" required="false" description="等级"%>
<%@ attribute name="styleClass" type="java.lang.String" required="false" description="样式"%>
<%@ attribute name="itemStyleClass" type="java.lang.String" required="false" description="样式"%>

<c:if test="${empty items}">
    <c:set var="items" value="<%=Principal.getCurrent().getMenus()%>" />
</c:if>

<ul class="${empty styleClass ? 'nav navbar-nav' : styleClass}">
<c:forEach var="menu" items="${items}" >
    <jsp:useBean id="menu" type="genesis.familytree.server.modules.sys.model.TreeMenu" />

    <c:if test="${menu.isShow}">
        <li class="${menu.level eq 1 ? 'dropdown' : menu.hasChildren  ? 'dropdown-submenu' : ''}">
            <c:choose>
                <c:when test="${menu.hasChildren}">
                    <a data-id="${menu.id}" href="#"
                       class="dropdown-toggle" data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false">${menu.name}</a>
                </c:when>
                <c:otherwise>
                    <a href="${menu.data.href}" target="mainFrame">${menu.name}</a>
                </c:otherwise>
            </c:choose>
            <c:choose>
                <c:when test="${menu.hasChildren and not(not empty level and menu.level eq level)}">
                    <sys:menu items="${menu.children}" styleClass="dropdown-menu" itemStyleClass="" />
                </c:when>
            </c:choose>
        </li>
    </c:if>
</c:forEach>
</ul>