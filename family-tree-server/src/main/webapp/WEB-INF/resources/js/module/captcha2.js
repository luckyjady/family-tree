window.genesis || (window.genesis = {});

(function ($) {
    genesis.required("/module/post");

    var _WIN = window;
    var _PARENT = _WIN.parent;

    var funs;
    if(_WIN != _PARENT) {
        do {
            _WIN = _PARENT;
            _PARENT = _WIN.parent;
        } while (_PARENT != _WIN);


        var showCaptcha = _WIN.genesis["showCaptcha"];
        var hideCaptcha = _WIN.genesis["hideCaptcha"];
        funs = (showCaptcha && hideCaptcha) ? {"showCaptcha": showCaptcha, "hideCaptcha": hideCaptcha} : null;
    }

    funs = funs || (function () {
            var html =
                '<div class="modal fade" data-backdrop="true">\
                    <div class="modal-dialog modal-sm" role="document">\
                        <div class="modal-content">\
                            <div class="modal-header">\
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\
                                <h4 class="modal-title">操作确认</h4>\
                            </div>\
                            <div class="modal-body">\
                                <form class="form-inline ">\
                                    <div style="color: #a94442;"></div>\
                                    <div class="form-group"><input type="text" class="form-control" placeholder="请输入验证码"/></div>\
                                    <div class="form-group"><img src="" style="width: 80px;"/></div>\
                                </form>\
                            </div>\
                            <div class="modal-footer">\
                                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>\
                                <button type="button" class="btn btn-primary">确认</button>\
                            </div>\
                        </div>\
                    </div>\
                </div>';
            var options = {};
        return {
            showCaptcha: function (prms, cb) {
                var $m;
                if(!options["modal"]) {
                    $m = $(html).appendTo("body");
                    options["modal"] = $m;
                }else {
                    $m = options["modal"];
                }

                $(".modal-body form>div:eq(0)", $m).text(prms.message || "");
                $(".modal-body :text", $m).focus().val("");
                $(".modal-body img", $m).attr("src", prms.url + "/captcha?" + new Date().getTime());
                $(".modal-footer .btn-primary", $m).off().on("click", function () {
                    $(this).off();
                    var code = $(".modal-body :text", $m).val();
                    cb && cb(code);
                });
                $(".modal-body form", $m).on("submit", function () {
                    $m.find(".btn-primary").trigger("click");
                    return false;
                });

                $m.off('shown.bs.modal').on('shown.bs.modal', function () {
                    $m.find("input").focus()
                });
                $m.modal("show");
            },
            hideCaptcha: function (cb) {
                var  $m = options["modal"];
                if(!$m) {
                    return;
                }
                $m.off("hidden.bs.modal").on("hidden.bs.modal", function(){
                    $m.off("hidden.bs.modal");
                    cb && cb();
                });
                $m.modal("hide");
            }
        }
    })();
    
    window.genesis = $.extend(window.genesis, funs);
})(jQuery);