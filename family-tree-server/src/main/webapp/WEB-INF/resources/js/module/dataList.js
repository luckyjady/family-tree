(function ($) {
    $.fn.dataList = (function() {

        var keyColumn_Label = "label";
        var keyColumn_Property = "property";

        var defaultOpt = {
            "columns": [/*{label: "名称", property: "name"// function($target, rowIndex, data), width: "auto", style: "", styleClass: "", headStyle:"", headStyleClass: ""}*/ ],
            "url": "",
            "pageNo": 1,
            "filter": function($target, data){
                var page = data["data"];
                return page;
            },
            "pagination": ".pagination",
            "onAfterLoad": function($target, response){
                if(response["code"] != 0) {
                    alert(response["message"]);
                    return false;
                }
                return true;
            },
            "onRowSelected": function(event, $target, data, rowIndex, columnIndex) {

            },
            "init": function($target) {
                var opt = this;

                var $target = $target.empty();
                $target.addClass("table table-bordered");
                $target.on("click", ">tbody>tr *", function (event) {
                    var $this = $(this);
                    var $tr = $this.parents("tr:eq(0)");
                    var data = $tr.data("_data");
                    opt["onRowSelected"](event, $target, data, $tr.index(), $this.index());
                    return false;
                });

                $target.on("mouseenter", ">tbody>tr", function (event) {
                    $(this).addClass("active info");
                }).on("mouseleave", ">tbody>tr", function (event) {
                    $(this).removeClass("active info");
                });

                var $thead = $("<thead>").appendTo($target);
                var $tbody = $("<tbody>").appendTo($target);
                var $tr = $("<tr>").appendTo($thead);

                var columns = opt["columns"];
                for(var i in columns) {
                    var column = columns[i];

                    var label = column[keyColumn_Label];
                    var $th = $("<th>").text(label).attr("style", column["headStyle"]).attr("class", column["headStyleClass"]);
                    $th.appendTo($tr);
                }
            },
            "getPage": function($target, pageNo) {
                var opt = this;
                opt["pageNo"] = pageNo;

                $.post(opt["url"], {"page": pageNo}, function (response) {
                    if(!opt["onAfterLoad"]($target, response)){
                        return;
                    }

                    var page = opt["filter"]($target, response);
                    var list = page;
                    if(opt["pagination"]){
                        list = page["list"];
                    }
                    opt["buildRows"]($target, list);

                    opt["buildPagination"]($target, page);
                });
            },
            "buildPagination": function($target, data) {
                var opt = this;
                if(!opt["pagination"]){
                    return;
                }
                $(opt["pagination"]).pagination({
                    "page": data,
                    "stepFun": function(index){
                        return function(){ opt["getPage"]($target, index);};
                    }
                });
            },
            "buildRows": function($target, data) {
                var opt = this;
                var list = data;
                var $tbody = $target.find("tbody").empty();

                for(var i in list){
                    var row = opt["buildRow"]($target, list[i], i);
                    $tbody.append(row);
                }
            },
            "buildRow": function($target, data, rowIndex) {
                var opt = this;

                var $tr = $('<tr>').data("_data", data);

                var columns = opt["columns"];
                for(var i in columns){
                    var column = columns[i];
                    var property = column[keyColumn_Property];

                    var $td = $("<td>");
                    if(typeof property == "function"){
                        var $p = property($target, rowIndex, data);
                        $td.append($p);
                    }else {
                        var value = data[property];
                        $td = $td.text(value);
                    }

                    $td.attr("style", column["style"]).attr("class", column["styleClass"])
                        .appendTo($tr);

                    if(typeof column.width == "string") {
                        $td.css({width: column.width});
                    }else if(typeof column.width == "number"){
                        $td.css({width: column.width});
                    }
                }
                return $tr;
            }
        };

        var options = {

        };

        var fun = function(opt, params){
            var $target = $(this);

            if(typeof opt == "object") {
                options = $.extend(defaultOpt, opt);
                options["init"]($target);
            }else if(typeof opt == "string"){
                if(opt === "reload"){
                    options["getPage"]($target, options["pageNo"])
                }else if(opt === "load"){
                    var pageNo = params || 1;
                    options["getPage"]($target, pageNo);
                }
            }

            return $target;
        };

        return fun;
    })();
})(jQuery);