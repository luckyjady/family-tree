<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
    <title>主页</title>
</head>
<body>

<div class="panel panel-primary">
    <div class="panel-heading">
        <span>后台角色</span>
        <span style="float: right;">

            <button class="btn btn-default btn-xs" type="button">
                <span aria-hidden="true" class="glyphicon glyphicon-refresh"></span>刷新</button>

            <button class="btn btn-default btn-xs" type="button">
                <span aria-hidden="true" class="glyphicon glyphicon-plus-sign"></span>新增</button>

        </span>

    </div>
    <div class="panel-body">
        <table id="dataList" class="table table-bordered">
        </table>
        <nav>
            <ul class="pagination">
            </ul>
        </nav>
    </div>
</div>

<div id="editPanel"></div>

<script>
    $(function () {

        var $dataList = $("#dataList").dataList({
            "url": "${ctx}/role/list",
            columns: [
                {label: "ID",  property: "id",    width: 80},
                {label: "角色编号", property: "code",  width: 300},
                {label: "角色名称", property: "name"},
                {label: "权限",   property: function ($target, rowIndex, data) {
                    var row = "";
                    <shiro:hasPermission name="sys:role:edit">
                    row += '<a class="access">权限</a>';
                    </shiro:hasPermission>
                    return row;
                }, width: 100},
                {
                    label: "操作", property: function ($target, rowIndex, data) {
                    var row = "";
                    <shiro:hasPermission name="sys:role:edit">
                    row += '<a class="edit">修改</a>';
                    </shiro:hasPermission>
                    <shiro:hasPermission name="sys:role:delete">
                    if (data["isSystem"] != 1) {
                        row += '<a class="delete">删除</a>';
                    }
                    </shiro:hasPermission>
                    return row;
                }, width: 110, style: "text-align: center;"}
            ],
            "onRowSelected": function(event, $target, data, rowIndex, columnIndex) {
                var $this = $(event.target);

                if($this.is(".edit")) {

                    $("#editPanel").editPanel({
                        "title": "修改 - 后台角色",
                        "url": "${ctx}/role/update?id=" + data.id,
                        "callback": function (refresh) {
                            $dataList.dataList("reload");
                        }
                    });
                }else if($this.is(".delete")){
                    if (!confirm("是否删除？")) {
                        return;
                    }

                    var params = {
                        "id": data["id"]
                    };
                    window.genesis.post("${ctx}/role/delete", params, function (response) {
                        $dataList.dataList("load", 1);
                    });
                }else  if($this.is(".access")) {

                    $("#editPanel").editPanel({
                        "title": "修改 - 后台角色",
                        "url": "${ctx}/role/update?id=" + data.id,
                        "callback": function (refresh) {
                            $dataList.dataList("reload");
                        }
                    });
                }
            }
        });

        $(".panel-heading .glyphicon-plus-sign").parent().on("click", function () {
            $("#editPanel").editPanel({
                "title": "新增 - 后台角色",
                "url": "${ctx}/role/create",
                "callback": function (refresh) {
                    $dataList.dataList("load", 1);
                }
            });
        });

        $(".panel-heading .glyphicon-refresh").parent().on("click", function () {
            $dataList.dataList("reload");
        });

        $dataList.dataList("load", 1);
    });
</script>

</body>
</html>
