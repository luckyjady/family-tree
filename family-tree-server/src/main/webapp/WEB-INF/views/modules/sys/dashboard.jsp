<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp" %>
<html>
<head>
    <title>主页</title>
</head>
<body>
<h1>dashboard</h1>

<div class="container">
    <div class="row">
        <h2>Bootstrap 3多级下拉菜单</h2>
        <hr>
        <div class="dropdown">
            <a id="dLabel" role="button" data-hover="dropdown" data-close-others="false" class="btn btn-primary" data-target="#"
               href="javascript:;">
                下拉多级菜单 <span class="caret"></span>
            </a>
            <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
                <li><a href="javascript:;">一级菜单</a></li>
                <li><a href="javascript:;">一级菜单</a></li>
                <li class="divider"></li>
                <li class="dropdown-submenu">
                    <a tabindex="-1" href="javascript:;">一级菜单</a>
                    <ul class="dropdown-menu">
                        <li><a tabindex="-1" href="javascript:;">二级菜单</a></li>
                        <li class="divider"></li>
                        <li class="dropdown-submenu">
                            <a href="javascript:;">二级菜单</a>
                            <ul class="dropdown-menu">
                                <li><a href="javascript:;">三级菜单</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
<h3>start</h3>
<c:forEach var="item" begin="1" end="100">
    <br/>
</c:forEach>
<h3>end</h3>
</body>
</html>
