<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
    <title>主页</title>
</head>
<body>

<div class="panel panel-primary">
    <div class="panel-heading">
        <span>管理员</span>
        <span style="float: right;">

            <button class="btn btn-default btn-xs" type="button">
                <span aria-hidden="true" class="glyphicon glyphicon-refresh"></span>刷新</button>

            <button class="btn btn-default btn-xs" type="button">
                <span aria-hidden="true" class="glyphicon glyphicon-plus-sign"></span>新增</button>

        </span>

    </div>
    <div class="panel-body">
        <table id="dataList" class="table table-bordered">
            <thead>
            <tr>
                <th width="80">ID</th>
                <th width="150">登录名</th>
                <th>昵称</th>
                <th width="100">权限</th>
                <th width="120">操作</th>
            </tr>
            </thead>
            <tbody>

            </tbody>

        </table>
        <nav>
            <ul class="pagination" style="margin: 0;">
            </ul>
        </nav>
    </div>
</div>

<div id="editPanel"></div>

<script>
    $(function () {
        var buildRow = function(data) {
            var row = '<tr data-id="'+ data["id"] +'">';
            row += '<td>'+ data["id"] +'</td>';
            row += '<td>'+ data["username"] +'</td>';
            row += '<td>'+ data["nickname"] +'</td>';
            row += '<td>';
            <shiro:hasPermission name="sys:user:edit">
            row += '<a class="access">权限</a>';
            </shiro:hasPermission>
            row += '</td>';
            row += '<td>';
            <shiro:hasPermission name="sys:user:edit">
            row += '<a class="edit">修改</a>';
            </shiro:hasPermission>
            <shiro:hasPermission name="sys:user:delete">
            if(data["isSystem"] != 1) {
                row += '<a class="delete">删除</a>';
            }
            </shiro:hasPermission>
            row += '</td>';
            row += '</tr>';
            return row;
        };

        var goPage = function(page) {
            var pageNo = page || $(".pagination .active a").text();

            $.post("${ctx}/manager/list", {"page": pageNo}, function (response) {
                if(response["code"] != 0) {
                    alert(response["message"]);
                    return;
                }

                var $parent = $("#dataList tbody").empty();

                var page = response["data"];
                var list = page["list"];
                for(var i in list) {
                    var row = buildRow(list[i]);
                    $parent.append(row);
                }

                $(".pagination").pagination({
                    "page": page,
                    "stepFun": function(index){
                        return function(){goPage(index);};
                    }
                });
            });
        };

        $("#dataList tbody").on("click", "a.edit", function () {
            $("#editPanel").editPanel({
                "title": "修改 - 系统参数",
                "url": "${ctx}/manager/update?id=" + $(this).parents("[data-id]").attr("data-id"),
                "callback": function (refresh) {
                    goPage();
                }
            });
        }).on("click", "a.delete", function () {
            if(!confirm("是否删除？")){
                return;
            }

            var params = {
                "id": $(this).parents("[data-id]").attr("data-id")
            };
            genesis.post("${ctx}/manager/delete", params, function (response) {
                //closeWin && closeWin(true);
                goPage();
            });
        });

        $(".panel-heading .glyphicon-plus-sign").parent().on("click", function() {
            $("#editPanel").editPanel({
                "title": "新增 - 系统参数",
                "url": "${ctx}/manager/create",
                "callback": function (refresh) {
                    goPage(1);
                }
            });
        });
        $(".panel-heading .glyphicon-refresh").parent().on("click", function() {
            goPage();
        });

        goPage(1);
    });
</script>
</body>
</html>
