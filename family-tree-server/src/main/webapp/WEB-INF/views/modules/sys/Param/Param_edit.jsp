<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<jsp:useBean id="entity" type="genesis.familytree.server.modules.common.entity.Param" scope="request" />
<html>
<head>
    <title>主页</title>
</head>
<body>
<div class="container">
    <form id="editForm" onsubmit="return onSubmit();">
        <div class="form-group">
            <label class="control-label" for="paramCode">参数编码</label>
            <input type="text" id="paramCode" name="paramCode" placeholder="请输入参数编码"
                   class="form-control input-lg"
                   value="${entity.paramCode}"
                   ${entity.isSystem eq 0 ? "" : "disabled"}/>
            <div class="help-block"></div>
        </div>

        <div class="form-group">
            <label class="control-label" for="paramName">参数名称</label>
            <input type="text" id="paramName" name="paramName" placeholder="请输入参数名称"
                   class="form-control input-lg"
                   value="${entity.paramName}"
                   ${entity.isSystem eq 0 ? "" : "disabled"}/>
            <div class="help-block"></div>
        </div>

        <div class="form-group">
            <label class="control-label" for="paramValue">参数值</label>
            <input type="text" id="paramValue" name="paramValue" placeholder="请输入参数值"
                   class="form-control input-lg"
                   value="${entity.paramValue}"/>
            <div class="help-block"></div>
        </div>

        <div class="form-group">
            <label class="control-label" for="paramGroup">参数分组</label>
            <input type="text" id="paramGroup" name="paramGroup" placeholder="请输入参数分组"
                   class="form-control input-lg"
                   value="${entity.paramGroup}"/>
            <div class="help-block"></div>
        </div>
    </form>
</div>

<script>
    var onSubmit = function() {
        var params = {
            "id": "${entity.id}",
            "paramCode": $("#paramCode").val(),
            "paramName": $("#paramName").val(),
            "paramValue": $("#paramValue").val(),
            "paramGroup": $("#paramGroup").val()
        };

        genesis.post("${ctx}/param/${empty entity.id ? 'create' : 'update' }", params, function (response) {
            closeWin && closeWin(true);
        });
        return false;
    };

</script>
</body>
</html>
