<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
    <title>主页</title>
</head>
<body>

<div class="panel panel-primary">
    <div class="panel-heading">
        <span>系统参数</span>
        <span style="float: right;">

            <button class="btn btn-default btn-xs" type="button">
                <span aria-hidden="true" class="glyphicon glyphicon-refresh"></span>刷新</button>

            <button class="btn btn-default btn-xs" type="button">
                <span aria-hidden="true" class="glyphicon glyphicon-plus-sign"></span>新增</button>

        </span>

    </div>
    <div class="panel-body">
        <table id="dataList">
        </table>
        <nav>
            <ul class="pagination">
            </ul>
        </nav>
    </div>
</div>

<div id="editPanel"></div>

<script>
    $(function () {

        var $dataList = $("#dataList").dataList({
            "url": "${ctx}/param/list",
            columns: [
                {label: "ID",  property: "id",         width: 80},
                {label: "编号", property: "paramCode"},
                {label: "名称", property: "paramName",  width: 300},
                {label: "值",   property: "paramValue", width: 200},
                {label: "分组", property: "paramGroup", width: 200},
                {
                    label: "操作",
                    property: function ($target, rowIndex, data) {
                        var row = "";
                        <shiro:hasPermission name="sys:param:edit">
                            row += '<a class="edit">修改</a>';
                        </shiro:hasPermission>
                        <shiro:hasPermission name="sys:param:delete">
                            if (data["isSystem"] != 1) {
                                row += '<a class="delete">删除</a>';
                            }
                        </shiro:hasPermission>
                        return row;
                    },
                    width: 110,
                    style: "text-align: center;"
                }
            ],
            "onRowSelected": function(event, $target, data, rowIndex, columnIndex) {
                var $this = $(event.target);

                if($this.is(".edit")) {

                    $("#editPanel").editPanel({
                        "title": "修改 - 系统参数",
                        "url": "${ctx}/param/update?id=" + data.id,
                        "callback": function (refresh) {
                            $dataList.dataList("reload");
                        }
                    });
                }else if($this.is(".delete")){
                    if (!confirm("是否删除？")) {
                        return;
                    }

                    var params = {
                        "id": data["id"]
                    };
                    window.genesis.post("${ctx}/param/delete", params, function (response) {
                        $dataList.dataList("load", 1);
                    });
                }
            }
        });

        $(".panel-heading .glyphicon-plus-sign").parent().on("click", function () {
            $("#editPanel").editPanel({
                "title": "新增 - 系统参数",
                "url": "${ctx}/param/create",
                "callback": function (refresh) {
                    $dataList.dataList("load", 1);
                }
            });
        });
        $(".panel-heading .glyphicon-refresh").parent().on("click", function () {
            $dataList.dataList("reload");
        });

        $dataList.dataList("load", 1);
    });
</script>

</body>
</html>
