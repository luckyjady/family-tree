<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
    <title>主页</title>

    <style>
        a.level0 .node_name {width: 120px;}
        a.level1 .node_name {width: 101px;}
        a.level2 .node_name {width: 81px;}
        .tree-row{display: inline-block;}
        .tree-row-table{border: 1px; border-spacing: 1px;border-collapse: collapse;}
        .ztree li a > * {float: left;}
        .ztree li a {overflow: hidden;}
    </style>
</head>
<body>
<h1>菜单管理</h1>

<div class="container">
    <ul id="treeDemo" class="ztree"></ul>
</div>

<div id="editPanel"></div>

<!----------------------------------------------------------------------->

<script>
    $(function () {

        $("#treeDemo").on("click", ".tree-row .update", function() {
            var id = $(this).parents(".tree-row").attr("data-id");

            $("#editPanel").editPanel({
                "title": "",
                "url": "${ctx}/menu/update?id=" + id
            });
        }).on("click", ".tree-row .delete", function() {
            var id = $(this).parents(".tree-row").attr("data-id");

            //$modal.show("${ctx}/menu/delete?id=" + id);
        }).on("click", ".tree-row .create", function() {
            var id = $(this).parents(".tree-row").attr("data-id");

            $("#editPanel").editPanel({
                "title": "",
                "url": "${ctx}/menu/create?pid=" + id
            });
        });
        function onAsyncSuccess(event, treeId, msg, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("treeDemo");
            zTree.expandAll(true);
        }


        function filter(treeId, parentNode, responseData) {
            var childNodes = responseData["data"];
            childNodes["open"] = true;
            return childNodes;
        }

        function addHoverDom(treeId, treeNode) {
            if (treeNode.parentNode && treeNode.parentNode.id!=2) return;
            var aObj = $("#" + treeNode.tId + "_a");

            aObj.parent().addClass("tree-row-hover");
        }

        function removeHoverDom(treeId, treeNode) {
            if (treeNode.parentNode && treeNode.parentNode.id!=2) return;
            var aObj = $("#" + treeNode.tId + "_a");

            aObj.parent().removeClass("tree-row-hover");
        }

        function addDiyDom(treeId, treeNode) {
            if (treeNode.parentNode && treeNode.parentNode.id!=2) return;
            var aObj = $("#" + treeNode.tId + "_a");

            aObj.append(
                    '<div class="tree-row" data-id="'+ treeNode['id'] +'">\
                        <table class="tree-row-table">\
                        <tbody>\
                            <tr>\
                                <td width="200">'+ treeNode['href'] +'</td>\
                                <td width="40">'+ treeNode['sort'] +'</td>\
                                <td width="40">'+ (treeNode['isShow'] == 1? '显示' : '隐藏') +'</td>\
                                <td width="200">'+ treeNode['permission'] +'</td>\
                                <td width="200">\
                                <shiro:hasPermission name="sys:menu:edit">\
                                    <a class="update">修改</a>\
                                </shiro:hasPermission>\
                                <shiro:hasPermission name="sys:menu:delete">\
                                    <a class="delete">删除</a>\
                                </shiro:hasPermission>\
                                <shiro:hasPermission name="sys:menu:create">\
                                    <a class="create">添加下菜单</a>\
                                </shiro:hasPermission>\
                                </td>\
                            </tr>\
                        </tbody>\
                        </table>\
                    </div>');
        }
        
        $.fn.zTree.init($("#treeDemo"), {
           /* check: {
                enable: true
            },*/
            view: {
                addHoverDom: addHoverDom,
                removeHoverDom: removeHoverDom,
                addDiyDom: addDiyDom
            },
            data: {
                simpleData: {
                    enable: true,
                    idKey: "id",
                    pIdKey: "parentId",
                    rootPId: 0
                }
            },
            async: {
                enable: true,
                url:"${ctx}/menu/list",
                dataType: "json",
                autoParam:["id", "name=n", "level=lv"],
                otherParam:{"otherParam":"zTreeAsyncTest"},
                dataFilter: filter
            },
            callback: {
                onAsyncSuccess: onAsyncSuccess
            }
        });

        var zTree = $.fn.zTree.getZTreeObj("treeDemo");
        zTree.setting.check.chkboxType = {"Y": "p", "N":"s"};


    });
</script>
</body>
</html>
