<%@ page import="genesis.familytree.server.common.security.SystemAuthorizingRealm" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
    <title>主页</title>
</head>
<body>
<nav class="navbar navbar-default" style="margin: 0;">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <a class="navbar-brand" href="${ctx}/dashboard" target="mainFrame">FamilyTree</a>
        </div>
        <div class="nav navbar-nav navbar-left">
            <sys:menu />
        </div>
        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><shiro:principal property="name"/><span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#">个人信息</a></li>
                    <li><a href="#">配置</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="${ctx}/logout">退出</a></li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
<div>
    <iframe id="mainFrame" name="mainFrame" frameborder="0" style="border: none;"
            src="${ctx}/dashboard">

    </iframe>
</div>

<script>
    $(function () {

        var resize = function () {
            var winHeight = $(window).height();
            var winWidth = $(window).width();

            $("#mainFrame").css({
                height: winHeight - 52 - 5,
                width: winWidth
            });
        };

        $(window).on("resize", resize);
        resize();
    });
</script>
</body>
</html>
