package genesis.familytree.server.modules.common.web;

import com.google.code.kaptcha.Producer;
import genesis.familytree.server.common.utils.CaptchaUtils;
import genesis.familytree.server.common.web.BaseController;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;

/**
 *
 */
@Controller
public class CommonController {
    private static final Logger LOG = LoggerFactory.getLogger(CommonController.class);

    @Resource private Producer captchaProducer;

    /**
     * 生成验证码，并输出成图片 <br/>
     * /weixin/user/login/captcha = SessionKey("SESSION_KEY_CAPTCHA_WEIXIN_USER_LOGIN")
     *
     * @param request  HttpServletRequest
     * @param response HttpServletResponse     @throws Exception
     * @see genesis.familytree.server.common.utils.CaptchaUtils#buildCaptchaKey(String...)
     */
    @RequestMapping("/**/captcha")
    public ModelAndView handleRequest(HttpServletRequest request,
                                      HttpServletResponse response,
                                      HttpSession session) {
        try {
            response.setDateHeader("Expires", 0);
            // Set standard HTTP/1.1 no-cache headers.
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            // Set IE extended HTTP/1.1 no-cache headers (use addHeader).
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            // Set standard HTTP/1.0 no-cache header.
            response.setHeader("Pragma", "no-cache");
            // return a jpeg
            response.setContentType("image/jpeg");
            // create the text for the image
            String capText = captchaProducer.createText();
            // store the text in the session

            String uri = request.getRequestURI();

            String[] keys = uri.split("/");
            keys = ArrayUtils.remove(keys, keys.length - 1);
            keys = CaptchaUtils.clearKeys(keys);

            CaptchaUtils.putCaptchaInSession(session, capText, keys);

            // create the image with the text
            BufferedImage bi = captchaProducer.createImage(capText);

            try(ServletOutputStream out = response.getOutputStream()) {
                // write the data out
                ImageIO.write(bi, "jpg", out);
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
        return null;
    }
}
