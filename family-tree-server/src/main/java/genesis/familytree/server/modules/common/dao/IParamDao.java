package genesis.familytree.server.modules.common.dao;

import genesis.familytree.server.common.persistence.Page;
import genesis.familytree.server.modules.common.entity.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 */
@Repository
public interface IParamDao {

    Param getById(@org.apache.ibatis.annotations.Param("id") int id);

    Param getByCode(@org.apache.ibatis.annotations.Param("paramCode") String paramCode);

    List<Param> getList(Param param);

    int saveOne(Param po);

    int updateOne(Param po);

    int deleteById(@org.apache.ibatis.annotations.Param("id") int id);
}
