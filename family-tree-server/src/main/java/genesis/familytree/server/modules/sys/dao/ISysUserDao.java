/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package genesis.familytree.server.modules.sys.dao;

import genesis.familytree.server.common.persistence.CrudDao;
import genesis.familytree.server.modules.sys.entity.SysUser;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 用户DAO接口
 * @author ThinkGem
 * @version 2014-05-16
 */
@Repository
public interface ISysUserDao extends CrudDao<SysUser> {
	
	/**
	 * 根据登录名称查询用户
	 * @return
	 */
	SysUser getByName(@Param("username") String username);

	/**
	 * 通过OfficeId获取用户列表，仅返回用户id和name（树查询用户时用）
	 * @param sysUser
	 * @return
	 */
	List<SysUser> findUserByOfficeId(SysUser sysUser);
	
	/**
	 * 查询全部用户数目
	 * @return
	 */
	long findAllCount(SysUser sysUser);
	
	/**
	 * 更新用户密码
	 * @param sysUser
	 * @return
	 */
	int updatePasswordById(SysUser sysUser);
	
	/**
	 * 更新登录信息，如：登录IP、登录时间
	 * @param sysUser
	 * @return
	 */
	int updateLoginInfo(SysUser sysUser);

	/**
	 * 删除用户角色关联数据
	 * @param sysUser
	 * @return
	 */
	int deleteUserRole(SysUser sysUser);
	
	/**
	 * 插入用户角色关联数据
	 * @param sysUser
	 * @return
	 */
	int insertUserRole(SysUser sysUser);
	
	/**
	 * 更新用户信息
	 * @param sysUser
	 * @return
	 */
	int updateUserInfo(SysUser sysUser);

	List<SysUser> getList(SysUser params);
}
