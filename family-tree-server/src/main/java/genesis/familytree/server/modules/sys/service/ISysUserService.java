package genesis.familytree.server.modules.sys.service;

import genesis.familytree.server.common.persistence.Page;
import genesis.familytree.server.modules.sys.entity.SysUser;

/**
 *
 */
public interface ISysUserService {

    SysUser getById(int id);

    SysUser getByName(String username);

    Page<SysUser> getPage(int pageNumber, int pageSize, SysUser params);
}
