package genesis.familytree.server.modules.home.web;

import genesis.familytree.server.common.web.BaseHomeController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 */
@Controller
@RequestMapping("/ajax")
public class AjaxController extends BaseHomeController {

    @RequestMapping({"/unreadMessageCnt.json", ""})
    public void index(HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        out.print("{\n" +
                "  \"state\": 0,\n" +
                "  \"message\": \"发送成功\",\n" +
                "  \"value\": 20\n" +
                "}");
    }

}
