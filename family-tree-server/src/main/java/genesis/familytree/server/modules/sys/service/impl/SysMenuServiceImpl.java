package genesis.familytree.server.modules.sys.service.impl;

import genesis.familytree.server.common.exception.GenesisException;
import genesis.familytree.server.common.service.BaseService;
import genesis.familytree.server.modules.sys.dao.ISysMenuDao;
import genesis.familytree.server.modules.sys.entity.SysMenu;
import genesis.familytree.server.modules.sys.service.ISysMenuService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 *
 */
@Transactional(readOnly = true)
@Service
public class SysMenuServiceImpl extends BaseService implements ISysMenuService {

    @Resource private ISysMenuDao menuDao;

    @Override
    public SysMenu getById(Integer id) {
        return menuDao.getById(id);
    }

    @Override
    public List<SysMenu> getListByRole(int roleId) {
        throw new UnsupportedOperationException();
    }

    @Override
    @Transactional(readOnly = false)
    public void saveOne(SysMenu vo) {
        SysMenu parent = vo.getParent();
        if (null == parent || parent.getId() == null) {
            throw new GenesisException("父菜单不能为空");
        }

        if (parent.getId() == 0) {
            parent.setParentIds(",");
        }else {
            parent = this.menuDao.getById(parent.getId());
            if (null == parent) {
                throw new GenesisException("父菜单不存在");
            }
        }

        boolean isNew = null == vo.getId();

        // 获取修改前的parentIds，用于更新子节点的parentIds
        String oldParentIds = null;
        SysMenu po;
        if (isNew) {
            po = new SysMenu();
            po.setParent(parent);
            po.setParentIds(parent.getParentIds() + parent.getId() + ",");
        }else {
            po = this.menuDao.getById(vo.getId());
            if (null == po) {
                throw new GenesisException("菜单不存在");
            }

            oldParentIds = po.getParentIds();

            // 设置新的父节点串
            po.setParentIds(parent.getParentIds() + parent.getId() + ",");
        }


        if (vo.getIcon().startsWith("icon-")) {
            po.setIcon(vo.getIcon().substring(5));
        }


        // 保存或更新实体
        if (isNew) {
            menuDao.saveOne(po);
        } else {
            menuDao.updateOne(po);

            // 更新子节点 parentIds
            SysMenu m = new SysMenu();
            m.setParentIds("%," + po.getId() + ",%");
            List<SysMenu> list = menuDao.findByParentIdsLike(m);
            for (SysMenu e : list) {
                e.setParentIds(e.getParentIds().replace(oldParentIds, po.getParentIds()));
                menuDao.updateParentIds(e);
            }
        }
    }

    @Override
    @Transactional(readOnly = false)
    public void deleteOneById(int id) {
        menuDao.deleteOneById(id);
    }

    @Override
    public List<SysMenu> getAll() {
        return this.menuDao.findAllList(new SysMenu());
    }

    @Override
    public List<SysMenu> getByUserId(int userId) {
        return this.menuDao.findByUserId(userId);
    }


}
