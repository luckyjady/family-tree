/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package genesis.familytree.server.modules.sys.dao;

import genesis.familytree.server.common.persistence.CrudDao;
import genesis.familytree.server.modules.sys.entity.SysMenu;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 菜单DAO接口
 *
 * @author ThinkGem
 * @version 2014-05-16
 */
@Repository
public interface ISysMenuDao {

    SysMenu getById(@Param("id") int id);

    List<SysMenu> findByParentIdsLike(SysMenu sysMenu);

    List<SysMenu> findByUserId(@Param("userId") int userId);

    int updateParentIds(SysMenu sysMenu);

    int updateSort(SysMenu sysMenu);

    void saveOne(SysMenu po);

    void updateOne(SysMenu po);

    void deleteOneById(@Param("id") int id);

    List<SysMenu> findAllList(SysMenu menu);
}
