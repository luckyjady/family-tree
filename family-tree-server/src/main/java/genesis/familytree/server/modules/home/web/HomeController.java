package genesis.familytree.server.modules.home.web;

import genesis.familytree.server.common.web.BaseHomeController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 */
@Controller
public class HomeController extends BaseHomeController{

    @RequestMapping({"/index", ""})
    public Object index() {

        return "/modules/home/index";
    }

}
