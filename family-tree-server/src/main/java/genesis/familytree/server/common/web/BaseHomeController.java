package genesis.familytree.server.common.web;

/**
 * Created by Cheng on 2016/05/22.
 */
public class BaseHomeController extends BaseController {

    @Override
    protected boolean isCaptchaEnabled() {
        return false;
    }

}
