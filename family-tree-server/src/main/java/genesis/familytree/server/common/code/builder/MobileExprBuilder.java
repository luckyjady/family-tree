package genesis.familytree.server.common.code.builder;

import genesis.familytree.server.common.code.*;
import genesis.familytree.server.common.exception.ExpressionException;
import genesis.familytree.server.common.utils.GenesisUtils;
import org.jasig.cas.client.util.CommonUtils;

/**
 *
 */
public class MobileExprBuilder extends AbstractDataTypeBuilder implements DataTypeBuilder, DataNameBuilder {


    @Override
    public String build(String name, String format, Params params) throws ExpressionException {

        Object obj = GenesisUtils.isMobile(name) ? name : getValue(name, params);
        if (null != obj) {
            String str = obj.toString();

            return str.substring(0, 3) + "****" + str.substring(7);
        }
        return null;
    }

    @Override
    public Attribute build(Expression expr, int startIndex, int endIndex, String expression) {
        if (!expression.contains(":")) {
            return null;
        }

        String[] str = expression.split(":", 3);
        if (str.length < 2) {
            return null;
        }
        if (!"mobile".equals(str[1])) {
            return null;
        }
        return new Attribute(expr, startIndex, endIndex, str[0], str[1], str.length > 2 ? str[2] : null);
    }

}
