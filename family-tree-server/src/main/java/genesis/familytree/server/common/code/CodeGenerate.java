package genesis.familytree.server.common.code;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 *
 */
public class CodeGenerate {

    private static final Logger LOG = LoggerFactory.getLogger(CodeGenerate.class);

    /**
     * <p>
     *     {xxx} 直接输出名字为xxx的值
     *     {now:yyyy} 输出当前时间，而yyyy是格式化
     *     {dd:date:yyyy} 将名为dd的值时间格式化为yyyy
     *     {dd:number:0.00} 将名为dd的值,数字格式化
     *     {script: a+b} 这个直接使用javascript引擎解析a+b
     * </p>
     *
     * @param expression 表达式
     * @param params 参数
     */
    public static String parse(String expression, Map<String, Object> params) {
        if (null == expression
                || (expression = expression.trim()).isEmpty()) {
            throw new IllegalArgumentException("param[expression] required");
        }

        try {
            return new Expression(expression).getValue(new Params(params));
        } catch (Exception e) {
            LOG.error(" parse Expression["+ expression + "] error ", e);
            return null;
        }
    }

}
