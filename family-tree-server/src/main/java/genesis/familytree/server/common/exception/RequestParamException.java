package genesis.familytree.server.common.exception;

import genesis.familytree.server.common.JsonResult;
import genesis.familytree.server.common.utils.GenesisUtils;

/**
 *
 */
public class RequestParamException extends GenesisException {

    private JsonResult result;

    public RequestParamException(JsonResult result) {
        super("参数错误");
        this.result = result;
    }

    public RequestParamException error(String paramName, String message) {
        result.paramError(paramName, message);
        return this;
    }
}
