package genesis.familytree.server.common.code;


import genesis.familytree.server.common.exception.ExpressionException;

/**
 *
 */
public interface DataTypeBuilder {

    /**
     * 通过表达式生成值
     * @param name 属性的名字
     * @param format 格式化
     * @param params 参数
     *
     * @return 值
     */
    String build(String name, String format, Params params) throws ExpressionException;
}
