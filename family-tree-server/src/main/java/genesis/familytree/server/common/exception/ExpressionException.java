package genesis.familytree.server.common.exception;

/**
 *
 */
public class ExpressionException extends GenesisException {

    public ExpressionException(String message) {
        super(message);
    }

    public ExpressionException(String message, Throwable cause) {
        super(message, cause);
    }
}
