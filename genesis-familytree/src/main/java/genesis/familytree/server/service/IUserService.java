package genesis.familytree.server.service;

import genesis.familytree.server.entity.SysUser;

/**
 * Created by Tailong on 2016/6/12.
 */
public interface IUserService {
    SysUser getByName(String username);
}
