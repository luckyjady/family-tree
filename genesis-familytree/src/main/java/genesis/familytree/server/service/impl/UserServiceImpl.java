package genesis.familytree.server.service.impl;

import genesis.familytree.server.dao.IUserDao;
import genesis.familytree.server.entity.SysUser;
import genesis.familytree.server.service.IUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 *
 */
@Service
public class UserServiceImpl implements IUserService{

    @Resource private IUserDao userDao;

    @Override
    public SysUser getByName(String username) {
        return userDao.getByName(username);
    }
}
