package genesis.familytree.server.controller;

import genesis.familytree.server.entity.SysUser;
import genesis.familytree.server.request.UserLoginRequest;
import genesis.familytree.server.response.UserLoginResponse;
import genesis.familytree.server.service.IUserService;
import io.swagger.annotations.*;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 *
 */
@Controller
@RequestMapping("/api/user")
@Api(tags = "用户相关", description = "用户相关的接口")
public class UserController {

    @Resource
    private IUserService userService;

    @ApiOperation(value="用户登录", notes="用户登录")
    @ApiResponse(code = 200, message = "成功")
    @ResponseBody
    @RequestMapping(value = "/login", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public UserLoginResponse login(@RequestBody UserLoginRequest request,
                                   HttpServletRequest req) {
        UserLoginResponse response = new UserLoginResponse();
        SysUser user = this.userService.getByName(request.getUsername());
        response.setData(user);
        return response;
    }

}
