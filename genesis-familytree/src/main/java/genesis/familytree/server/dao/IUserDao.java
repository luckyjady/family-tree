package genesis.familytree.server.dao;

import genesis.familytree.server.entity.SysUser;
import org.apache.ibatis.annotations.Mapper;

/**
 *
 */
@Mapper
public interface IUserDao {

    SysUser getByName(String username);
}
