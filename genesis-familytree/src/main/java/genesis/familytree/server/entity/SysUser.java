/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package genesis.familytree.server.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Date;

/**
 * 用户Entity
 *
 * @author ThinkGem
 * @version 2013-12-05
 */
@ApiModel("user")
public class SysUser extends BaseEntity{

    public static final String HASH_ALGORITHM = "SHA-1";
    public static final int HASH_INTERATIONS = 1024;
    private static final int SALT_SIZE = 8;

    @ApiModelProperty("ID")
    private Integer id;

    @ApiModelProperty("登录名")
    private String username;// 登录名

    @ApiModelProperty("密码")
    private String password;// 密码

    @ApiModelProperty("姓名")
    private String nickname;    // 姓名

    @ApiModelProperty("邮箱")
    private String email;    // 邮箱

    @ApiModelProperty("手机")
    private String mobile;    // 手机

    @ApiModelProperty("用户类型")
    private int userType;// 用户类型

    @ApiModelProperty("头像")
    private String icon;    // 头像

    @ApiModelProperty("最后登陆IP")
    private String loginIp;    // 最后登陆IP

    @ApiModelProperty("最后登陆日期")
    private Date loginTime;    // 最后登陆日期

    @JsonIgnore
    private Integer isSystem;

    public boolean isAdmin() {
        return userType == 1;
    }

    public SysUser() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public String getLoginIp() {
        return loginIp;
    }

    public void setLoginIp(String loginIp) {
        this.loginIp = loginIp;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Date getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    public Integer getIsSystem() {
        return isSystem;
    }

    public void setIsSystem(Integer isSystem) {
        this.isSystem = isSystem;
    }
}