package genesis.familytree.server.response;

import genesis.familytree.server.entity.SysUser;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 */
@ApiModel(value = "response", description = "返回值")
public class UserLoginResponse extends BaseResponse{

    @ApiModelProperty(value = "用户信息", dataType = "user")
    private SysUser data;

    public SysUser getData() {
        return data;
    }

    public void setData(SysUser data) {
        this.data = data;
    }

}
