package genesis.familytree.server.response;

/**
 * Created by Tailong on 2016/6/13.
 */
public abstract class BaseResponse {

    private String code;
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
