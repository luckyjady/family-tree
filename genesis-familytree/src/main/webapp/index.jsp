<%@ page import="org.springframework.web.context.support.WebApplicationContextUtils" %>
<%@ page import="org.springframework.context.ApplicationContext" %>
<%@ page import="genesis.familytree.server.service.IUserService" %>
<%@ page import="genesis.familytree.server.entity.SysUser" %><%--
  Created by IntelliJ IDEA.
  User: Tailong
  Date: 2016/6/12
  Time: 11:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>$Title$</title>
  </head>
  <body>

  <%
    ApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(request.getServletContext());
    IUserService srv = appContext.getBean(IUserService.class);
    SysUser user = srv.getByName("admin");

    out.println(user);
  %>
  </body>
</html>
