
USE family_tree;

DROP TABLE IF EXISTS `HOME_USER`;
CREATE TABLE `HOME_USER` (
  `ID` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
  `NICKNAME` VARCHAR(50) NOT NULL COMMENT '昵称',
  `PASSWORD` VARCHAR(50) NOT  NULL COMMENT '密码',
  `MOBILE` VARCHAR(50) NOT NULL COMMENT '手机号',
  `EMAIL` VARCHAR(50) NOT NULL COMMENT '电子邮箱',
  `REAL_NAME` VARCHAR(50) NOT NULL COMMENT '真实姓名',
  `ID_CARD` VARCHAR(50) NOT NULL COMMENT '身份证',
  `GENDER` TINYINT UNSIGNED NOT NULL COMMENT '性别:0保密，1男，2女',
  PRIMARY KEY (`ID`)
)COMMENT '前台用户'
  ENGINE InnoDB DEFAULT CHARSET UTF8;

DROP TABLE IF EXISTS `HOME_FRIEND`;
CREATE TABLE `HOME_FRIEND` (
  FROM_USER_ID INT UNSIGNED NOT NULL COMMENT '',
  TO_USER_ID INT UNSIGNED NOT   NULL COMMENT '',
  PRIMARY KEY (`FROM_USER_ID`, `TO_USER_ID`)
)COMMENT '前台好友关系'
  ENGINE InnoDB DEFAULT CHARSET UTF8;

DROP TABLE IF EXISTS `HOME_RELATE_TYPE`;
CREATE TABLE `HOME_RELATE_TYPE` (
  `ID` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
  `NAME` VARCHAR(50) NOT NULL COMMENT '默认的名称',
  `REMARKS` VARCHAR(50) NOT NULL COMMENT '描述',
  PRIMARY KEY (`ID`)
) COMMENT '前台亲属称呼'
  ENGINE InnoDB DEFAULT CHARSET UTF8;

INSERT INTO `HOME_RELATE_TYPE`(ID, NAME, REMARKS)
VALUES (1, '外婆', ''),(2, '外公', ''),(3, '太姥姥', ''),(4, '太姥爷', ''),(5, '姨姥姥', ''),
  (6, '姨姥爷', ''),(7, '舅姥爷', ''),(8, '舅姥姥', ''),(9, '姑姥姥', ''),(10, '姑姥爷', ''),
  (11, '大姥爷', ''),(12, '大姥姥', ''),(13, '小姥爷', ''),(14, '小姥姥', ''),(15, 'xx姥爷', ''),
  (16, '表舅', ''),(17, '表舅妈', ''),(18, '堂舅', ''),(19, '堂舅妈', ''),(20, '表姨', ''),
  (21, '表姨丈', ''),(22, '堂姨', ''),(23, '堂姨丈', ''),(24, '舅舅', ''),(25, '舅妈', ''),
  (26, '表哥', ''),(27, '表嫂', ''),(28, '表弟', ''),(29, '表弟媳', ''),(30, '表侄子', ''),
  (31, '表侄女', ''),(32, '表姐', ''),(33, '表姐夫', ''),(34, '表妹', ''),(35, '表妹夫', ''),
  (36, '表外甥', ''),(37, '表外甥女', ''),(38, '大舅', ''),(39, '大舅妈', ''),(40, '小舅', ''),
  (41, '小舅妈', ''),(42, '姨妈', ''),(43, '姨丈', ''),(44, '大姨', ''),(45, '大姨父', ''),
  (46, '小姨', ''),(47, '小姨父', ''),(48, '姑妈', ''),(49, '姑丈', ''),(50, '姑母', ''),
  (51, '姑姐', ''),(52, '堂哥', ''),(53, '堂嫂', ''),(54, '堂弟', ''),(55, '堂弟媳', ''),
  (56, '堂侄子', ''),(57, '堂侄女', ''),(58, '堂姐', ''),(59, '堂姐夫', ''),(60, '堂妹', ''),
  (61, '堂妹夫', ''),(62, '堂外甥', ''),(63, '堂外甥女', ''),(64, '伯父', ''),(65, '伯母', ''),
  (66, '叔叔', ''),(67, '婶婶', ''),(68, '表伯', ''),(69, '表叔', ''),(70, '表婶', ''),
  (71, '堂伯', ''),(72, '堂叔', ''),(73, '堂婶', ''),(74, '表姑', ''),(75, '表姑丈', ''),
  (76, '堂姑', ''),(77, '堂姑丈', ''),(78, '岳母', ''),(79, '岳父', ''),(80, '伯岳', ''),
  (81, '伯岳母', ''),(82, '叔岳', ''),(83, '叔岳母', ''),(84, '太岳父', ''),(85, '太岳母', ''),
  (86, '太伯岳', ''),(87, '太伯岳母', ''),(88, '太叔岳', ''),(89, '太叔岳母', ''),(90, '姻伯', ''),
  (91, '姻姆', ''),(92, '姻叔', ''),(93, '姻婶', ''),(94, '大舅哥', ''),(95, '嫂子', ''),
  (96, '小舅子', ''),(97, '弟媳妇', ''),(98, '姻家兄', ''),(99, '姻家弟', ''),(100, '内侄', ''),
  (101, '内侄女', ''),(102, '大姨子', ''),(103, '大姨夫', ''),(104, '小姨子', ''),(105, '小姨夫', ''),
  (106, '妻外甥', ''),(107, '妻外甥女', ''),(108, '婆婆', ''),(109, '公公', ''),(110, '伯翁', ''),
  (111, '伯婆', ''),(112, '叔翁', ''),(113, '叔婆', ''),(114, '祖翁', ''),(115, '祖婆', ''),
  (116, '太公翁', ''),(117, '太奶亲', ''),(118, '堂小弟', ''),(119, '大伯子', ''),(120, '大婶子', ''),
  (121, '小叔子', ''),(122, '小婶子', ''),(123, '侄子', ''),(124, '侄媳', ''),(125, '侄孙', ''),
  (126, '侄孙媳', ''),(127, '侄孙女', ''),(128, '侄女', ''),(129, '侄女婿', ''),(130, '外侄孙', ''),
  (131, '外侄孙女', ''),(132, '大姑子', ''),(133, '大姑夫', ''),(134, '小姑子', ''),(135, '小姑夫', ''),
  (136, '姨甥', ''),(137, '姨甥女', ''),(138, '舅公', ''),(139, '舅婆', ''),(140, '姨婆', ''),
  (141, '姨公', ''),(142, '兄弟', ''),(143, '姐妹', ''),(144, '哥哥', ''),(145, '姻伯父', ''),
  (146, '姻伯母', ''),(147, '弟弟', ''),(148, '弟妹', ''),(149, '姻叔父', ''),(150, '姻叔母', ''),
  (151, '外侄孙子', ''),(152, '姐姐', ''),(153, '姐夫', ''),(154, '妹妹', ''),(155, '妹夫', ''),
  (156, '亲家爷', ''),(157, '亲家娘', ''),(158, '外甥', ''),(159, '外甥媳妇', ''),(160, '甥孙', ''),
  (161, '甥孙女', ''),(162, '外甥女', ''),(163, '外甥女婿', ''),(164, '甥外孙', ''),(165, '甥外孙媳妇', ''),
  (166, '甥外孙女', ''),(167, '甥外孙女婿', ''),(168, '高祖父', ''),(169, '高祖母', ''),(170, '太爷爷', ''),
  (171, '太奶奶', ''),(172, '奶奶', ''),(173, '爷爷', ''),(174, 'xx爷爷', ''),(175, 'xx奶奶', ''),
  (176, '大爷爷', ''),(177, '大奶奶', ''),(178, '小爷爷', ''),(179, '小奶奶', ''),(180, '姑奶奶', ''),
  (181, '姑爷爷', ''),(182, '姨奶奶', ''),(183, '姨爷爷', ''),(184, '舅爷爷', ''),(185, '舅奶奶', ''),
  (186, '妈妈', ''),(187, '爸爸', ''),(188, '老婆', ''),(189, '老公', ''),(190, '儿子', ''),
  (191, '儿媳妇', ''),(192, '姻侄', ''),(193, '姻侄女', ''),(194, '孙子', ''),(195, '孙媳妇', ''),
  (196, '曾孙', ''),(197, '曾孙媳妇', ''),(198, '玄孙', ''),(199, '曾孙女', ''),(200, '孙女', ''),
  (201, '孙女婿', ''),(202, '曾外孙', ''),(203, '曾外孙女', ''),(204, '女儿', ''),(205, '女婿', ''),
  (206, '外孙', ''),(207, '外孙媳', ''),(208, '外曾孙', ''),(209, '外曾孙女', ''),(210, '外孙女', ''),
  (211, '外孙女婿', ''),(212, '外曾外孙', ''),(213, '外曾外孙女', ''),(214, '亲家母', ''),(215, '亲家公', ''),
  (216, '姻兄', ''),(217, '姻弟', ''),(218, '太姻姆', '');

DROP TABLE IF EXISTS `HOME_RELATE_TYPE_REF`;
CREATE TABLE `HOME_RELATE_TYPE_REF` (
  `ID` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
  `EXPRESSION` VARCHAR(50) NOT NULL COMMENT '计算公式',
  `RELATE_TYPE_ID` INT UNSIGNED NOT NULL COMMENT '称呼的ID',
  `REMARKS` VARCHAR(50) NOT NULL COMMENT '描述',
  PRIMARY KEY (`ID`)
) COMMENT '前台亲属称呼'
  ENGINE InnoDB DEFAULT CHARSET UTF8;

INSERT INTO `HOME_RELATE_TYPE_REF`(ID, RELATE_TYPE_ID, EXPRESSION, REMARKS)
VALUES (1, 187, ',144,187,', '哥哥的父亲：父亲')
;


/**
 * 亲属的模板组
 *    group name | default name | customer name
 *       china1  |   父亲       |  爸爸
 *       en1     |   父亲        | father
 */
DROP TABLE IF EXISTS `HOME_RELATE_GROUP`;
CREATE TABLE `HOME_RELATE_GROUP` (
  `ID` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
  `USER_ID` INT UNSIGNED NOT NULL COMMENT '贡献者，0为系统', /* 初始中，系统会提供数个称呼组，但每个地区都有所区别，所以用户可以根据原来的模板或创建全新的称呼组 */
  `DISTRICT_ID` INT UNSIGNED NOT NULL COMMENT '所属地区', /* 将分组绑定地区，可以根据当前用户所在地区推荐最优的称呼组 */
  `NAME` VARCHAR(50) NOT NULL COMMENT '名称',
  PRIMARY KEY (`ID`)
) COMMENT '前台亲属模板组'
  ENGINE InnoDB DEFAULT CHARSET UTF8;
INSERT INTO `HOME_RELATE_GROUP`(ID, USER_ID, DISTRICT_ID, NAME)
    VALUES (1, 0, 1/*中国*/, 'china1'), (2, 0, 2/*北京 */, 'china2'), (3, 0, 3/*广东*/, 'china3');

/**
 * 亲属的模板组
 *    group name | default name | customer name
 *       china1  |   父亲       |  爸爸
 *       en1     |   父亲        | father
 */
DROP TABLE IF EXISTS `HOME_RELATE_GROUP_REF`;
CREATE TABLE `HOME_RELATE_GROUP_REF` (
  `RELAT_GROUP_ID` INT UNSIGNED NOT NULL COMMENT '', /* 称呼组 */
  `RELAT_TYPE_ID` INT UNSIGNED NOT NULL COMMENT '', /* 称呼 */
  `NAME` VARCHAR(50) NOT NULL COMMENT '名称', /* 自定义称呼 */
  PRIMARY KEY (`RELAT_GROUP_ID`, `RELAT_TYPE_ID`)
) COMMENT '前台亲属关系分组'
  ENGINE InnoDB DEFAULT CHARSET UTF8;
INSERT INTO HOME_RELATE_GROUP_REF (RELAT_GROUP_ID, RELAT_TYPE_ID, NAME)
VALUES (3/*china3:广东*/, 187/*父亲*/, '爸爸'),
       (3/*china3:广东*/, 186/*母亲*/, '妈咪'),
       (3/*china3:广东*/, 173/*爷爷*/, '阿公');

DROP TABLE IF EXISTS `HOME_RELATE`;
CREATE TABLE `HOME_RELATE` (
	`ID` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
  `FROM_USER_ID` INT UNSIGNED NOT NULL COMMENT '申请用户',
  `TO_USER_ID` INT UNSIGNED DEFAULT NULL COMMENT '绑定用户', /* 不一定存在， */
  `TO_USER_NAME` INT UNSIGNED NOT NULL COMMENT '绑定的用户名称', /* 当前绑定用户不存在的时候，使用此名称 */
  `FAMILY_TYPE_ID` INT UNSIGNED NOT NULL COMMENT '',
  `FAMILY_TYPE_NAME` INT UNSIGNED NOT NULL COMMENT '',
  `REMARKS` VARCHAR(50) NOT NULL COMMENT '',
  PRIMARY KEY (`ID`)
)COMMENT '前台家庭关系'
  ENGINE InnoDB DEFAULT CHARSET UTF8;


ALTER TABLE `HOME_RELATE`
MODIFY COLUMN `TO_USER_NAME`  varchar(50) NOT NULL COMMENT '绑定的用户名称' AFTER `TO_USER_ID`,
MODIFY COLUMN `FAMILY_TYPE_NAME`  varchar(50) NOT NULL AFTER `FAMILY_TYPE_ID`;

