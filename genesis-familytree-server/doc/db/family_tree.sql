/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50547
Source Host           : localhost:3306
Source Database       : family_tree

Target Server Type    : MYSQL
Target Server Version : 50547
File Encoding         : 65001

Date: 2016-07-29 15:29:08
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for common_auth_code
-- ----------------------------
DROP TABLE IF EXISTS `common_auth_code`;
CREATE TABLE `common_auth_code` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TARGET` varchar(255) NOT NULL COMMENT '发送目标',
  `CONTENT` varchar(255) NOT NULL COMMENT '发送内容',
  `USER_ID` int(11) unsigned DEFAULT '0' COMMENT '发送用户ID',
  `ADD_TIME` datetime DEFAULT NULL COMMENT '发送时间',
  PRIMARY KEY (`ID`),
  KEY `AUTH_CODE_USER_ID` (`USER_ID`),
  CONSTRAINT `AUTH_CODE_USER_ID` FOREIGN KEY (`USER_ID`) REFERENCES `home_user2` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of common_auth_code
-- ----------------------------
INSERT INTO `common_auth_code` VALUES ('1', '13927441616', 'abcde', '1', '2016-07-22 10:49:01');
INSERT INTO `common_auth_code` VALUES ('2', '13927441616', 'aaaa', '1', '2016-07-22 11:36:10');

-- ----------------------------
-- Table structure for c_captcha
-- ----------------------------
DROP TABLE IF EXISTS `c_captcha`;
CREATE TABLE `c_captcha` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '标识',
  `SERVICE_TYPE_ID` int(10) unsigned NOT NULL COMMENT '业务类型',
  `ACCOUNT` varchar(50) NOT NULL COMMENT '账号',
  `CONTENT` varchar(50) NOT NULL COMMENT '内容',
  `USER_ID` int(10) unsigned DEFAULT NULL COMMENT '用户ID',
  `ADD_TIME` datetime NOT NULL COMMENT '添加时间',
  `END_TIME` datetime NOT NULL COMMENT '有效时间',
  `USE_TIME` datetime DEFAULT NULL COMMENT '使用时间',
  `IS_DELETED` tinyint(1) NOT NULL COMMENT '状态',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='验证码';

-- ----------------------------
-- Records of c_captcha
-- ----------------------------

-- ----------------------------
-- Table structure for c_dict
-- ----------------------------
DROP TABLE IF EXISTS `c_dict`;
CREATE TABLE `c_dict` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `DICT_TYPE` varchar(50) DEFAULT NULL COMMENT '字典类型',
  `DICT_LABEL` varchar(200) NOT NULL COMMENT '字典的标签',
  `DICT_VALUE` varchar(500) NOT NULL COMMENT '字典的值',
  `REMARKS` varchar(500) DEFAULT NULL COMMENT '备注',
  `CREATE_BY` int(10) unsigned DEFAULT NULL COMMENT '创建者',
  `CREATE_DATE` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_BY` int(10) unsigned DEFAULT NULL COMMENT '最后更新者',
  `UPDATE_DATE` datetime DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='系统字典';

-- ----------------------------
-- Records of c_dict
-- ----------------------------
INSERT INTO `c_dict` VALUES ('1', 'relate_type', '外家', '', '亲戚关系类型', null, null, null, null);
INSERT INTO `c_dict` VALUES ('2', 'relate_type', '舅家', '', '亲戚关系类型', null, null, null, null);
INSERT INTO `c_dict` VALUES ('3', 'relate_type', '姨家', '', '亲戚关系类型', null, null, null, null);
INSERT INTO `c_dict` VALUES ('4', 'relate_type', '姑家', '', '亲戚关系类型', null, null, null, null);
INSERT INTO `c_dict` VALUES ('5', 'relate_type', '本家', '', '亲戚关系类型', null, null, null, null);
INSERT INTO `c_dict` VALUES ('6', 'relate_type', '岳家', '', '亲戚关系类型', null, null, null, null);
INSERT INTO `c_dict` VALUES ('7', 'relate_type', '婆家', '', '亲戚关系类型', null, null, null, null);
INSERT INTO `c_dict` VALUES ('8', 'relate_type', '旁支', '', '亲戚关系类型', null, null, null, null);
INSERT INTO `c_dict` VALUES ('9', 'relate_type', '内家', '', '亲戚关系类型', null, null, null, null);
INSERT INTO `c_dict` VALUES ('10', 'relate_type', '自家', '', '亲戚关系类型', null, null, null, null);
INSERT INTO `c_dict` VALUES ('11', 'relate_type', '其他', '', '亲戚关系类型', null, null, null, null);

-- ----------------------------
-- Table structure for c_message_history
-- ----------------------------
DROP TABLE IF EXISTS `c_message_history`;
CREATE TABLE `c_message_history` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '标识',
  `MESSAGE_TYPE` tinyint(4) NOT NULL COMMENT '消息类型：1短信，2邮件',
  `TO_ACCOUNT` varchar(50) NOT NULL COMMENT '账号',
  `USER_ID` int(11) DEFAULT NULL COMMENT '用户ID，不一定有',
  `TITLE` varchar(200) DEFAULT NULL COMMENT '消息标题',
  `CONTENT` text NOT NULL COMMENT '消息内容',
  `ADD_TIME` datetime NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='消息发送记录';

-- ----------------------------
-- Records of c_message_history
-- ----------------------------

-- ----------------------------
-- Table structure for c_message_template
-- ----------------------------
DROP TABLE IF EXISTS `c_message_template`;
CREATE TABLE `c_message_template` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '标识',
  `SERVICE_TYPE_ID` int(10) unsigned NOT NULL COMMENT '业务类型',
  `MESSAGE_TYPE` int(10) unsigned NOT NULL COMMENT '消息类型：1短信，2邮件',
  `TITLE` varchar(200) NOT NULL COMMENT '标识',
  `CONTENT` text NOT NULL COMMENT '内容',
  `ADD_TIME` datetime NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='消息模板';

-- ----------------------------
-- Records of c_message_template
-- ----------------------------

-- ----------------------------
-- Table structure for c_param
-- ----------------------------
DROP TABLE IF EXISTS `c_param`;
CREATE TABLE `c_param` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PARAM_CODE` varchar(50) NOT NULL COMMENT '参数编码',
  `PARAM_NAME` varchar(200) NOT NULL COMMENT '参数的名字',
  `PARAM_VALUE` varchar(500) NOT NULL COMMENT '参数值',
  `PARAM_GROUP` varchar(50) DEFAULT NULL COMMENT '参数分组，方便获取',
  `IS_SYSTEM` tinyint(1) NOT NULL COMMENT '是否系统参数，系统参数只能修改PARAM_VALUE',
  `IS_DELETED` tinyint(1) NOT NULL COMMENT '状态',
  `REMARKS` varchar(500) DEFAULT NULL COMMENT '备注',
  `CREATE_BY` int(10) unsigned DEFAULT NULL COMMENT '创建者',
  `CREATE_DATE` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_BY` int(10) unsigned DEFAULT NULL COMMENT '最后更新者',
  `UPDATE_DATE` datetime DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `PARAM_CODE` (`PARAM_CODE`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='系统参数';

-- ----------------------------
-- Records of c_param
-- ----------------------------
INSERT INTO `c_param` VALUES ('1', 'CAPTCHA_SYSTEM_ENABLED', '后台是否全局开启验证码', '0', '', '1', '0', null, null, null, null, null);
INSERT INTO `c_param` VALUES ('2', 'CAPTCHA_HOME_ENABLED', '前台是否全局开启验证码', '0', '', '1', '0', null, null, null, null, null);
INSERT INTO `c_param` VALUES ('3', 'CAPTCHA_URL_ADMIN_LOGIN_ENABLED', '后台登录是否开启验证码', '1', '', '0', '0', null, null, null, null, null);

-- ----------------------------
-- Table structure for c_service_type
-- ----------------------------
DROP TABLE IF EXISTS `c_service_type`;
CREATE TABLE `c_service_type` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '标识',
  `TYPE_CODE` varchar(30) NOT NULL COMMENT '业务类型编码',
  `TYPE_NAME` varchar(30) NOT NULL COMMENT '业务类型名称',
  `IS_ENABLED` tinyint(1) NOT NULL COMMENT '是否启用：0禁止，1开启',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `TYPE_CODE` (`TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='业务类型';

-- ----------------------------
-- Records of c_service_type
-- ----------------------------

-- ----------------------------
-- Table structure for c_service_type_config
-- ----------------------------
DROP TABLE IF EXISTS `c_service_type_config`;
CREATE TABLE `c_service_type_config` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '标识',
  `SERVICE_TYPE_ID` int(10) unsigned NOT NULL COMMENT '业务类型ID',
  `CONFIG_CODE` varchar(30) NOT NULL COMMENT '配置编码',
  `CONFIG_NAME` varchar(30) NOT NULL COMMENT '配置的名字',
  `IS_ENABLED` tinyint(1) NOT NULL COMMENT '是否开启：0禁止，1开启',
  `DEFAULT_VALUE` tinyint(1) NOT NULL COMMENT '默认值：0不发送，1发送',
  `USER_CONFIG` tinyint(1) NOT NULL COMMENT '是否用户可配置：0不可更改，2可更改',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SERVICE_TYPE_ID` (`SERVICE_TYPE_ID`,`CONFIG_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='业务类型配置';

-- ----------------------------
-- Records of c_service_type_config
-- ----------------------------

-- ----------------------------
-- Table structure for home_base_relate
-- ----------------------------
DROP TABLE IF EXISTS `home_base_relate`;
CREATE TABLE `home_base_relate` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `SIGN` varchar(10) NOT NULL COMMENT '关系符号',
  `LEVEL` int(1) DEFAULT NULL COMMENT '辈份关系',
  `REMARK` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='基础关系表，用来计算关系';

-- ----------------------------
-- Records of home_base_relate
-- ----------------------------
INSERT INTO `home_base_relate` VALUES ('1', 'F', '1', 'Father 父亲');
INSERT INTO `home_base_relate` VALUES ('2', 'M', '1', 'Mother 母亲');
INSERT INTO `home_base_relate` VALUES ('3', 'S', '-1', 'Son 儿子');
INSERT INTO `home_base_relate` VALUES ('4', 'D', '-1', 'Daughter 女儿');
INSERT INTO `home_base_relate` VALUES ('5', 'OS', '0', 'Old Sister 姐姐');
INSERT INTO `home_base_relate` VALUES ('6', 'LS', '0', 'Little Sister 妹妹');
INSERT INTO `home_base_relate` VALUES ('7', 'OB', '0', 'Old Brother 哥哥');
INSERT INTO `home_base_relate` VALUES ('8', 'LB', '0', 'Litter Brother 弟弟');
INSERT INTO `home_base_relate` VALUES ('9', 'W', '0', 'Wife 妻子');
INSERT INTO `home_base_relate` VALUES ('10', 'H', '0', 'Husband 丈夫');

-- ----------------------------
-- Table structure for home_friend
-- ----------------------------
DROP TABLE IF EXISTS `home_friend`;
CREATE TABLE `home_friend` (
  `FROM_USER_ID` int(10) unsigned NOT NULL,
  `TO_USER_ID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`FROM_USER_ID`,`TO_USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='前台好友关系';

-- ----------------------------
-- Records of home_friend
-- ----------------------------

-- ----------------------------
-- Table structure for home_relate
-- ----------------------------
DROP TABLE IF EXISTS `home_relate`;
CREATE TABLE `home_relate` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `FROM_USER_ID` int(10) unsigned NOT NULL COMMENT '申请用户',
  `TO_USER_ID` int(10) unsigned DEFAULT NULL COMMENT '绑定用户',
  `TO_USER_NAME` varchar(50) NOT NULL COMMENT '绑定的用户名称',
  `FAMILY_TYPE_ID` int(10) unsigned NOT NULL COMMENT '对应home_base_relate表',
  `FAMILY_TYPE_NAME` varchar(50) NOT NULL,
  `REMARKS` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='前台家庭关系';

-- ----------------------------
-- Records of home_relate
-- ----------------------------
INSERT INTO `home_relate` VALUES ('1', '1', '3', 'to妈妈', '2', '妈妈', '备注');
INSERT INTO `home_relate` VALUES ('2', '1', '2', 'to爸爸', '1', '爸爸', '备注');
INSERT INTO `home_relate` VALUES ('3', '2', '4', 'to爷爷', '1', '姜爷爷 爸爸', '');
INSERT INTO `home_relate` VALUES ('4', '2', '5', 'to奶奶', '2', '姜奶奶 ', '');

-- ----------------------------
-- Table structure for home_relate_group
-- ----------------------------
DROP TABLE IF EXISTS `home_relate_group`;
CREATE TABLE `home_relate_group` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `USER_ID` int(10) unsigned NOT NULL COMMENT '贡献者，0为系统',
  `DISTRICT_ID` int(10) unsigned NOT NULL COMMENT '所属地区',
  `NAME` varchar(50) NOT NULL COMMENT '名称',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='前台亲属模板组';

-- ----------------------------
-- Records of home_relate_group
-- ----------------------------
INSERT INTO `home_relate_group` VALUES ('1', '0', '1', 'china1');
INSERT INTO `home_relate_group` VALUES ('2', '0', '2', 'china2');
INSERT INTO `home_relate_group` VALUES ('3', '0', '3', 'china3');

-- ----------------------------
-- Table structure for home_relate_group_ref
-- ----------------------------
DROP TABLE IF EXISTS `home_relate_group_ref`;
CREATE TABLE `home_relate_group_ref` (
  `RELAT_GROUP_ID` int(10) unsigned NOT NULL,
  `RELAT_TYPE_ID` int(10) unsigned NOT NULL,
  `NAME` varchar(50) NOT NULL COMMENT '名称',
  PRIMARY KEY (`RELAT_GROUP_ID`,`RELAT_TYPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='前台亲属关系分组';

-- ----------------------------
-- Records of home_relate_group_ref
-- ----------------------------
INSERT INTO `home_relate_group_ref` VALUES ('3', '173', '阿公');
INSERT INTO `home_relate_group_ref` VALUES ('3', '186', '妈咪');
INSERT INTO `home_relate_group_ref` VALUES ('3', '187', '爸爸');

-- ----------------------------
-- Table structure for home_relate_type
-- ----------------------------
DROP TABLE IF EXISTS `home_relate_type`;
CREATE TABLE `home_relate_type` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `TYPE` int(10) unsigned DEFAULT NULL COMMENT '关系类型，对应系统字典表',
  `CHAIN` varchar(255) DEFAULT NULL COMMENT '关系链构成 对应 home_base_relate &符号表示年龄',
  `NAME` varchar(255) DEFAULT NULL COMMENT '关系名称',
  `REMARK` varchar(255) DEFAULT NULL COMMENT '注释',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=289 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of home_relate_type
-- ----------------------------
INSERT INTO `home_relate_type` VALUES ('1', '10', '', '自己', '[\'自己\']');
INSERT INTO `home_relate_type` VALUES ('2', '1', 'M,M', '外婆', '[\'外婆\'\'姥姥\']');
INSERT INTO `home_relate_type` VALUES ('3', '1', 'M,F', '外公', '[\'外公\'\'姥爷\']');
INSERT INTO `home_relate_type` VALUES ('4', '1', 'M,M,M', '太姥姥', '[\'太姥姥\']');
INSERT INTO `home_relate_type` VALUES ('5', '1', 'M,M,F', '太姥爷', '[\'太姥爷\']');
INSERT INTO `home_relate_type` VALUES ('6', '1', 'M,M,XS', '姨姥姥', '[\'姨姥姥\'\'姨婆\']');
INSERT INTO `home_relate_type` VALUES ('7', '1', 'M,M,XS,W', '姨姥爷', '[\'姨姥爷\']');
INSERT INTO `home_relate_type` VALUES ('8', '1', 'M,M,XB', '舅姥爷', '[\'舅姥爷\'\'舅外祖父\'\'舅外公\'\'舅公\']');
INSERT INTO `home_relate_type` VALUES ('9', '1', 'M,M,XB,W', '舅姥姥', '[\'舅姥姥\']');
INSERT INTO `home_relate_type` VALUES ('10', '1', 'M,F,M', '太姥姥', '[\'太姥姥\'\'外太祖父\']');
INSERT INTO `home_relate_type` VALUES ('11', '1', 'M,F,F', '太姥爷', '[\'太姥爷\'\'外太祖母\']');
INSERT INTO `home_relate_type` VALUES ('12', '1', 'M,F,XS', '姑姥姥', '[\'姑姥姥\'\'外太姑母\']');
INSERT INTO `home_relate_type` VALUES ('13', '1', 'M,F,XS,H', '姑姥爷', '[\'姑姥爷\'\'外太姑父\']');
INSERT INTO `home_relate_type` VALUES ('14', '1', 'M,F,OB', '大姥爷', '[\'大姥爷\'\'外伯祖\']');
INSERT INTO `home_relate_type` VALUES ('15', '1', 'M,F,OB,W', '大姥姥', '[\'大姥姥\'\'外姆婆\']');
INSERT INTO `home_relate_type` VALUES ('16', '1', 'M,F,LB', '小姥爷', '[\'小姥爷\'\'外叔祖\']');
INSERT INTO `home_relate_type` VALUES ('17', '1', 'M,F,LB,W', '小姥姥', '[\'小姥姥\'\'外姆婆\']');
INSERT INTO `home_relate_type` VALUES ('18', '1', 'M,F,XB', 'XX姥爷', '[\'XX姥爷\'\'二姥爷\'\'三姥爷\']');
INSERT INTO `home_relate_type` VALUES ('19', '1', 'M,F,XS,S', '表舅', '[\'表舅\']');
INSERT INTO `home_relate_type` VALUES ('20', '1', 'M,F,XS,S,W', '表舅妈', '[\'表舅妈\']');
INSERT INTO `home_relate_type` VALUES ('21', '1', 'M,M,XS,S', '表舅', '[\'表舅\']');
INSERT INTO `home_relate_type` VALUES ('22', '1', 'M,M,XS,S,W', '表舅妈', '[\'表舅妈\']');
INSERT INTO `home_relate_type` VALUES ('23', '1', 'M,M,XB,S', '表舅', '[\'表舅\']');
INSERT INTO `home_relate_type` VALUES ('24', '1', 'M,M,XB,S,W', '表舅妈', '[\'表舅妈\']');
INSERT INTO `home_relate_type` VALUES ('25', '1', 'M,F,XB,S', '堂舅', '[\'堂舅\']');
INSERT INTO `home_relate_type` VALUES ('26', '1', 'M,F,XB,S,W', '堂舅妈', '[\'堂舅妈\']');
INSERT INTO `home_relate_type` VALUES ('27', '1', 'M,M,XS,D', '表姨', '[\'表姨\']');
INSERT INTO `home_relate_type` VALUES ('28', '1', 'M,M,XS,D,H', '表姨丈', '[\'表姨丈\']');
INSERT INTO `home_relate_type` VALUES ('29', '1', 'M,M,XB,D', '表姨', '[\'表姨\']');
INSERT INTO `home_relate_type` VALUES ('30', '1', 'M,M,XB,D,H', '表姨丈', '[\'表姨丈\']');
INSERT INTO `home_relate_type` VALUES ('31', '1', 'M,F,XS,D', '表姨', '[\'表姨\']');
INSERT INTO `home_relate_type` VALUES ('32', '1', 'M,F,XS,D,H', '表姨丈', '[\'表姨丈\']');
INSERT INTO `home_relate_type` VALUES ('33', '1', 'M,F,XB,D', '堂姨', '[\'堂姨\']');
INSERT INTO `home_relate_type` VALUES ('34', '1', 'M,F,XB,D,H', '堂姨丈', '[\'堂姨丈\']');
INSERT INTO `home_relate_type` VALUES ('35', '2', 'M,XB', '舅舅', '[\'舅舅\'\'舅父\'\'舅\'\'娘舅\'\'二舅\'\'三舅\']');
INSERT INTO `home_relate_type` VALUES ('36', '2', 'M,XB,W', '舅妈', '[\'舅妈\'\'舅母\'\'妗妗\'\'二舅妈\'\'三舅妈\']');
INSERT INTO `home_relate_type` VALUES ('37', '2', 'M,XB,S&O', '表哥(舅家)', '[\'表哥(舅家)\'\'表哥\']');
INSERT INTO `home_relate_type` VALUES ('38', '2', 'M,XB,S&O,W', '表嫂(舅家)', '[\'表嫂(舅家)\'\'表嫂\']');
INSERT INTO `home_relate_type` VALUES ('39', '2', 'M,XB,S&L', '表弟(舅家)', '[\'表弟(舅家)\'\'表弟\']');
INSERT INTO `home_relate_type` VALUES ('40', '2', 'M,XB,S&L,W', '表弟媳(舅家)', '[\'表弟媳(舅家)\'\'表弟媳\']');
INSERT INTO `home_relate_type` VALUES ('41', '2', 'M,XB,S,S', '表侄子', '[\'表侄子\']');
INSERT INTO `home_relate_type` VALUES ('42', '2', 'M,XB,S,D', '表侄女', '[\'表侄女\']');
INSERT INTO `home_relate_type` VALUES ('43', '2', 'M,XB,D&O', '表姐(舅家)', '[\'表姐(舅家)\'\'表姐\']');
INSERT INTO `home_relate_type` VALUES ('44', '2', 'M,XB,D&O,H', '表姐夫(舅家)', '[\'表姐夫(舅家)\'\'表姐夫\'\'表姐丈\']');
INSERT INTO `home_relate_type` VALUES ('45', '2', 'M,XB,D&L', '表妹(舅家)', '[\'表妹(舅家)\'\'表妹\']');
INSERT INTO `home_relate_type` VALUES ('46', '2', 'M,XB,D&L,H', '表妹夫(舅家)', '[\'表妹夫(舅家)\'\'表妹夫\']');
INSERT INTO `home_relate_type` VALUES ('47', '2', 'M,XB,D,S', '表外甥', '[\'表外甥\']');
INSERT INTO `home_relate_type` VALUES ('48', '2', 'M,XB,D,D', '表外甥女', '[\'表外甥女\']');
INSERT INTO `home_relate_type` VALUES ('49', '2', 'M,OB', '大舅', '[\'大舅\']');
INSERT INTO `home_relate_type` VALUES ('50', '2', 'M,OB,W', '大舅妈', '[\'大舅妈\']');
INSERT INTO `home_relate_type` VALUES ('51', '2', 'M,LB', '小舅', '[\'小舅\']');
INSERT INTO `home_relate_type` VALUES ('52', '2', 'M,LB,W', '小舅妈', '[\'小舅妈\']');
INSERT INTO `home_relate_type` VALUES ('53', '3', 'M,XS', '姨妈', '[\'姨妈\'\'姨母\'\'姨姨\'\'姨娘\'\'姨\'\'二姨\'\'三姨\']');
INSERT INTO `home_relate_type` VALUES ('54', '3', 'M,XS,H', '姨丈', '[\'姨丈\'\'姨父\'\'二姨父\'\'三姨父\']');
INSERT INTO `home_relate_type` VALUES ('55', '3', 'M,XS,S&O', '表哥(姨家)', '[\'表哥(姨家)\'\'表哥\']');
INSERT INTO `home_relate_type` VALUES ('56', '3', 'M,XS,S&O,W', '表嫂(姨家)', '[\'表嫂(姨家)\'\'表嫂\']');
INSERT INTO `home_relate_type` VALUES ('57', '3', 'M,XS,S&L', '表弟(姨家)', '[\'表弟(姨家)\'\'表弟\']');
INSERT INTO `home_relate_type` VALUES ('58', '3', 'M,XS,S&L,W', '表弟媳(姨家)', '[\'表弟媳(姨家)\'\'表弟媳\']');
INSERT INTO `home_relate_type` VALUES ('59', '3', 'M,XS,S,S', '表侄子', '[\'表侄子\']');
INSERT INTO `home_relate_type` VALUES ('60', '3', 'M,XS,S,D', '表侄女', '[\'表侄女\']');
INSERT INTO `home_relate_type` VALUES ('61', '3', 'M,XS,D&O', '表姐(姨家)', '[\'表姐(姨家)\'\'表姐\']');
INSERT INTO `home_relate_type` VALUES ('62', '3', 'M,XS,D&O,H', '表姐夫(姨家)', '[\'表姐夫(姨家)\'\'表姐夫\'\'表姐丈\']');
INSERT INTO `home_relate_type` VALUES ('63', '3', 'M,XS,D&L', '表妹(姨家)', '[\'表妹(姨家)\'\'表妹\']');
INSERT INTO `home_relate_type` VALUES ('64', '3', 'M,XS,D&L,H', '表妹夫(姨家)', '[\'表妹夫(姨家)\'\'表妹夫\']');
INSERT INTO `home_relate_type` VALUES ('65', '3', 'M,XS,D,S', '表外甥', '[\'表外甥\']');
INSERT INTO `home_relate_type` VALUES ('66', '3', 'M,XS,D,D', '表外甥女', '[\'表外甥女\']');
INSERT INTO `home_relate_type` VALUES ('67', '3', 'M,OS', '大姨', '[\'大姨\'\'大姨妈\']');
INSERT INTO `home_relate_type` VALUES ('68', '3', 'M,OS,H', '大姨父', '[\'大姨父\'\'大姨丈\'\'大姨夫\']');
INSERT INTO `home_relate_type` VALUES ('69', '3', 'M,LS', '小姨', '[\'小姨\'\'小姨妈\']');
INSERT INTO `home_relate_type` VALUES ('70', '3', 'M,LS,H', '小姨父', '[\'小姨父\'\'小姨丈\'\'小姨夫\']');
INSERT INTO `home_relate_type` VALUES ('71', '4', 'F,XS', '姑妈', '[\'姑妈\'\'姑母\'\'姑姑\'\'姑\']');
INSERT INTO `home_relate_type` VALUES ('72', '4', 'F,XS,H', '姑丈', '[\'姑丈\'\'姑父\']');
INSERT INTO `home_relate_type` VALUES ('73', '4', 'F,XS,S&O', '表哥(姑家)', '[\'表哥(姑家)\'\'表哥\']');
INSERT INTO `home_relate_type` VALUES ('74', '4', 'F,XS,S&O,W', '表嫂(姑家)', '[\'表嫂(姑家)\'\'表嫂\']');
INSERT INTO `home_relate_type` VALUES ('75', '4', 'F,XS,S&L', '表弟(姑家)', '[\'表弟(姑家)\'\'表弟\']');
INSERT INTO `home_relate_type` VALUES ('76', '4', 'F,XS,S&L,W', '表弟媳(姑家)', '[\'表弟媳(姑家)\'\'表弟媳\']');
INSERT INTO `home_relate_type` VALUES ('77', '4', 'F,XS,S,S', '表侄子', '[\'表侄子\']');
INSERT INTO `home_relate_type` VALUES ('78', '4', 'F,XS,S,D', '表侄女', '[\'表侄女\']');
INSERT INTO `home_relate_type` VALUES ('79', '4', 'F,XS,D&O', '表姐(姑家)', '[\'表姐(姑家)\'\'表姐\']');
INSERT INTO `home_relate_type` VALUES ('80', '4', 'F,XS,D&O,H', '表姐夫(姑家)', '[\'表姐夫(姑家)\'\'表姐夫\'\'表姐丈\']');
INSERT INTO `home_relate_type` VALUES ('81', '4', 'F,XS,D&L', '表妹(姑家)', '[\'表妹(姑家)\'\'表妹\']');
INSERT INTO `home_relate_type` VALUES ('82', '4', 'F,XS,D&L,H', '表妹夫(姑家)', '[\'表妹夫(姑家)\'\'表妹夫\']');
INSERT INTO `home_relate_type` VALUES ('83', '4', 'F,XS,D,S', '表外甥', '[\'表外甥\']');
INSERT INTO `home_relate_type` VALUES ('84', '4', 'F,XS,D,D', '表外甥女', '[\'表外甥女\']');
INSERT INTO `home_relate_type` VALUES ('85', '4', 'F,OS', '姑母', '[\'姑母\']');
INSERT INTO `home_relate_type` VALUES ('86', '4', 'F,LS', '姑姐', '[\'姑姐\']');
INSERT INTO `home_relate_type` VALUES ('87', '5', 'F,XB,S&O', '堂哥', '[\'堂哥\'\'堂兄\']');
INSERT INTO `home_relate_type` VALUES ('88', '5', 'F,XB,S&O,W', '堂嫂', '[\'堂嫂\']');
INSERT INTO `home_relate_type` VALUES ('89', '5', 'F,XB,S&L', '堂弟', '[\'堂弟\']');
INSERT INTO `home_relate_type` VALUES ('90', '5', 'F,XB,S&L,W', '堂弟媳', '[\'堂弟媳\']');
INSERT INTO `home_relate_type` VALUES ('91', '5', 'F,XB,S,S', '堂侄子', '[\'堂侄子\']');
INSERT INTO `home_relate_type` VALUES ('92', '5', 'F,XB,S,D', '堂侄女', '[\'堂侄女\']');
INSERT INTO `home_relate_type` VALUES ('93', '5', 'F,XB,D&O', '堂姐', '[\'堂姐\']');
INSERT INTO `home_relate_type` VALUES ('94', '5', 'F,XB,D&O,H', '堂姐夫', '[\'堂姐夫\']');
INSERT INTO `home_relate_type` VALUES ('95', '5', 'F,XB,D&L', '堂妹', '[\'堂妹\']');
INSERT INTO `home_relate_type` VALUES ('96', '5', 'F,XB,D&L,H', '堂妹夫', '[\'堂妹夫\']');
INSERT INTO `home_relate_type` VALUES ('97', '5', 'F,XB,D,S', '堂外甥', '[\'堂外甥\']');
INSERT INTO `home_relate_type` VALUES ('98', '5', 'F,XB,D,D', '堂外甥女', '[\'堂外甥女\']');
INSERT INTO `home_relate_type` VALUES ('99', '5', 'F,OB', '伯父', '[\'伯父\'\'伯伯\'\'大伯\'\'二伯\'\'三伯\']');
INSERT INTO `home_relate_type` VALUES ('100', '5', 'F,OB,W', '伯母', '[\'伯母\'\'大娘\']');
INSERT INTO `home_relate_type` VALUES ('101', '5', 'F,LB', '叔叔', '[\'叔叔\'\'叔父\'\'叔\'\'二叔\'\'三叔\']');
INSERT INTO `home_relate_type` VALUES ('102', '5', 'F,LB,W', '婶婶', '[\'婶婶\'\'婶母\'\'婶\']');
INSERT INTO `home_relate_type` VALUES ('103', '5', 'F,F,XS,S&O,', '表伯', '[\'表伯\']');
INSERT INTO `home_relate_type` VALUES ('104', '5', 'F,F,XS,S&L,', '表叔', '[\'表叔\']');
INSERT INTO `home_relate_type` VALUES ('105', '5', 'F,F,XS,S,W', '表婶', '[\'表婶\']');
INSERT INTO `home_relate_type` VALUES ('106', '5', 'F,M,XS,S&O', '表伯', '[\'表伯\']');
INSERT INTO `home_relate_type` VALUES ('107', '5', 'F,M,XS,S&L', '表叔', '[\'表叔\']');
INSERT INTO `home_relate_type` VALUES ('108', '5', 'F,M,XS,S,W', '表婶', '[\'表婶\']');
INSERT INTO `home_relate_type` VALUES ('109', '5', 'F,M,XB,S&O', '表伯', '[\'表伯\']');
INSERT INTO `home_relate_type` VALUES ('110', '5', 'F,M,XB,S&L', '表叔', '[\'表叔\']');
INSERT INTO `home_relate_type` VALUES ('111', '5', 'F,M,XB,S,W', '表婶', '[\'表婶\']');
INSERT INTO `home_relate_type` VALUES ('112', '5', 'F,F,XB,S&O', '堂伯', '[\'堂伯\']');
INSERT INTO `home_relate_type` VALUES ('113', '5', 'F,F,XB,S&L', '堂叔', '[\'堂叔\']');
INSERT INTO `home_relate_type` VALUES ('114', '5', 'F,F,XB,S,W', '堂婶', '[\'堂婶\']');
INSERT INTO `home_relate_type` VALUES ('115', '5', 'F,F,XS,D', '表姑', '[\'表姑\']');
INSERT INTO `home_relate_type` VALUES ('116', '5', 'F,F,XS,D,H', '表姑丈', '[\'表姑丈\']');
INSERT INTO `home_relate_type` VALUES ('117', '5', 'F,M,XS,D', '表姑', '[\'表姑\']');
INSERT INTO `home_relate_type` VALUES ('118', '5', 'F,M,XS,D,H', '表姑丈', '[\'表姑丈\']');
INSERT INTO `home_relate_type` VALUES ('119', '5', 'F,M,XB,D', '表姑', '[\'表姑\']');
INSERT INTO `home_relate_type` VALUES ('120', '5', 'F,M,XB,D,H', '表姑丈', '[\'表姑丈\']');
INSERT INTO `home_relate_type` VALUES ('121', '5', 'F,F,XB,D', '堂姑', '[\'堂姑\']');
INSERT INTO `home_relate_type` VALUES ('122', '5', 'F,F,XB,D,H', '堂姑丈', '[\'堂姑丈\']');
INSERT INTO `home_relate_type` VALUES ('123', '6', 'W,M', '岳母', '[\'岳母\'\'丈母娘\']');
INSERT INTO `home_relate_type` VALUES ('124', '6', 'W,F', '岳父', '[\'岳父\'\'老丈人\'\'丈人\'\'泰山\'\'妻父\']');
INSERT INTO `home_relate_type` VALUES ('125', '6', 'W,F,OB', '伯岳', '[\'伯岳\']');
INSERT INTO `home_relate_type` VALUES ('126', '6', 'W,F,OB,W', '伯岳母', '[\'伯岳母\']');
INSERT INTO `home_relate_type` VALUES ('127', '6', 'W,F,LB', '叔岳', '[\'叔岳\']');
INSERT INTO `home_relate_type` VALUES ('128', '6', 'W,F,LB,W', '叔岳母', '[\'叔岳母\']');
INSERT INTO `home_relate_type` VALUES ('129', '6', 'W,F,F', '太岳父', '[\'太岳父\']');
INSERT INTO `home_relate_type` VALUES ('130', '6', 'W,F,M', '太岳母', '[\'太岳母\']');
INSERT INTO `home_relate_type` VALUES ('131', '6', 'W,F,F,OB', '太伯岳', '[\'太伯岳\']');
INSERT INTO `home_relate_type` VALUES ('132', '6', 'W,F,F,OB,W', '太伯岳母', '[\'太伯岳母\']');
INSERT INTO `home_relate_type` VALUES ('133', '6', 'W,F,F,LB,', '太叔岳', '[\'太叔岳\']');
INSERT INTO `home_relate_type` VALUES ('134', '6', 'W,F,F,LB,W', '太叔岳母', '[\'太叔岳母\']');
INSERT INTO `home_relate_type` VALUES ('135', '6', 'W,F,F,XB,S&O', '姻伯', '[\'姻伯\']');
INSERT INTO `home_relate_type` VALUES ('136', '6', 'W,F,F,XB,S&O,W', '姻姆', '[\'姻姆\']');
INSERT INTO `home_relate_type` VALUES ('137', '6', 'W,F,F,XB,S&L', '姻叔', '[\'姻叔\']');
INSERT INTO `home_relate_type` VALUES ('138', '6', 'W,F,F,XB,S&L,W', '姻婶', '[\'姻婶\']');
INSERT INTO `home_relate_type` VALUES ('139', '6', 'W,OB', '大舅哥', '[\'大舅哥\'\'大舅子\'\'内兄\']');
INSERT INTO `home_relate_type` VALUES ('140', '6', 'W,OB,W', '嫂子', '[\'嫂子\'\'大妗子\'\'内嫂\']');
INSERT INTO `home_relate_type` VALUES ('141', '6', 'W,LB', '小舅子', '[\'小舅子\'\'内弟\']');
INSERT INTO `home_relate_type` VALUES ('142', '6', 'W,LB,W', '弟媳妇', '[\'弟媳妇\'\'小妗子\']');
INSERT INTO `home_relate_type` VALUES ('143', '6', 'W,F,XB,S&O', '姻家兄', '[\'姻家兄\']');
INSERT INTO `home_relate_type` VALUES ('144', '6', 'W,F,XB,S&L', '姻家弟', '[\'姻家弟\']');
INSERT INTO `home_relate_type` VALUES ('145', '6', 'W,XB,S', '内侄', '[\'内侄\'\'妻侄\']');
INSERT INTO `home_relate_type` VALUES ('146', '6', 'W,XB,D', '内侄女', '[\'内侄女\'\'妻侄女\']');
INSERT INTO `home_relate_type` VALUES ('147', '6', 'W,OS', '大姨子', '[\'大姨子\'\'大姨姐\'\'妻姐\']');
INSERT INTO `home_relate_type` VALUES ('148', '6', 'W,OS,H', '大姨夫', '[\'大姨夫\'\'襟兄\'\'连襟\']');
INSERT INTO `home_relate_type` VALUES ('149', '6', 'W,LS', '小姨子', '[\'小姨子\'\'小姨姐\'\'妻妹\'\'小妹儿\']');
INSERT INTO `home_relate_type` VALUES ('150', '6', 'W,LS,H', '小姨夫', '[\'小姨夫\'\'襟弟\'\'连襟\']');
INSERT INTO `home_relate_type` VALUES ('151', '6', 'W,XS,S', '妻外甥', '[\'妻外甥\']');
INSERT INTO `home_relate_type` VALUES ('152', '6', 'W,XS,D', '妻外甥女', '[\'妻外甥女\']');
INSERT INTO `home_relate_type` VALUES ('153', '7', 'H,M', '婆婆', '[\'婆婆\']');
INSERT INTO `home_relate_type` VALUES ('154', '7', 'H,F', '公公', '[\'公公\']');
INSERT INTO `home_relate_type` VALUES ('155', '7', 'H,F,OB', '伯翁', '[\'伯翁\']');
INSERT INTO `home_relate_type` VALUES ('156', '7', 'H,F,OB,W', '伯婆', '[\'伯婆\']');
INSERT INTO `home_relate_type` VALUES ('157', '7', 'H,F,LB', '叔翁', '[\'叔翁\']');
INSERT INTO `home_relate_type` VALUES ('158', '7', 'H,F,LB,W', '叔婆', '[\'叔婆\']');
INSERT INTO `home_relate_type` VALUES ('159', '7', 'H,F,F', '祖翁', '[\'祖翁\']');
INSERT INTO `home_relate_type` VALUES ('160', '7', 'H,F,M', '祖婆', '[\'祖婆\']');
INSERT INTO `home_relate_type` VALUES ('161', '7', 'H,F,F,F', '太公翁', '[\'太公翁\']');
INSERT INTO `home_relate_type` VALUES ('162', '7', 'H,F,F,M', '太奶亲', '[\'太奶亲\']');
INSERT INTO `home_relate_type` VALUES ('163', '7', 'H,F,XB,S&O', '堂哥', '[\'堂哥\'\'堂兄\']');
INSERT INTO `home_relate_type` VALUES ('164', '7', 'H,F,XB,S&O,W', '堂嫂', '[\'堂嫂\']');
INSERT INTO `home_relate_type` VALUES ('165', '7', 'H,F,XB,S&L', '堂弟', '[\'堂弟\']');
INSERT INTO `home_relate_type` VALUES ('166', '7', 'H,F,XB,S&L,W', '堂小弟', '[\'堂小弟\']');
INSERT INTO `home_relate_type` VALUES ('167', '7', 'H,OB', '大伯子', '[\'大伯子\'\'夫兄\']');
INSERT INTO `home_relate_type` VALUES ('168', '7', 'H,OB,W', '大婶子', '[\'大婶子\'\'大伯娘\'\'大嫂\'\'夫兄嫂\'\'妯娌\']');
INSERT INTO `home_relate_type` VALUES ('169', '7', 'H,LB', '小叔子', '[\'小叔子\']');
INSERT INTO `home_relate_type` VALUES ('170', '7', 'H,LB,W', '小婶子', '[\'小婶子\'\'妯娌\']');
INSERT INTO `home_relate_type` VALUES ('171', '7', 'H,XB,S', '侄子', '[\'侄子\']');
INSERT INTO `home_relate_type` VALUES ('172', '7', 'H,XB,S,W', '侄媳', '[\'侄媳\'\'侄媳妇\']');
INSERT INTO `home_relate_type` VALUES ('173', '7', 'H,XB,S,S', '侄孙', '[\'侄孙\'\'侄孙子\']');
INSERT INTO `home_relate_type` VALUES ('174', '7', 'H,XB,S,S,W', '侄孙媳', '[\'侄孙媳\']');
INSERT INTO `home_relate_type` VALUES ('175', '7', 'H,XB,S,D', '侄孙女', '[\'侄孙女\']');
INSERT INTO `home_relate_type` VALUES ('176', '7', 'H,XB,D', '侄女', '[\'侄女\']');
INSERT INTO `home_relate_type` VALUES ('177', '7', 'H,XB,D,H', '侄女婿', '[\'侄女婿\']');
INSERT INTO `home_relate_type` VALUES ('178', '7', 'H,XB,D,S', '外侄孙', '[\'外侄孙\']');
INSERT INTO `home_relate_type` VALUES ('179', '7', 'H,XB,D,D', '外侄孙女', '[\'外侄孙女\']');
INSERT INTO `home_relate_type` VALUES ('180', '7', 'H,OS', '大姑子', '[\'大姑子\'\'大姑\'\'大娘姑\']');
INSERT INTO `home_relate_type` VALUES ('181', '7', 'H,OS,H', '大姑夫', '[\'大姑夫\'\'姊丈\']');
INSERT INTO `home_relate_type` VALUES ('182', '7', 'H,LS', '小姑子', '[\'小姑子\'\'小姑\']');
INSERT INTO `home_relate_type` VALUES ('183', '7', 'H,LS,H', '小姑夫', '[\'小姑夫\']');
INSERT INTO `home_relate_type` VALUES ('184', '7', 'H,XS,S', '姨甥', '[\'姨甥\']');
INSERT INTO `home_relate_type` VALUES ('185', '7', 'H,XS,D', '姨甥女', '[\'姨甥女\']');
INSERT INTO `home_relate_type` VALUES ('186', '7', 'H,M,XB', '舅公', '[\'舅公\']');
INSERT INTO `home_relate_type` VALUES ('187', '7', 'H,M,XB,W', '舅婆', '[\'舅婆\']');
INSERT INTO `home_relate_type` VALUES ('188', '7', 'H,M,XS', '姨婆', '[\'姨婆\']');
INSERT INTO `home_relate_type` VALUES ('189', '7', 'H,M,XS,H', '姨公', '[\'姨公\']');
INSERT INTO `home_relate_type` VALUES ('190', '8', 'XB', '兄弟', '[\'兄弟\']');
INSERT INTO `home_relate_type` VALUES ('191', '8', 'XS', '姐妹', '[\'姐妹\']');
INSERT INTO `home_relate_type` VALUES ('192', '8', 'OB', '哥哥', '[\'哥哥\'\'哥\'\'兄\'\'大哥\'\'大佬\']');
INSERT INTO `home_relate_type` VALUES ('193', '8', 'OB,W', '嫂子', '[\'嫂子\'\'嫂\']');
INSERT INTO `home_relate_type` VALUES ('194', '8', 'OB,W,F', '姻伯父', '[\'姻伯父\']');
INSERT INTO `home_relate_type` VALUES ('195', '8', 'OB,W,M', '姻伯母', '[\'姻伯母\']');
INSERT INTO `home_relate_type` VALUES ('196', '8', 'LB', '弟弟', '[\'弟弟\'\'弟\']');
INSERT INTO `home_relate_type` VALUES ('197', '8', 'LB,W', '弟妹', '[\'弟妹\'\'弟媳\'\'弟媳妇\']');
INSERT INTO `home_relate_type` VALUES ('198', '8', 'LB,W,F', '姻叔父', '[\'姻叔父\']');
INSERT INTO `home_relate_type` VALUES ('199', '8', 'LB,W,M', '姻叔母', '[\'姻叔母\']');
INSERT INTO `home_relate_type` VALUES ('200', '8', 'XB,S', '侄子', '[\'侄子\']');
INSERT INTO `home_relate_type` VALUES ('201', '8', 'XB,S,W', '侄媳', '[\'侄媳\'\'侄媳妇\']');
INSERT INTO `home_relate_type` VALUES ('202', '8', 'XB,S,S', '侄孙', '[\'侄孙\'\'侄孙子\']');
INSERT INTO `home_relate_type` VALUES ('203', '8', 'XB,S,S,W', '侄孙媳', '[\'侄孙媳\']');
INSERT INTO `home_relate_type` VALUES ('204', '8', 'XB,S,D', '侄孙女', '[\'侄孙女\']');
INSERT INTO `home_relate_type` VALUES ('205', '8', 'XB,D', '侄女', '[\'侄女\']');
INSERT INTO `home_relate_type` VALUES ('206', '8', 'XB,D,H', '侄女婿', '[\'侄女婿\']');
INSERT INTO `home_relate_type` VALUES ('207', '8', 'XB,D,S', '外侄孙子', '[\'外侄孙子\']');
INSERT INTO `home_relate_type` VALUES ('208', '8', 'XB,D,D', '外侄孙女', '[\'外侄孙女\']');
INSERT INTO `home_relate_type` VALUES ('209', '8', 'OS', '姐姐', '[\'姐姐\'\'姐\']');
INSERT INTO `home_relate_type` VALUES ('210', '8', 'OS,H', '姐夫', '[\'姐夫\'\'姊夫\'\'姊婿\']');
INSERT INTO `home_relate_type` VALUES ('211', '8', 'LS', '妹妹', '[\'妹妹\'\'妹\']');
INSERT INTO `home_relate_type` VALUES ('212', '8', 'LS,H', '妹夫', '[\'妹夫\'\'妹婿\']');
INSERT INTO `home_relate_type` VALUES ('213', '8', 'XS,H,F', '亲家爷', '[\'亲家爷\'\'亲爹\'\'亲伯\']');
INSERT INTO `home_relate_type` VALUES ('214', '8', 'XS,H,M', '亲家娘', '[\'亲家娘\'\'亲娘\']');
INSERT INTO `home_relate_type` VALUES ('215', '8', 'XS,S', '外甥', '[\'外甥\']');
INSERT INTO `home_relate_type` VALUES ('216', '8', 'XS,S,W', '外甥媳妇', '[\'外甥媳妇\']');
INSERT INTO `home_relate_type` VALUES ('217', '8', 'XS,S,S', '甥孙', '[\'甥孙\']');
INSERT INTO `home_relate_type` VALUES ('218', '8', 'XS,S,D', '甥孙女', '[\'甥孙女\']');
INSERT INTO `home_relate_type` VALUES ('219', '8', 'XS,D', '外甥女', '[\'外甥女\'\'甥女\']');
INSERT INTO `home_relate_type` VALUES ('220', '8', 'XS,D,H', '外甥女婿', '[\'外甥女婿\']');
INSERT INTO `home_relate_type` VALUES ('221', '8', 'XS,D,S', '甥外孙', '[\'甥外孙\']');
INSERT INTO `home_relate_type` VALUES ('222', '8', 'XS,D,S,W', '甥外孙媳妇', '[\'甥外孙媳妇\']');
INSERT INTO `home_relate_type` VALUES ('223', '8', 'XS,D,D', '甥外孙女', '[\'甥外孙女\']');
INSERT INTO `home_relate_type` VALUES ('224', '8', 'XS,D,D,H', '甥外孙女婿', '[\'甥外孙女婿\']');
INSERT INTO `home_relate_type` VALUES ('225', '9', 'F,F,F,F', '高祖父', '[\'高祖父\']');
INSERT INTO `home_relate_type` VALUES ('226', '9', 'F,F,F,M', '高祖母', '[\'高祖母\']');
INSERT INTO `home_relate_type` VALUES ('227', '9', 'F,F,F', '曾祖父', '[\'曾祖父\']');
INSERT INTO `home_relate_type` VALUES ('228', '9', 'F,F,M', '曾祖母', '[\'曾祖母\']');
INSERT INTO `home_relate_type` VALUES ('229', '9', 'F,M', '奶奶', '[\'奶奶\'\'祖母\']');
INSERT INTO `home_relate_type` VALUES ('230', '9', 'F,F', '爷爷', '[\'爷爷\'\'祖父\']');
INSERT INTO `home_relate_type` VALUES ('231', '9', 'F,F,F', '太爷爷', '[\'太爷爷\']');
INSERT INTO `home_relate_type` VALUES ('232', '9', 'F,F,M', '太奶奶', '[\'太奶奶\']');
INSERT INTO `home_relate_type` VALUES ('233', '9', 'F,F,XB', 'XX爷爷', '[\'XX爷爷\'\'二爷爷\'\'三爷爷\'\'堂祖父\']');
INSERT INTO `home_relate_type` VALUES ('234', '9', 'F,F,XB,W', 'XX奶奶', '[\'XX奶奶\'\'堂祖母\']');
INSERT INTO `home_relate_type` VALUES ('235', '9', 'F,F,OB', '大爷爷', '[\'大爷爷\'\'大爷\'\'堂祖父\']');
INSERT INTO `home_relate_type` VALUES ('236', '9', 'F,F,OB,W', '大奶奶', '[\'大奶奶\'\'堂祖母\']');
INSERT INTO `home_relate_type` VALUES ('237', '9', 'F,F,LB', '小爷爷', '[\'小爷爷\'\'堂祖父\']');
INSERT INTO `home_relate_type` VALUES ('238', '9', 'F,F,LB,W', '小奶奶', '[\'小奶奶\'\'堂祖母\']');
INSERT INTO `home_relate_type` VALUES ('239', '9', 'F,F,XS', '姑奶奶', '[\'姑奶奶\']');
INSERT INTO `home_relate_type` VALUES ('240', '9', 'F,F,XS,H', '姑爷爷', '[\'姑爷爷\']');
INSERT INTO `home_relate_type` VALUES ('241', '9', 'F,M,F', '太爷爷', '[\'太爷爷\']');
INSERT INTO `home_relate_type` VALUES ('242', '9', 'F,M,M', '太奶奶', '[\'太奶奶\']');
INSERT INTO `home_relate_type` VALUES ('243', '9', 'F,M,XS', '姨奶奶', '[\'姨奶奶\'\'姨婆\']');
INSERT INTO `home_relate_type` VALUES ('244', '9', 'F,M,XS,H', '姨爷爷', '[\'姨爷爷\'\'姨公\'\'姨爷\']');
INSERT INTO `home_relate_type` VALUES ('245', '9', 'F,M,XB', '舅爷爷', '[\'舅爷爷\'\'舅公\'\'舅爷\'\'舅祖\'\'舅奶爷\'\'舅祖父\']');
INSERT INTO `home_relate_type` VALUES ('246', '9', 'F,M,XB,W', '舅奶奶', '[\'舅奶奶\'\'舅婆\']');
INSERT INTO `home_relate_type` VALUES ('247', '10', 'M', '妈妈', '[\'妈妈\'\'母亲\'\'老妈\'\'老母\'\'娘\'\'娘亲\'\'妈咪\']');
INSERT INTO `home_relate_type` VALUES ('248', '10', 'F', '爸爸', '[\'爸爸\'\'父亲\'\'老爸\'\'老窦\'\'爹\'\'爹地\'\'老爷子\']');
INSERT INTO `home_relate_type` VALUES ('249', '10', 'W', '老婆', '[\'老婆\'\'妻子\'\'太太\'\'媳妇\'\'夫人\'\'女人\'\'婆娘\'\'妻\'\'内人\'\'娘子\'\'爱人\']');
INSERT INTO `home_relate_type` VALUES ('250', '10', 'H', '老公', '[\'老公\'\'丈夫\'\'先生\'\'官人\'\'男人\'\'汉子\'\'夫\'\'夫君\'\'爱人\']');
INSERT INTO `home_relate_type` VALUES ('251', '10', 'S', '儿子', '[\'儿子\']');
INSERT INTO `home_relate_type` VALUES ('252', '10', 'S,W', '儿媳妇', '[\'儿媳妇\'\'儿媳\']');
INSERT INTO `home_relate_type` VALUES ('253', '10', 'S,W,XB', '姻侄', '[\'姻侄\']');
INSERT INTO `home_relate_type` VALUES ('254', '10', 'S,W,XS', '姻侄女', '[\'姻侄女\']');
INSERT INTO `home_relate_type` VALUES ('255', '10', 'S,S', '孙子', '[\'孙子\']');
INSERT INTO `home_relate_type` VALUES ('256', '10', 'S,S,W', '孙媳妇', '[\'孙媳妇\'\'孙媳\']');
INSERT INTO `home_relate_type` VALUES ('257', '10', 'S,S,S', '曾孙', '[\'曾孙\']');
INSERT INTO `home_relate_type` VALUES ('258', '10', 'S,S,S,W', '曾孙媳妇', '[\'曾孙媳妇\']');
INSERT INTO `home_relate_type` VALUES ('259', '10', 'S,S,S,S', '玄孙', '[\'玄孙\']');
INSERT INTO `home_relate_type` VALUES ('260', '10', 'S,S,D', '曾孙女', '[\'曾孙女\']');
INSERT INTO `home_relate_type` VALUES ('261', '10', 'S,D', '孙女', '[\'孙女\']');
INSERT INTO `home_relate_type` VALUES ('262', '10', 'S,D,H', '孙女婿', '[\'孙女婿\']');
INSERT INTO `home_relate_type` VALUES ('263', '10', 'S,D,S', '曾外孙', '[\'曾外孙\']');
INSERT INTO `home_relate_type` VALUES ('264', '10', 'S,D,D', '曾外孙女', '[\'曾外孙女\']');
INSERT INTO `home_relate_type` VALUES ('265', '10', 'D', '女儿', '[\'女儿\'\'千金\']');
INSERT INTO `home_relate_type` VALUES ('266', '10', 'D,H', '女婿', '[\'女婿\']');
INSERT INTO `home_relate_type` VALUES ('267', '10', 'D,H,XB', '姻侄', '[\'姻侄\']');
INSERT INTO `home_relate_type` VALUES ('268', '10', 'D,H,XS', '姻侄女', '[\'姻侄女\']');
INSERT INTO `home_relate_type` VALUES ('269', '10', 'D,S', '外孙', '[\'外孙\']');
INSERT INTO `home_relate_type` VALUES ('270', '10', 'D,S,W', '外孙媳', '[\'外孙媳\']');
INSERT INTO `home_relate_type` VALUES ('271', '10', 'D,S,S', '外曾孙', '[\'外曾孙\'\'重外孙\']');
INSERT INTO `home_relate_type` VALUES ('272', '10', 'D,S,D', '外曾孙女', '[\'外曾孙女\'\'重外孙女\']');
INSERT INTO `home_relate_type` VALUES ('273', '10', 'D,D', '外孙女', '[\'外孙女\']');
INSERT INTO `home_relate_type` VALUES ('274', '10', 'D,D,H', '外孙女婿', '[\'外孙女婿\']');
INSERT INTO `home_relate_type` VALUES ('275', '10', 'D,D,S', '外曾外孙', '[\'外曾外孙\']');
INSERT INTO `home_relate_type` VALUES ('276', '10', 'D,D,D', '外曾外孙女', '[\'外曾外孙女\']');
INSERT INTO `home_relate_type` VALUES ('277', '11', 'S,W,M', '亲家母', '[\'亲家母\']');
INSERT INTO `home_relate_type` VALUES ('278', '11', 'S,W,F', '亲家公', '[\'亲家公\'\'亲家翁\']');
INSERT INTO `home_relate_type` VALUES ('279', '11', 'S,W,F,OB', '姻兄', '[\'姻兄\']');
INSERT INTO `home_relate_type` VALUES ('280', '11', 'S,W,F,LB', '姻弟', '[\'姻弟\']');
INSERT INTO `home_relate_type` VALUES ('281', '11', 'S,W,F,F', '太姻翁', '[\'太姻翁\']');
INSERT INTO `home_relate_type` VALUES ('282', '11', 'S,W,F,F', '太姻姆', '[\'太姻姆\']');
INSERT INTO `home_relate_type` VALUES ('283', '11', 'D,H,M', '亲家母', '[\'亲家母\']');
INSERT INTO `home_relate_type` VALUES ('284', '11', 'D,H,F', '亲家公', '[\'亲家公\'\'亲家翁\']');
INSERT INTO `home_relate_type` VALUES ('285', '11', 'D,H,F,OB', '姻兄', '[\'姻兄\']');
INSERT INTO `home_relate_type` VALUES ('286', '11', 'D,H,F,LB', '姻弟', '[\'姻弟\']');
INSERT INTO `home_relate_type` VALUES ('287', '11', 'D,H,F,F', '太姻翁', '[\'太姻翁\']');
INSERT INTO `home_relate_type` VALUES ('288', '11', 'D,H,F,F', '太姻姆', '[\'太姻姆\']');

-- ----------------------------
-- Table structure for home_relate_type_20160728
-- ----------------------------
DROP TABLE IF EXISTS `home_relate_type_20160728`;
CREATE TABLE `home_relate_type_20160728` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TYPE` int(2) DEFAULT NULL COMMENT '关系类型',
  `NAME` varchar(50) NOT NULL COMMENT '默认的名称',
  `LEVEL` int(2) DEFAULT NULL COMMENT '层级 1 长辈 2 长辈的长辈 0 平辈 -1 晚辈',
  `CHAIN` varchar(255) DEFAULT NULL COMMENT '关系组成链',
  `REMARKS` varchar(50) NOT NULL COMMENT '描述',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=219 DEFAULT CHARSET=utf8 COMMENT='前台亲属称呼';

-- ----------------------------
-- Records of home_relate_type_20160728
-- ----------------------------
INSERT INTO `home_relate_type_20160728` VALUES ('0', '11', '自己', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('1', '1', '外婆', '2', 'M,M', '');
INSERT INTO `home_relate_type_20160728` VALUES ('2', '1', '外公', '2', 'M,F', '母亲的父亲');
INSERT INTO `home_relate_type_20160728` VALUES ('3', '1', '太姥姥', '3', 'M,M,M', '母亲的奶奶');
INSERT INTO `home_relate_type_20160728` VALUES ('4', '1', '太姥爷', '3', 'M,M,F', '');
INSERT INTO `home_relate_type_20160728` VALUES ('5', '1', '姨姥姥', '2', 'M,M,XS', '妈妈的姨妈');
INSERT INTO `home_relate_type_20160728` VALUES ('6', '1', '姨姥爷', '2', 'M,M,XS,W', '妈妈的姨爸');
INSERT INTO `home_relate_type_20160728` VALUES ('7', '1', '舅姥爷', '2', 'M,M,XB', '妈妈的舅爸');
INSERT INTO `home_relate_type_20160728` VALUES ('8', '1', '舅姥姥', '2', 'M,M,XB,W', '姥姥的兄弟的妻子');
INSERT INTO `home_relate_type_20160728` VALUES ('9', '1', '姑姥姥', '2', 'M,F,XS', '母亲的姑母');
INSERT INTO `home_relate_type_20160728` VALUES ('10', '1', '姑姥爷', '2', 'M,F,XS,H', '妈妈的姑爸');
INSERT INTO `home_relate_type_20160728` VALUES ('11', '1', '大姥爷', '2', 'M,F,OB', '');
INSERT INTO `home_relate_type_20160728` VALUES ('12', '1', '大姥姥', '2', 'M,F,OB,W', '');
INSERT INTO `home_relate_type_20160728` VALUES ('13', '1', '小姥爷', '2', 'M,F,LB', '');
INSERT INTO `home_relate_type_20160728` VALUES ('14', '1', '小姥姥', '2', 'M,F,LB,W', '');
INSERT INTO `home_relate_type_20160728` VALUES ('15', '1', 'xx姥爷', '2', 'M,F,XB', '');
INSERT INTO `home_relate_type_20160728` VALUES ('16', '2', '表舅', '1', 'M,F,XS,S', '');
INSERT INTO `home_relate_type_20160728` VALUES ('17', '2', '表舅妈', '1', 'M,F,XS,S,W', '');
INSERT INTO `home_relate_type_20160728` VALUES ('18', '2', '堂舅', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('19', '2', '堂舅妈', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('20', '2', '表姨', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('21', '2', '表姨丈', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('22', '2', '堂姨', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('23', '2', '堂姨丈', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('24', '2', '舅舅', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('25', '2', '舅妈', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('26', '2', '表哥', '0', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('27', '2', '表嫂', '0', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('28', '2', '表弟', '0', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('29', '2', '表弟媳', '0', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('30', '2', '表侄子', '-1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('31', '2', '表侄女', '-1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('32', '2', '表姐', '0', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('33', '2', '表姐夫', '0', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('34', '2', '表妹', '0', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('35', '2', '表妹夫', '0', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('36', '2', '表外甥', '-1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('37', '2', '表外甥女', '-1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('38', '2', '大舅', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('39', '2', '大舅妈', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('40', '2', '小舅', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('41', '2', '小舅妈', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('42', '3', '姨妈', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('43', '3', '姨丈', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('44', '3', '大姨', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('45', '3', '大姨父', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('46', '3', '小姨', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('47', '3', '小姨父', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('48', '4', '姑妈', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('49', '4', '姑丈', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('50', '4', '姑母', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('51', '4', '姑姐', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('52', null, '堂哥', '0', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('53', '5', '堂嫂', '0', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('54', '5', '堂弟', '0', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('55', '5', '堂弟媳', '0', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('56', '5', '堂侄子', '-1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('57', '5', '堂侄女', '-1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('58', '5', '堂姐', '0', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('59', '5', '堂姐夫', '0', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('60', '5', '堂妹', '0', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('61', '5', '堂妹夫', '0', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('62', '5', '堂外甥', '-1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('63', '5', '堂外甥女', '-1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('64', '5', '伯父', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('65', '5', '伯母', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('66', '5', '叔叔', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('67', '5', '婶婶', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('68', '5', '表伯', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('69', '5', '表叔', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('70', '5', '表婶', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('71', '5', '堂伯', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('72', '5', '堂叔', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('73', '5', '堂婶', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('74', '5', '表姑', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('75', '5', '表姑丈', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('76', '5', '堂姑', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('77', '5', '堂姑丈', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('78', '6', '岳母', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('79', '6', '岳父', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('80', '6', '伯岳', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('81', '6', '伯岳母', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('82', '6', '叔岳', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('83', '6', '叔岳母', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('84', '6', '太岳父', '2', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('85', '6', '太岳母', '2', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('86', '6', '太伯岳', '2', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('87', '6', '太伯岳母', '2', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('88', '6', '太叔岳', '2', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('89', '6', '太叔岳母', '2', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('90', '6', '姻伯', '2', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('91', '6', '姻姆', '2', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('92', '6', '姻叔', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('93', '6', '姻婶', '1', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('94', '6', '大舅哥', '0', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('95', '6', '嫂子', '0', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('96', '6', '小舅子', '0', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('97', '6', '弟媳妇', '0', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('98', '6', '姻家兄', '0', null, '姻亲中同辈兄弟的互称');
INSERT INTO `home_relate_type_20160728` VALUES ('99', '6', '姻家弟', '0', null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('100', '6', '内侄', '0', null, '指妻子的弟兄的儿子');
INSERT INTO `home_relate_type_20160728` VALUES ('101', '6', '内侄女', '0', null, '指妻子的弟兄的女儿');
INSERT INTO `home_relate_type_20160728` VALUES ('102', '6', '大姨子', '0', null, '妻子的姐姐');
INSERT INTO `home_relate_type_20160728` VALUES ('103', '6', '大姨夫', '1', null, '母亲姐姐或妹妹的丈夫');
INSERT INTO `home_relate_type_20160728` VALUES ('104', '6', '小姨子', null, null, '妻子的姐姐或者妹妹');
INSERT INTO `home_relate_type_20160728` VALUES ('105', '6', '小姨夫', null, null, '妻子的妹弟');
INSERT INTO `home_relate_type_20160728` VALUES ('106', '6', '妻外甥', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('107', '6', '妻外甥女', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('108', '7', '婆婆', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('109', '7', '公公', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('110', '7', '伯翁', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('111', '7', '伯婆', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('112', '7', '叔翁', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('113', '7', '叔婆', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('114', '7', '祖翁', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('115', '7', '祖婆', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('116', '7', '太公翁', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('117', '7', '太奶亲', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('118', '7', '堂小弟', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('119', '7', '大伯子', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('120', '7', '大婶子', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('121', '7', '小叔子', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('122', '7', '小婶子', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('123', '7', '侄子', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('124', '7', '侄媳', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('125', '7', '侄孙', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('126', '7', '侄孙媳', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('127', '7', '侄孙女', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('128', '7', '侄女', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('129', '7', '侄女婿', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('130', '7', '外侄孙', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('131', '7', '外侄孙女', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('132', '7', '大姑子', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('133', '7', '大姑夫', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('134', '7', '小姑子', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('135', '7', '小姑夫', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('136', '7', '姨甥', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('137', '7', '姨甥女', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('138', '7', '舅公', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('139', '7', '舅婆', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('140', '7', '姨婆', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('141', '7', '姨公', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('142', '8', '兄弟', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('143', '8', '姐妹', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('144', '8', '哥哥', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('145', '8', '姻伯父', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('146', '8', '姻伯母', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('147', '8', '弟弟', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('148', '8', '弟妹', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('149', '8', '姻叔父', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('150', '8', '姻叔母', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('151', '8', '外侄孙子', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('152', '8', '姐姐', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('153', '8', '姐夫', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('154', '8', '妹妹', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('155', '8', '妹夫', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('156', '8', '亲家爷', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('157', '8', '亲家娘', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('158', '8', '外甥', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('159', '8', '外甥媳妇', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('160', '8', '甥孙', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('161', '8', '甥孙女', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('162', '8', '外甥女', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('163', '8', '外甥女婿', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('164', '8', '甥外孙', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('165', '8', '甥外孙媳妇', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('166', '8', '甥外孙女', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('167', '8', '甥外孙女婿', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('168', '9', '高祖父', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('169', '9', '高祖母', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('170', '9', '太爷爷', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('171', '9', '太奶奶', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('172', '9', '奶奶', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('173', '9', '爷爷', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('174', '9', 'xx爷爷', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('175', '9', 'xx奶奶', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('176', '9', '大爷爷', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('177', '9', '大奶奶', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('178', '9', '小爷爷', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('179', '9', '小奶奶', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('180', '9', '姑奶奶', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('181', '9', '姑爷爷', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('182', '9', '姨奶奶', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('183', '9', '姨爷爷', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('184', '9', '舅爷爷', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('185', '9', '舅奶奶', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('186', '10', '妈妈', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('187', '10', '爸爸', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('188', '10', '老婆', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('189', '10', '老公', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('190', '10', '儿子', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('191', '10', '儿媳妇', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('192', '10', '姻侄', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('193', '10', '姻侄女', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('194', '10', '孙子', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('195', '10', '孙媳妇', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('196', '10', '曾孙', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('197', '10', '曾孙媳妇', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('198', '10', '玄孙', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('199', '10', '曾孙女', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('200', '10', '孙女', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('201', '10', '孙女婿', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('202', '10', '曾外孙', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('203', '10', '曾外孙女', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('204', '10', '女儿', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('205', '10', '女婿', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('206', '10', '外孙', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('207', '10', '外孙媳', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('208', '10', '外曾孙', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('209', '10', '外曾孙女', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('210', '10', '外孙女', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('211', '10', '外孙女婿', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('212', '10', '外曾外孙', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('213', '10', '外曾外孙女', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('214', '11', '亲家母', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('215', '11', '亲家公', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('216', '11', '姻兄', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('217', '11', '姻弟', null, null, '');
INSERT INTO `home_relate_type_20160728` VALUES ('218', '11', '太姻姆', null, null, '');

-- ----------------------------
-- Table structure for home_relate_type_ref
-- ----------------------------
DROP TABLE IF EXISTS `home_relate_type_ref`;
CREATE TABLE `home_relate_type_ref` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `EXPRESSION` varchar(50) NOT NULL COMMENT '计算公式',
  `RELATE_TYPE_ID` int(10) unsigned NOT NULL COMMENT '称呼的ID',
  `REMARKS` varchar(50) NOT NULL COMMENT '描述',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='前台亲属称呼';

-- ----------------------------
-- Records of home_relate_type_ref
-- ----------------------------
INSERT INTO `home_relate_type_ref` VALUES ('1', ',144,187,', '187', '哥哥的父亲：父亲');

-- ----------------------------
-- Table structure for home_relate_type_template
-- ----------------------------
DROP TABLE IF EXISTS `home_relate_type_template`;
CREATE TABLE `home_relate_type_template` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `RELATE_ID` int(10) unsigned DEFAULT NULL COMMENT '关系ID 0 表示自己',
  `PARENT_ID` int(10) unsigned DEFAULT NULL COMMENT '父节点ID',
  `GENDER` int(1) DEFAULT NULL COMMENT '关系性别 1 男方 2 女方',
  `REMARK` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of home_relate_type_template
-- ----------------------------
INSERT INTO `home_relate_type_template` VALUES ('1', '1', '248', '1', '自己的爸爸');
INSERT INTO `home_relate_type_template` VALUES ('2', '1', '247', '2', '自己的妈妈');
INSERT INTO `home_relate_type_template` VALUES ('3', '248', '230', '1', '爸爸的爸爸');
INSERT INTO `home_relate_type_template` VALUES ('4', '248', '229', '2', '爸爸的妈妈');
INSERT INTO `home_relate_type_template` VALUES ('5', '247', '3', '1', '妈妈的爸爸');
INSERT INTO `home_relate_type_template` VALUES ('6', '247', '2', '2', '妈妈的妈妈');

-- ----------------------------
-- Table structure for home_user
-- ----------------------------
DROP TABLE IF EXISTS `home_user`;
CREATE TABLE `home_user` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `NICKNAME` varchar(50) NOT NULL COMMENT '昵称',
  `PASSWORD` varchar(50) DEFAULT '' COMMENT '密码',
  `MOBILE` varchar(50) NOT NULL COMMENT '手机号',
  `EMAIL` varchar(50) DEFAULT NULL COMMENT '电子邮箱',
  `REAL_NAME` varchar(50) NOT NULL COMMENT '真实姓名',
  `ID_CARD` varchar(50) NOT NULL COMMENT '身份证',
  `GENDER` tinyint(3) unsigned DEFAULT NULL COMMENT '性别:0保密，1男，2女',
  `WECHAT_ID` int(11) DEFAULT NULL COMMENT '外键',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='前台用户';

-- ----------------------------
-- Records of home_user
-- ----------------------------
INSERT INTO `home_user` VALUES ('1', '姜俊国', '', '13927441616', null, '姜俊国', '220104198104174419', null, '1');
INSERT INTO `home_user` VALUES ('2', '姜爸爸', '', '13927441616', null, '姜爸爸', '220104198104174418', null, null);
INSERT INTO `home_user` VALUES ('3', '姜妈妈', '', '13927441616', null, '姜妈妈', '220104198104174418', null, null);
INSERT INTO `home_user` VALUES ('4', '姜爷爷', '', '13927441616', null, '姜爷爷', '220104198104174418', null, null);
INSERT INTO `home_user` VALUES ('5', '姜奶奶', '', '', null, '姜奶奶', '', null, null);

-- ----------------------------
-- Table structure for home_user2
-- ----------------------------
DROP TABLE IF EXISTS `home_user2`;
CREATE TABLE `home_user2` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `NICKNAME` varchar(50) NOT NULL COMMENT '昵称',
  `PASSWORD` varchar(50) DEFAULT '' COMMENT '密码',
  `MOBILE` varchar(50) NOT NULL COMMENT '手机号',
  `EMAIL` varchar(50) DEFAULT NULL COMMENT '电子邮箱',
  `REAL_NAME` varchar(50) NOT NULL COMMENT '真实姓名',
  `ID_CARD` varchar(50) NOT NULL COMMENT '身份证',
  `GENDER` tinyint(3) unsigned DEFAULT NULL COMMENT '性别:0保密，1男，2女',
  `WECHAT_ID` int(11) DEFAULT NULL COMMENT '外键',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COMMENT='前台用户';

-- ----------------------------
-- Records of home_user2
-- ----------------------------
INSERT INTO `home_user2` VALUES ('1', 'coderjiang', '123456', '13927441616', 'coderjiang@sina.com', 'jiangjunguo', '2201041918102', '1', '1');
INSERT INTO `home_user2` VALUES ('2', 'Coder Jiang', '', '13927441616', null, '姜', '123456', null, '1');
INSERT INTO `home_user2` VALUES ('4', 'coderjiang', '', '13927441616', null, '姜', '220', null, null);
INSERT INTO `home_user2` VALUES ('9', 'coderjiang', '', '13927441616', null, '姜', '220', null, null);
INSERT INTO `home_user2` VALUES ('10', 'coderjiang', '', '13927441616', null, '姜', '220', null, null);
INSERT INTO `home_user2` VALUES ('11', 'Coder Jiang', '', '13927441616', null, '姜', '123456', null, '1');
INSERT INTO `home_user2` VALUES ('12', 'coderjiang', '', '13927441616', null, '姜', '220', null, null);
INSERT INTO `home_user2` VALUES ('13', 'coderjiang', '', '13927441616', null, '姜', '220', null, null);
INSERT INTO `home_user2` VALUES ('14', 'coderjiang', '', '13927441616', null, '姜', '220', null, null);
INSERT INTO `home_user2` VALUES ('15', 'coderjiang', '', '13927441616', null, '姜', '220', null, null);
INSERT INTO `home_user2` VALUES ('20', 'coderjiang', '', '13927441616', null, '姜', '220', null, null);
INSERT INTO `home_user2` VALUES ('24', 'coderjiang', '', '13927441616', null, '姜', '220', null, null);
INSERT INTO `home_user2` VALUES ('30', 'coderjiang', '', '13927441616', null, '姜', '220', null, null);

-- ----------------------------
-- Table structure for home_user_wechat
-- ----------------------------
DROP TABLE IF EXISTS `home_user_wechat`;
CREATE TABLE `home_user_wechat` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `openid` char(28) NOT NULL COMMENT '用户唯一标识，请注意，在未关注公众号时，用户访问公众号的网页，也会产生一个用户和公众号唯一的OpenID',
  `access_token` varchar(255) DEFAULT NULL COMMENT '网页授权接口调用凭证,注意：此access_token与基础支持的access_token不同',
  `expires_in` int(9) DEFAULT NULL COMMENT 'access_token接口调用凭证超时时间，单位（秒）',
  `scope` varchar(50) DEFAULT NULL COMMENT '用户授权的作用域，使用逗号（,）分隔',
  `refresh_token` varchar(255) DEFAULT NULL COMMENT '用户刷新access_token',
  `unionid` varchar(255) DEFAULT NULL COMMENT '只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段',
  `nickname` varchar(255) DEFAULT NULL COMMENT '用户昵称',
  `sex` int(1) DEFAULT NULL COMMENT '用户的性别，值为1时是男性，值为2时是女性，值为0时是未知',
  `province` varchar(255) DEFAULT NULL COMMENT '用户个人资料填写的省份',
  `city` varchar(255) DEFAULT NULL COMMENT '普通用户个人资料填写的城市',
  `country` varchar(255) DEFAULT NULL COMMENT '国家，如中国为CN',
  `headimgurl` varchar(255) DEFAULT NULL COMMENT '用户头像，最后一个数值代表正方形头像大小（有0、46、64、96、132数值可选，0代表640*640正方形头像），用户没有头像时该项为空。若用户更换头像，原有头像URL将失效',
  `privilege` varchar(255) DEFAULT NULL COMMENT '用户特权信息，json 数组，如微信沃卡用户为（chinaunicom）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of home_user_wechat
-- ----------------------------
INSERT INTO `home_user_wechat` VALUES ('1', 'o1Pqbsz1N1KzEtkik1t0SdI7W3jE', 'NULL', '7200', 'snsapi_userinfo', null, null, 'Coder Jiang', '1', '广东', '深圳', '中国', 'http://wx.qlogo.cn/mmopen/mK3uFdyP43qwn9qpntF2LjjNBz4jtULz96SxqIVBEb9FSibk69r6tLpTzrsH3tMhV8taneQKkTJnibf7icJvcqsHvWbSYiamjoKe/0', '广东');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `PARENT_ID` int(10) unsigned NOT NULL COMMENT '父级编号',
  `PARENT_IDS` varchar(2000) NOT NULL COMMENT '所有父级编号',
  `NAME` varchar(100) NOT NULL COMMENT '名称',
  `LEVEL` int(10) unsigned NOT NULL COMMENT '层级',
  `SORT` int(11) NOT NULL COMMENT '排序',
  `HREF` varchar(2000) DEFAULT NULL COMMENT '链接',
  `ICON` varchar(100) DEFAULT NULL COMMENT '图标',
  `IS_SHOW` tinyint(1) NOT NULL COMMENT '是否在菜单中显示',
  `PERMISSION` varchar(200) DEFAULT NULL COMMENT '权限标识',
  `IS_DELETED` tinyint(1) NOT NULL COMMENT '删除标记',
  `REMARKS` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `CREATE_BY` int(10) unsigned DEFAULT NULL COMMENT '创建者',
  `CREATE_DATE` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_BY` int(10) unsigned DEFAULT NULL COMMENT '更新者',
  `UPDATE_DATE` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COMMENT='菜单表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '0', ',0,', '系统管理', '1', '30', '', '', '1', '', '0', null, null, null, null, null);
INSERT INTO `sys_menu` VALUES ('2', '1', ',0,1,', '管理员管理', '2', '1', '/admin/manager/list', '', '1', '', '0', null, null, null, null, null);
INSERT INTO `sys_menu` VALUES ('3', '2', ',0,1,2,', '查看', '3', '1', '', '', '0', 'sys:user:view', '0', null, null, null, null, null);
INSERT INTO `sys_menu` VALUES ('4', '2', ',0,1,2,', '新增', '3', '2', '', '', '0', 'sys:user:create', '0', null, null, null, null, null);
INSERT INTO `sys_menu` VALUES ('5', '2', ',0,1,2,', '修改', '3', '3', '', '', '0', 'sys:user:edit', '0', null, null, null, null, null);
INSERT INTO `sys_menu` VALUES ('6', '2', ',0,1,2,', '删除', '3', '4', '', '', '0', 'sys:user:delete', '0', null, null, null, null, null);
INSERT INTO `sys_menu` VALUES ('7', '2', ',0,1,2,', '导出', '3', '5', '', '', '0', 'sys:user:export', '0', null, null, null, null, null);
INSERT INTO `sys_menu` VALUES ('8', '1', ',0,1,', '角色管理', '2', '2', '/admin/role/list', '', '1', '', '0', null, null, null, null, null);
INSERT INTO `sys_menu` VALUES ('9', '8', ',0,1,8,', '查看', '3', '1', '', '', '0', 'sys:role:view', '0', null, null, null, null, null);
INSERT INTO `sys_menu` VALUES ('10', '8', ',0,1,8,', '新增', '3', '2', '', '', '0', 'sys:role:create', '0', null, null, null, null, null);
INSERT INTO `sys_menu` VALUES ('11', '8', ',0,1,8,', '修改', '3', '3', '', '', '0', 'sys:role:edit', '0', null, null, null, null, null);
INSERT INTO `sys_menu` VALUES ('12', '8', ',0,1,8,', '删除', '3', '4', '', '', '0', 'sys:role:delete', '0', null, null, null, null, null);
INSERT INTO `sys_menu` VALUES ('13', '1', ',0,1,', '菜单管理', '2', '3', '/admin/menu/list', '', '1', '', '0', null, null, null, null, null);
INSERT INTO `sys_menu` VALUES ('14', '13', ',0,1,13,', '查看', '3', '1', '', '', '0', 'sys:menu:view', '0', null, null, null, null, null);
INSERT INTO `sys_menu` VALUES ('15', '13', ',0,1,13,', '新增', '3', '2', '', '', '0', 'sys:menu:create', '0', null, null, null, null, null);
INSERT INTO `sys_menu` VALUES ('16', '13', ',0,1,13,', '修改', '3', '3', '', '', '0', 'sys:menu:edit', '0', null, null, null, null, null);
INSERT INTO `sys_menu` VALUES ('17', '13', ',0,1,13,', '删除', '3', '4', '', '', '0', 'sys:menu:delete', '0', null, null, null, null, null);
INSERT INTO `sys_menu` VALUES ('18', '1', ',0,1,', '参数管理', '2', '3', '/admin/param/list', '', '1', '', '0', null, null, null, null, null);
INSERT INTO `sys_menu` VALUES ('19', '18', ',0,1,18,', '查看', '3', '1', '', '', '0', 'sys:param:view', '0', null, null, null, null, null);
INSERT INTO `sys_menu` VALUES ('20', '18', ',0,1,18,', '新增', '3', '2', '', '', '0', 'sys:param:create', '0', null, null, null, null, null);
INSERT INTO `sys_menu` VALUES ('21', '18', ',0,1,18,', '修改', '3', '3', '', '', '0', 'sys:param:edit', '0', null, null, null, null, null);
INSERT INTO `sys_menu` VALUES ('22', '18', ',0,1,18,', '删除', '3', '4', '', '', '0', 'sys:param:delete', '0', null, null, null, null, null);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `CODE` varchar(255) DEFAULT NULL COMMENT '角色编码',
  `NAME` varchar(100) NOT NULL COMMENT '角色名称',
  `IS_SYSTEM` tinyint(1) NOT NULL COMMENT '是否系统',
  `IS_DELETED` tinyint(1) NOT NULL COMMENT '删除标记',
  `REMARKS` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `CREATE_BY` int(10) unsigned DEFAULT NULL COMMENT '创建者',
  `CREATE_DATE` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_BY` int(10) unsigned DEFAULT NULL COMMENT '更新者',
  `UPDATE_DATE` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='角色表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', 'admin', '超级管理员', '0', '0', null, null, null, null, null);
INSERT INTO `sys_role` VALUES ('2', 'test', '测试管理员', '0', '0', null, null, null, null, null);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `ROLE_ID` int(10) unsigned NOT NULL COMMENT '角色编号',
  `MENU_ID` int(10) unsigned NOT NULL COMMENT '菜单编号',
  PRIMARY KEY (`ROLE_ID`,`MENU_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色-菜单';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('1', '1');
INSERT INTO `sys_role_menu` VALUES ('1', '2');
INSERT INTO `sys_role_menu` VALUES ('1', '3');
INSERT INTO `sys_role_menu` VALUES ('1', '4');
INSERT INTO `sys_role_menu` VALUES ('1', '5');
INSERT INTO `sys_role_menu` VALUES ('1', '6');
INSERT INTO `sys_role_menu` VALUES ('1', '7');
INSERT INTO `sys_role_menu` VALUES ('1', '8');
INSERT INTO `sys_role_menu` VALUES ('1', '9');
INSERT INTO `sys_role_menu` VALUES ('1', '10');
INSERT INTO `sys_role_menu` VALUES ('1', '11');
INSERT INTO `sys_role_menu` VALUES ('1', '12');
INSERT INTO `sys_role_menu` VALUES ('1', '13');
INSERT INTO `sys_role_menu` VALUES ('1', '14');
INSERT INTO `sys_role_menu` VALUES ('1', '15');
INSERT INTO `sys_role_menu` VALUES ('1', '16');
INSERT INTO `sys_role_menu` VALUES ('1', '17');
INSERT INTO `sys_role_menu` VALUES ('1', '18');
INSERT INTO `sys_role_menu` VALUES ('1', '19');
INSERT INTO `sys_role_menu` VALUES ('1', '20');
INSERT INTO `sys_role_menu` VALUES ('1', '21');
INSERT INTO `sys_role_menu` VALUES ('1', '22');
INSERT INTO `sys_role_menu` VALUES ('2', '1');
INSERT INTO `sys_role_menu` VALUES ('2', '8');
INSERT INTO `sys_role_menu` VALUES ('2', '9');
INSERT INTO `sys_role_menu` VALUES ('2', '10');
INSERT INTO `sys_role_menu` VALUES ('2', '11');
INSERT INTO `sys_role_menu` VALUES ('2', '12');
INSERT INTO `sys_role_menu` VALUES ('2', '13');
INSERT INTO `sys_role_menu` VALUES ('2', '14');
INSERT INTO `sys_role_menu` VALUES ('2', '15');
INSERT INTO `sys_role_menu` VALUES ('2', '16');
INSERT INTO `sys_role_menu` VALUES ('2', '17');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `USERNAME` varchar(50) NOT NULL COMMENT '用户名',
  `PASSWORD` varchar(200) NOT NULL COMMENT '密码',
  `NICKNAME` varchar(50) NOT NULL COMMENT '昵称',
  `IS_SYSTEM` tinyint(1) NOT NULL COMMENT '是否系统账号',
  `IS_DELETED` tinyint(1) NOT NULL COMMENT '是否删除',
  `REMARKS` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `CREATE_BY` int(10) unsigned DEFAULT NULL COMMENT '创建者',
  `CREATE_DATE` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_BY` int(10) unsigned DEFAULT NULL COMMENT '更新者',
  `UPDATE_DATE` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='后台用户';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', '9cbf1b0c4fdb935ecd47f32429f8c45e5cc20264053237b3e8b08a71', '超级管理员', '1', '0', null, null, null, null, null);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `USER_ID` int(10) unsigned NOT NULL COMMENT '用户编号',
  `ROLE_ID` int(10) unsigned NOT NULL COMMENT '角色编号',
  PRIMARY KEY (`USER_ID`,`ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户-角色';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '1');
