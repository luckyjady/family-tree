<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp" %>
<%--<%@ taglib prefix="sitemesh" uri="http://www.opensymphony.com/sitemesh/decorator" %>--%>
<!DOCTYPE html>
<html style="overflow-x:auto;overflow-y:auto;">
<head>
    <title><sitemesh:write property="title"/> - 后台管理 - </title>
    <%@include file="/WEB-INF/views/include/head.jsp" %>

    <script src="${ctxStatic}/jquery/jquery-1.12.3.min.js" type="text/javascript"></script>
    <script src="${ctxStatic}/jquery/jquery.ba-resize.min.js" type="text/javascript"></script>

    <%--<link rel="stylesheet" href="${ctxStatic}/bootstrap/3.3.6/css/bootstrap.min.css">--%>
    <%--<link rel="stylesheet" href="${ctxStatic}/bootstrap/3.3.6/css/bootstrap-theme.min.css">--%>
    <%--<script src="${ctxStatic}/bootstrap/3.3.6/js/bootstrap.min.js"></script>--%>
    <%--<script src="${ctxStatic}/bootstrap/3.3.6/js/bootstrap-hover-dropdown.min.js"></script>--%>
    <script src="${ctxStatic}/js/admin.js"></script>
    <script src="${ctxStatic}/js/genesis.js"></script>
    <link rel="stylesheet" href="${ctxStatic}/css/genesis.css">

    <%--<link rel="stylesheet" href="${ctxStatic}/zTree/3.5.23/css/zTreeStyle/zTreeStyle.css">--%>
    <%--<link rel="stylesheet" href="${ctxStatic}/zTree/3.5.23/css/metroStyle/metroStyle.css">--%>
    <%--<script src="${ctxStatic}/zTree/3.5.23/js/jquery.ztree.all.min.js"></script>--%>

    <link rel="stylesheet" type="text/css" href="${ctxStatic}/easyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="${ctxStatic}/easyui/themes/icon.css">
    <script type="text/javascript" src="${ctxStatic}/easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="${ctxStatic}/easyui/locale/easyui-lang-zh_CN.js"></script>

    <sitemesh:write property='head'/>
</head>
<body class="easyui-layout">
<sitemesh:write property='body'/>

</body>
</html>