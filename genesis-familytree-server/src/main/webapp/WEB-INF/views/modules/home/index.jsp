<%@ page import="genesis.familytree.server.common.security.SystemAuthorizingRealm" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head lang="zh-cn">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Amaze UI Touch</title>
    <meta name="renderer" content="webkit">
    <!-- No Baidu Siteapp-->
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <link rel="alternate icon" type="image/png" href="i/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="i/app-icon72x72@2x.png">
    <meta name="apple-mobile-web-app-title" content="AMUI React"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" href="${ctxWebStatic}/css/amazeui.touch.css"/>
    <link rel="stylesheet" href="${ctxWebStatic}/css/common.css"/>
</head>
<body>
<div id="root" style="height: 100%; width: 100%; overflow: hidden;">
</div>
<script src="//cdn.bootcss.com/react/15.1.0/react-with-addons.js"></script>
<script src="//cdn.bootcss.com/react/15.1.0/react-dom.js"></script>
<script src="${ctxWebStatic}/cdn/amazeui.touch.js"></script>
<script src="//libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
<script src="${ctxWebStatic}/amazeui/js/index.bundle.js"></script>
</body>
</html>