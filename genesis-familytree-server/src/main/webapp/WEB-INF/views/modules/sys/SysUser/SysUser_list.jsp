<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
    <title>主页</title>
</head>
<body>
<table id="dataList"></table>

<script>
    $(function () {
        var $dataList = $("#dataList").datagrid({
            url:'${ctx}/manager/list',
            method: "post",
            columns:[[
                {field:'id',title:'ID',width:60},
                {field:'username',title:'登录名',width:200},
                {field:'nickname',title:'昵称',width:250}
            ]],
            pagination: true,
            pageNumber: 1,
            pageSize: 15,
            pageList: [1, 15, 20, 25, 50],
            toolbar: [{
                text:'新增',
                iconCls:'icon-add',
                handler:function(){
                    var node = $("#dataList").datagrid("getSelected");

                    $("#editPanel").editPanel({
                        "title": "",
                        "url": "${ctx}/menu/update?id=" + id
                    });
                }
            },{
                text:'修改',
                iconCls:'icon-edit',
                handler:function(){
                    var node = $("#dataList").datagrid("getSelected");

                    $("#editPanel").editPanel({
                        "title": "",
                        "url": "${ctx}/menu/update?id=" + id
                    });
                }
            },{
                text:'删除',
                iconCls:'icon-cancel',
                handler:function(){
                    var node = $("#dataList").datagrid("getSelected");
                    var url = "${ctx}/menu/delete?id=" + node.id
                }
            },'-',{
                text:'刷新',
                iconCls:'icon-reload',
                handler:function(){
                    $("#dataList").datagrid("reload");
                }
            }],
            loadFilter: function(response) {
                return {
                    total: response["data"]["count"],
                    rows: response["data"]["list"]
                };
            }
        });

       /* var buildRow = function(data) {
            var row = '<tr data-id="'+ data["id"] +'">';
            row += '<td>'+ data["id"] +'</td>';
            row += '<td>'+ data["username"] +'</td>';
            row += '<td>'+ data["nickname"] +'</td>';
            row += '<td>';
            <shiro:hasPermission name="sys:user:edit">
            row += '<a class="access">权限</a>';
            </shiro:hasPermission>
            row += '</td>';
            row += '<td>';
            <shiro:hasPermission name="sys:user:edit">
            row += '<a class="edit">修改</a>';
            </shiro:hasPermission>
            <shiro:hasPermission name="sys:user:delete">
            if(data["isSystem"] != 1) {
                row += '<a class="delete">删除</a>';
            }
            </shiro:hasPermission>
            row += '</td>';
            row += '</tr>';
            return row;
        };

        var goPage = function(page) {
            var pageNo = page || $(".pagination .active a").text();

            $.post("${ctx}/manager/list", {"page": pageNo}, function (response) {
                if(response["code"] != 0) {
                    alert(response["message"]);
                    return;
                }

                var $parent = $("#dataList tbody").empty();

                var page = response["data"];
                var list = page["list"];
                for(var i in list) {
                    var row = buildRow(list[i]);
                    $parent.append(row);
                }

                $(".pagination").pagination({
                    "page": page,
                    "stepFun": function(index){
                        return function(){goPage(index);};
                    }
                });
            });
        };

        $("#dataList tbody").on("click", "a.edit", function () {
            $("#editPanel").editPanel({
                "title": "修改 - 系统参数",
                "url": "${ctx}/manager/update?id=" + $(this).parents("[data-id]").attr("data-id"),
                "callback": function (refresh) {
                    goPage();
                }
            });
        }).on("click", "a.delete", function () {
            if(!confirm("是否删除？")){
                return;
            }

            var params = {
                "id": $(this).parents("[data-id]").attr("data-id")
            };
            genesis.post("${ctx}/manager/delete", params, function (response) {
                //closeWin && closeWin(true);
                goPage();
            });
        });

        $(".panel-heading .glyphicon-plus-sign").parent().on("click", function() {
            $("#editPanel").editPanel({
                "title": "新增 - 系统参数",
                "url": "${ctx}/manager/create",
                "callback": function (refresh) {
                    goPage(1);
                }
            });
        });
        $(".panel-heading .glyphicon-refresh").parent().on("click", function() {
            goPage();
        });

        goPage(1);
        */
    });
</script>
</body>
</html>
