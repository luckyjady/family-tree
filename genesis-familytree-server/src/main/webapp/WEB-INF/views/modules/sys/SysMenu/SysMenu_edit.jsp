<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<jsp:useBean id="entity" type="genesis.familytree.server.modules.sys.entity.SysMenu" scope="request" />
<html>
<head>
    <title>主页</title>
</head>
<body>
<div class="container">
    <form id="editForm" onsubmit="return onSubmit();">

        <div class="form-group">
            <label class="control-label" for="parent.name">用户名</label>
            <input type="hidden" id="parent.id" name="parent.id" value="${entity.parent.id}" />
            <input type="text" id="parent.name" name="parent.name" placeholder="请选择父菜单"
                   class="form-control input-lg" value="${entity.parent.name}"/>
            <div class="help-block"></div>
        </div>

        <div class="form-group">
            <label class="control-label" for="name">菜单名称</label>
            <input type="text" id="name" name="name" placeholder="请输入菜单名称"
                   class="form-control input-lg" value="${entity.name}"/>
            <div class="help-block"></div>
        </div>

        <div class="form-group">
            <label class="control-label" for="href">菜单链接</label>
            <input type="text" id="href" name="href" placeholder="请输入菜单链接"
                   class="form-control input-lg" value="${entity.href}"/>
            <div class="help-block"></div>
        </div>

        <div class="form-group">
            <label class="control-label" for="permission">菜单权限</label>
            <input type="text" id="permission" name="permission" placeholder="请输入菜单权限"
                   class="form-control input-lg" value="${entity.permission}"/>
            <div class="help-block"></div>
        </div>
    </form>
</div>

<script>
    var onSubmit = function() {

        closeWin && closeWin();

        return false;
    };

</script>
</body>
</html>
