<%@ page import="genesis.familytree.server.common.security.SystemAuthorizingRealm.Principal" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
    <title>主页</title>
</head>
<body >
<div data-options="region:'north',border:false" style="height:auto;background:#B3DFDA;">
    <div class="easyui-panel" data-options="border: false," style="padding: 5px;">
        <a href="#" class="easyui-menubutton" data-options="menu:'#mm1',plain:true" style="height: 40px;">
            <shiro:principal property="name"/>
        </a>
      <%--  <div class="logo" style="display: inline-block;">
            <img src="http://www.jeasyui.com/images/logo1.png" style="width:150px;height:50px">
        </div>--%>
        <c:set var="principal" value="<%=Principal.getCurrent()%>" />

        <c:forEach var="item" items="${principal.menus}">
            <a href="#" class="easyui-linkbutton" data-options="plain:false"
               style="height: 40px;width: 80px;">${item.name}</a>
        </c:forEach>
        <div id="mm1" style="width:150px;">
            <div data-options="iconCls:'icon-undo'">个人信息</div>
            <div data-options="iconCls:'icon-redo'">配置</div>
            <div class="menu-sep"></div>
            <div><a href="${ctx}/logout" >退出</a></div>

        </div>
    </div>
</div>
<div data-options="region:'west',split:true,title:'系统管理'" style="width:200px;">
    <div id="menuTree" class="easyui-panel" style="width: 100%;height: 100%;border: 0;padding: 5px;">
        <sys:menu />
    </div>

</div>
<div data-options="region:'center',title:'', border: false,">
    <div id="mainTabs" class="easyui-tabs" data-options="plain:true,fit:true">
        <div title="个人面板">
            <iframe frameborder="0" style="border: none;width: 100%;height: 99.2%;"
                    src="${ctx}/dashboard">

            </iframe>
        </div>
    </div>
</div>



<script>
    $(function () {
        var frames = { index: 1, next: function(){
            this.index += 1;
            return this.index;
        }};

        $( "a[data-href]").on("click", function () {
            var $this = $(this);
            var title = $this.text();
            var href = $this.attr("data-href");

            var $tabs = $('#mainTabs');
            var tab = $tabs.tabs("getTab", title);
            if(tab != null) {
                var index = $tabs.tabs("getTabIndex", tab);
                $tabs.tabs("select", index);
                return;
            }

            var index = frames.next();
            $tabs.tabs('add',{
                id: "tab_" + index,
                title: title,
                content: '<iframe id="tabs_frame_'+index+'" scrolling="auto" frameborder="0" style="border: none;width: 100%;height: 99.5%;" src="'+href+'"></iframe>',
//                href: href,
                closable:true,
            });
        });
    });
</script>
</body>
</html>
