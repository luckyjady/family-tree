<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<jsp:useBean id="entity" type="genesis.familytree.server.modules.sys.entity.SysRole" scope="request" />
<html>
<head>
    <title>主页</title>
</head>
<body>
<div class="container">
    <form id="editForm" onsubmit="return onSubmit();">
        <div class="form-group">
            <label class="control-label" for="code">角色编号</label>
            <input type="text" id="code" name="code" placeholder="请输入角色编号"
                   class="form-control input-lg"
                   value="${entity.code}"/>
            <div class="help-block"></div>
        </div>

        <div class="form-group">
            <label class="control-label" for="name">角色名称</label>
            <input type="text" id="name" name="name" placeholder="请输入角色名称"
                   class="form-control input-lg"
                   value="${entity.name}"/>
            <div class="help-block"></div>
        </div>
    </form>
</div>

<script>
    var onSubmit = function() {
        var params = {
            "id": "${entity.id}",
            "code": $("#code").val(),
            "name": $("#name").val()
        };

        genesis.post("${ctx}/role/${empty entity.id ? 'create' : 'update' }", params, function (response) {
            closeWin && closeWin(true);
        });
        return false;
    };

</script>
</body>
</html>
