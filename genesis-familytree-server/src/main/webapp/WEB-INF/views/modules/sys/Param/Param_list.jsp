<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
    <title>主页</title>
</head>
<body>

<table id="dataList">
</table>

<div id="editPanel"></div>

<script>
    $(function () {
        var $dataList = $("#dataList").datagrid({
            url:'${ctx}/param/list',
            method: "post",
            columns:[[
                {field:'id',title:'ID',width:60},
                {field:'paramCode',title:'编号',width:400},
                {field:'paramName',title:'名称',width:250},
                {field:'paramValue',title:'值',width:100},
                {field:'paramGroup',title:'分组',width:100}
            ]],
            pagination: true,
            pageNumber: 1,
            pageSize: 15,
            pageList: [1, 15, 20, 25, 50],
            toolbar: [{
                text:'新增',
                iconCls:'icon-add',
                handler:function(){
                    var node = $("#dataList").datagrid("getSelected");

                    $("#editPanel").editPanel({
                        "title": "",
                        "url": "${ctx}/param/update?id=" + id
                    });
                }
            },{
                text:'修改',
                iconCls:'icon-edit',
                handler:function(){
                    var node = $("#dataList").datagrid("getSelected");
                    if(null == node) {
                        $.messager.alert('错误', '请选择要修改的记录', 'error');
                        return;
                    }
                    var url =  "${ctx}/param/update?id=" + node.id;
                    $('#editPanel').dialog({
                        title: '系统参数编辑',
                        width: 500,
                        //height: 400,
                        closed: false,
                        cache: false,
                        href: url,
                        modal: true,
                        buttons:[{
                            text:'Save',
                            handler:function(){
                                $("#editPanel").find("form").submit();
                            }
                        },{
                            text:'Close',
                            handler:function(){

                            }
                        }],
                        onLoad: function() {
                            $("#editpanel").dialog("center")
                        }
                    });

                  /*  $("#editPanel").editPanel({
                        "title": "",
                        "url": "${ctx}/menu/update?id=" + id
                    });*/
                }
            },{
                text:'删除',
                iconCls:'icon-cancel',
                handler:function(){
                    var node = $("#dataList").datagrid("getSelected");
                    var url = "${ctx}/param/delete?id=" + node.id
                }
            },'-',{
                text:'刷新',
                iconCls:'icon-reload',
                handler:function(){
                    $("#dataList").datagrid("reload");
                }
            }],
            loadFilter: function(response) {
                return {
                    total: response["data"]["count"],
                    rows: response["data"]["list"]
                };
            }
        });

/*
        var $dataList = $("#dataList").dataList({
            "url": "${ctx}/param/list",
            columns: [
                {label: "ID",  property: "id",         width: 80},
                {label: "编号", property: "paramCode"},
                {label: "名称", property: "paramName",  width: 300},
                {label: "值",   property: "paramValue", width: 200},
                {label: "分组", property: "paramGroup", width: 200},
                {
                    label: "操作",
                    property: function ($target, rowIndex, data) {
                        var row = "";
                        <shiro:hasPermission name="sys:param:edit">
                            row += '<a class="edit">修改</a>';
                        </shiro:hasPermission>
                        <shiro:hasPermission name="sys:param:delete">
                            if (data["isSystem"] != 1) {
                                row += '<a class="delete">删除</a>';
                            }
                        </shiro:hasPermission>
                        return row;
                    },
                    width: 110,
                    style: "text-align: center;"
                }
            ],
            "onRowSelected": function(event, $target, data, rowIndex, columnIndex) {
                var $this = $(event.target);

                if($this.is(".edit")) {

                    $("#editPanel").editPanel({
                        "title": "修改 - 系统参数",
                        "url": "${ctx}/param/update?id=" + data.id,
                        "callback": function (refresh) {
                            $dataList.dataList("reload");
                        }
                    });
                }else if($this.is(".delete")){
                    if (!confirm("是否删除？")) {
                        return;
                    }

                    var params = {
                        "id": data["id"]
                    };
                    window.genesis.post("${ctx}/param/delete", params, function (response) {
                        $dataList.dataList("load", 1);
                    });
                }
            }
        });

        $(".panel-heading .glyphicon-plus-sign").parent().on("click", function () {
            $("#editPanel").editPanel({
                "title": "新增 - 系统参数",
                "url": "${ctx}/param/create",
                "callback": function (refresh) {
                    $dataList.dataList("load", 1);
                }
            });
        });
        $(".panel-heading .glyphicon-refresh").parent().on("click", function () {
            $dataList.dataList("reload");
        });

        $dataList.dataList("load", 1);*/
    });
</script>

</body>
</html>
