<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<jsp:useBean id="entity" type="genesis.familytree.server.modules.common.entity.Param" scope="request" />
<html>
<head>
    <title>主页</title>

</head>
<body>
<div class="easyui-panel" data-options="fit:true,border: false," style="padding: 20px;">
    <form id="editForm" onsubmit="return onEditSubmit()">
        <div style="margin-bottom:20px">
            <label class="control-label">参数编码</label>
            <input type="text" name="paramCode" placeholder="请输入参数编码"
                   class="easyui-textbox" style="width: 100%;height: 32px;"
                   value="${entity.paramCode}"
                   ${entity.isSystem eq 0 ? "" : "disabled"}/>
            <div class="help-block"></div>
        </div>

        <div style="margin-bottom:20px">
            <label class="control-label">参数名称</label>
            <input type="text" name="paramName" placeholder="请输入参数名称"
                   class="easyui-textbox" style="width: 100%;height: 32px;"
                   value="${entity.paramName}"
                   ${entity.isSystem eq 0 ? "" : "disabled"}/>
            <div class="help-block"></div>
        </div>

        <div style="margin-bottom:20px">
            <label class="control-label">参数值</label>
            <input type="text" name="paramValue" placeholder="请输入参数值"
                   class="easyui-textbox" style="width: 100%;height: 32px;"
                   value="${entity.paramValue}"/>
            <div class="help-block"></div>
        </div>

        <div style="margin-bottom:20px">
            <label class="control-label">参数分组</label>
            <input type="text" name="paramGroup" placeholder="请输入参数分组"
                   class="easyui-textbox" style="width: 100%;height: 32px;"
                   data-options="required:true"
                   value="${entity.paramGroup}"/>
            <div class="help-block"></div>
        </div>
    </form>
</div>

<script>
    var onEditSubmit = function() {
        var $editForm = $("#editForm");
        if($editForm.form("validate")) {
            var params = {
                "id": "${entity.id}",
                "paramCode": $editForm.find("[name=paramCode]").val(),
                "paramName": $editForm.find("[name=paramName]").val(),
                "paramValue": $editForm.find("[name=paramValue]").val(),
                "paramGroup": $editForm.find("[name=paramGroup]").val()
            };

            genesis.post("${ctx}/param/${empty entity.id ? 'create' : 'update' }", params, function (response) {
                closeWin && closeWin(true);
            });
        }
        return false;
    };
</script>
</body>
</html>
