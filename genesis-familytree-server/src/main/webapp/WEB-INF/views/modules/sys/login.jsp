<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp" %>
<html>
<head>
    <title>登录</title>

    <link rel="stylesheet" href="${ctxStatic}/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctxStatic}/bootstrap/3.3.6/css/bootstrap-theme.min.css">
    <script src="${ctxStatic}/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="${ctxStatic}/bootstrap/3.3.6/js/bootstrap-hover-dropdown.min.js"></script>
</head>
<body>


<div class="container">
<h1>System Sign in</h1>
<hr/>

<form id="loginForm">
    <div class="form-group">
        <label class="control-label" for="username">用户名</label>
        <input type="text" id="username" name="username" placeholder="请输入用户名"
               class="form-control input-lg"/>
        <div class="help-block"></div>
    </div>

    <div class="form-group">
        <label class="control-label" for="password">密码</label>
        <input type="password" id="password" name="password" placeholder="请输入密码"
               class="form-control input-lg"/>
        <div class="help-block"></div>
    </div>

    <div class="form-group">
        <input id="btnLogin" type="submit" value="登录"
               class="btn btn-primary btn-block btn-lg"/>
    </div>
</form>
</div>

<script>
$(function () {
    $("#username").focus();
    $("#loginForm").on("submit", function () {
        var params = {
            "username": $("#username").val(),
            "password": $("#password").val()
        };
        genesis.post("/admin/login", params, function (response) {
            window.location = "${ctx}/index";
        });
        return false;
    });

});
</script>
</body>
</html>
