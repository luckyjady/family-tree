<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<%@ attribute name="content" type="java.util.List" required="true" description="消息内容"%>
<script type="text/javascript">top.$.jBox.closeTip();</script>
<c:if test="${not empty content}">
	<%--<c:if test="${not empty type}"><c:set var="ctype" value="${type}"/></c:if><c:if test="${empty type}"><c:set var="ctype" value="${fn:indexOf(content,'失败') eq -1?'success':'error'}"/></c:if>--%>
    <div id="messageBox">
        <c:forEach var="item" items="${content}">
            <jsp:useBean id="item" type="genesis.familytree.server.common.web.BaseController.Message" />
            <div class="alert alert-${item.type}"><button data-dismiss="alert" class="close">×</button>${item.message}</div>
        </c:forEach>
    </div>
	<%--<script type="text/javascript">
		if(!top.$.jBox.tip.mess){
            top.$.jBox.tip.mess=1;
            top.$.jBox.tip("${content}","${ctype}",
                    {persistent:true,opacity:0});
            $("#messageBox").show();
        }
	</script>--%>
</c:if>