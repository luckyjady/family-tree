window.genesis || (window.genesis = {});

(function ($) {
    genesis.required("/module/loading");
    genesis.required("/module/captcha2");


    var _WIN = window;
    var _PARENT = _WIN.parent;

    var funs;
    if (_WIN != _PARENT) {
        do {
            _WIN = _PARENT;
            _PARENT = _WIN.parent;
        } while (_PARENT != _WIN);


        var post = _WIN.genesis["post"];
        funs = (post && {"post": post}) || null;
    }

    funs = funs || (function () {
            var ajaxResult = function (request, response) {
                var params = request["params"];
                var onBeforeSubmit = request["onBeforeSubmit"];
                var onParamError = request["onParamError"];

                var resultCode = response["code"];
                if (resultCode != 0) {
                    if (resultCode == 1) { // 参数不正确
                        var errors = response["paramErrors"];
                        for (var key in errors) {
                            var msg = errors[key];
                            onParamError(key, msg);
                        }
                    } else if (resultCode == 2) { // 需要验证码
                        var url = request.url;

                        window.genesis.showCaptcha({"url": url, "message": response.message}, function (code) {
                            window.genesis.hideCaptcha(function () {
                                var prms = $.extend(params, {"_captcha": code});
                                request = $.extend(request, {
                                    "params": prms
                                });
                                window.genesis.post(request);
                            });
                        });
                    } else {
                        alert(response["message"]);
                    }
                    return false;
                }
                return true;
            };

            return {
                post: function (opt) {
                    var url = opt["url"];
                    var params = opt["params"];
                    var callback = opt["callback"];
                    var onBeforeSubmit = opt["onBeforeSubmit"];
                    var onParamError = opt["onParamError"];

                    onBeforeSubmit();

                    //window.genesis.showLoading();
                    $.post(url, params, function (response) {
                        //window.genesis.hideLoading();
                        var request = {
                            "url": url,
                            "params": params,
                            "callback": callback,
                            "onBeforeSubmit": onBeforeSubmit,
                            "onParamError": onParamError
                        };
                        if (ajaxResult(request, response)) {
                            callback && callback(response);
                        }
                    });
                }
            };
        })();

    var _onBeforeSubmit = function () {
        var $f = $("form");
        $f.find(".form-group").removeClass("has-error").find(".help-block").empty();
    };

    var _onParamError = function (paramName, errorMessage) {
        $("[name='" + paramName + "']").parent().addClass("has-error").find(".help-block").text(errorMessage);
    };

    window.genesis = $.extend(window.genesis, {
        post: function(url, params, opt) {
            var request;

            if(typeof url == "object"){
                request = url;
            }else {
                if(typeof url == "string") {
                    request = {
                        "url": url,
                        "params": params
                    };
                }

                if(typeof opt == "object"){
                    request = $.extend(request, opt);
                }else if(typeof opt == "function") {
                    request = $.extend(request, {
                        "callback": opt,
                        "onParamError": _onParamError,
                        "onBeforeSubmit": _onBeforeSubmit
                    });
                }
            }

            var callback = request["callback"] || function(){};
            var onBeforeSubmit = request["onBeforeSubmit"] || _onBeforeSubmit;
            var onParamError = request["onParamError"] || _onParamError;

            funs["post"]($.extend(request, {
                "onBeforeSubmit": onBeforeSubmit,
                "onParamError": onParamError
            }));
        }
    });
})(jQuery);


