(function($){
    $.fn.editPanel = (function() {
        var defaultOptions = {
            "title": "编辑",
            "url": ""
        };

        var show = function($modal) {
            $modal.modal("show");
        };

        var hide = function($modal) {
            $modal.modal("hide");
        };

        var init = function($target, opt){
            $target.empty();

            var html =
                '<div class="modal fade" role="dialog">\
                    <div class="modal-dialog" role="document">\
                        <div class="modal-content">\
                            <div class="modal-header">\
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\
                                <h4 class="modal-title">'+ opt["title"] +'</h4>\
                            </div>\
                            <div class="modal-body">\
                                <div class="container-fluid">\
                                    <div class="loading">Loading</div>\
                                    <iframe frameborder="0" style="border: none;display: none;width: 100%;"></iframe>\
                                </div>\
                            </div>\
                            <div class="modal-footer">\
                                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>\
                                <button type="button" class="btn btn-primary">保存</button>\
                            </div>\
                        </div>\
                    </div>\
                </div>';

            var $modal = $(html).appendTo($target);
            opt["modal"] = $modal;

            var $loading = $modal.find(".loading").show();
            var $frame = $modal.find("iframe");
            var $btn = $modal.find(".btn-primary");

            var callback = opt["callback"];

            $btn.off("click").on("click", function () {
                var $form = $("#editForm", $frame[0].contentDocument);
                $form.submit();
            });

            var resize = function(){
                var h = $("body", $frame[0].contentDocument).height();
                var maxHeight = $(window).height() - 220;
                $frame.height(h > maxHeight ? maxHeight : h);
            };


            $frame.hide().off("load").on("load", function() {
                $("body", $frame[0].contentDocument).on("resize", resize);

                $frame.delay(500).show(0, function(){
                    $loading.hide();
                    $frame[0].contentWindow.closeWin = function (refresh) {
                        $modal.modal("hide");
                        callback && callback(refresh);
                    };

                });
            }).attr("src", opt["url"]);

            return $modal;
        };


        return function(opt){
            var $target = $(this);


            if(typeof opt == "string"){
                var options = $target.data("options");
                if(typeof options != "object") {
                    console.error("editPanel non init");
                    return;
                }

                var action = opt;
                var $m = options["modal"];
                if(action == "show"){
                    show($m);
                }else if(action == "hide"){
                    hide($m);
                }
                return;
            }

            options = $.extend(defaultOptions, opt);
            init($target, options);
            $target.data("options", options);
            show(options["modal"]);
        };
    })(/*创建*/);
})(jQuery);