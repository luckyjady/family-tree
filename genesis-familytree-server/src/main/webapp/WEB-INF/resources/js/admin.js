
(function($) {
    var modules = [];
    var cachedScript = function(url, options) {

        options = $.extend(options || {}, {
            dataType: "script",
            cache: true,
            url: url,
            async:false
        });

        return $.ajax(options);
    };

    window.genesis = $.extend(window.genesis, {
        required: function(moduleName){
            if(modules[moduleName]){
                return;
            }
            modules[moduleName] = true;

            var url = ctxStatic + "/js" + moduleName + ".js";
            cachedScript(url).done(function( script, textStatus ) {

            });
        }
    });
})(jQuery);

