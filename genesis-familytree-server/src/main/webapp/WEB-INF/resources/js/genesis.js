(function($){

    genesis.required("/module/loading");
    genesis.required("/module/post");
    genesis.required("/module/captcha2");

    //genesis.required("/module/editPanel");
    genesis.required("/module/pagination");

    //genesis.required("/module/dataList");

    $(window).ajaxStart(function(){
        window.genesis.showLoading();
    }).ajaxStop(function(){
        window.genesis.hideLoading();
    });
})(jQuery);