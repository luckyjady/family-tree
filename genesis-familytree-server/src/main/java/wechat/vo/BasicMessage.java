package wechat.vo;

import java.util.Map;

/**
 * Created by CoderJiang@sina.com on 2016/7/7.
 */
public abstract class  BasicMessage {

    /**
     * 开发者微信号
     */
    private String toUserName;

    /**
     * 发送方帐号（一个OpenID）
     */
    private String fromUserName;

    /**
     * 消息创建时间 （整型）
     */
    private Long createTime;

    /**
     * 消息类型
     */
    private String msgType;

    /**
     * 	消息id，64位整型
     */
    private Long msgId;

    public BasicMessage(){
    }

    public void setSuperValues(Map<String, String> values){
        this.setToUserName(values.get("ToUserName"));
        this.setFromUserName(values.get("FromUserName"));
        this.setCreateTime(Long.valueOf(values.get("CreateTime")));
        this.setMsgType(values.get("MsgType"));
        this.setMsgId(Long.valueOf(values.get("MsgId")));
    }

    public String getToUserName() {
        return toUserName;
    }

    public void setToUserName(String toUserName) {
        this.toUserName = toUserName;
    }

    public String getFromUserName() {
        return fromUserName;
    }

    public void setFromUserName(String fromUserName) {
        this.fromUserName = fromUserName;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public Long getMsgId() {
        return msgId;
    }

    public void setMsgId(Long msgId) {
        this.msgId = msgId;
    }
}
