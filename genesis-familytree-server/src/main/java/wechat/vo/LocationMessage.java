package wechat.vo;

import java.util.Map;

/**
 * Created by CoderJiang@Sina.com on 2016/7/7.
 * 地理位置消息
 */
public class LocationMessage extends BasicMessage {


    /**
     * 地理位置维度
     */
    private int location_X;

    /**
     * 地理位置经度
     */
    private int location_Y;

    /**
     * 地图缩放大小
     */
    private int scale;

    /**
     * 地理位置信息
     */
    private String label;

    public LocationMessage(Map<String, String> values){
        this.setSuperValues(values);
        this.setLocation_X(Integer.valueOf(values.get("Location_X")));
        this.setLocation_Y(Integer.valueOf(values.get("Location_Y")));
        this.setScale(Integer.valueOf(values.get("Scale")));
        this.setLabel(values.get("Label"));
    }

    public int getLocation_X() {
        return location_X;
    }

    public void setLocation_X(int location_X) {
        this.location_X = location_X;
    }

    public int getLocation_Y() {
        return location_Y;
    }

    public void setLocation_Y(int location_Y) {
        this.location_Y = location_Y;
    }

    public int getScale() {
        return scale;
    }

    public void setScale(int scale) {
        this.scale = scale;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
