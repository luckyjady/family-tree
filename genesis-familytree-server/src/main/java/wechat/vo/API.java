package wechat.vo;

/**
 * Created by jiangjunguo on 16-6-30.
 */
public class  API {

    public final static String ACCESS_TOKEN = "https://api.weixin.qq.com/cgi-bin/token";    // 获取token
    public final static String AUTHORIZE = "https://open.weixin.qq.com/connect/oauth2/authorize";   // 用户授权接口
    public final static String USER_ACCESS_TOKEN = "https://api.weixin.qq.com/sns/oauth2/access_token"; // 通过code换取网页授权access_token
    public final static String REFRESH_TOKEN = "https://api.weixin.qq.com/sns/oauth2/refresh_token"; // 刷新access_token（如果需要）
    public final static String USERINFO = "https://api.weixin.qq.com/sns/userinfo"; // 拉取用户信息(需scope为 snsapi_userinfo)
}
