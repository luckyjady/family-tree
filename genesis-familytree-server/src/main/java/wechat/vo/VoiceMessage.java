package wechat.vo;

import java.util.Map;

/**
 * Created by CoderJiang@Sina.com on 2016/7/7.
 * 语音消息
 */
public class VoiceMessage extends BasicMessage {

    /**
     * 语音消息媒体id，可以调用多媒体文件下载接口拉取数据。
     */
    private String MediaId;

    /**
     * 语音格式，如amr，speex等
     */
    private String Format;

    public VoiceMessage(Map<String, String> values){
        this.setSuperValues(values);
        this.setMediaId(values.get("MediaId"));
        this.setFormat(values.get("Format"));
    }

    public String getMediaId() {
        return MediaId;
    }

    public void setMediaId(String mediaId) {
        MediaId = mediaId;
    }

    public String getFormat() {
        return Format;
    }

    public void setFormat(String format) {
        Format = format;
    }
}
