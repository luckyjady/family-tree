package wechat.vo;

import java.util.Map;

/**
 * Created by CoderJiang@Sina.com on 2016/7/7.
 * 文本消息
 */
public class TextMessage extends BasicMessage {

    /**
     * 文本消息内容
     */
    private String content;

    public TextMessage(Map<String, String> values){
        this.setSuperValues(values);
        this.setContent(values.get("Content"));
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }



}
