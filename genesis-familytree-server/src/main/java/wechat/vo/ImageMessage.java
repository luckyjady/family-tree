package wechat.vo;

import java.util.Map;

/**
 * Created by CoderJiang@Sina.com on 2016/7/7.
 * 图片消息
 */
public class ImageMessage extends BasicMessage {

    /**
     * 图片链接
     */
    private String picUrl;

    /**
     * 图片消息媒体id，可以调用多媒体文件下载接口拉取数据。
     */
    private String MediaId;

    public ImageMessage(Map<String, String> values){
        this.setSuperValues(values);
        this.setPicUrl(values.get("PicUrl"));
        this.setMediaId(values.get("MediaId"));
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public String getMediaId() {
        return MediaId;
    }

    public void setMediaId(String mediaId) {
        MediaId = mediaId;
    }
}
