package wechat.vo;

import java.util.Map;

/**
 * Created by CoderJiang@Sina.com on 2016/7/7.
 * 小视频消息
 */
public class ShortVideoMessage extends BasicMessage {


    /**
     * 视频消息媒体id，可以调用多媒体文件下载接口拉取数据。
     */
    private String mediaId;

    /**
     * 视频消息缩略图的媒体id，可以调用多媒体文件下载接口拉取数据。
     */
    private String thumbMediaId;

    public ShortVideoMessage(Map<String, String> values){
        this.setSuperValues(values);
        this.setMediaId("MediaId");
        this.setThumbMediaId("ThumbMediaId");
    }

    public String getMediaId() {
        return mediaId;
    }

    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }

    public String getThumbMediaId() {
        return thumbMediaId;
    }

    public void setThumbMediaId(String thumbMediaId) {
        this.thumbMediaId = thumbMediaId;
    }
}
