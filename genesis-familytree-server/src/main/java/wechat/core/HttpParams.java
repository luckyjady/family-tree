package wechat.core;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by jiangjunguo on 16-6-30.
 */
public class HttpParams {

    private String end;
    private Map<String, String> params = new HashMap<String, String>();

    public void addParam(String key, String value) {
        this.params.put(key, value);
    }

    public String getParam(String key) {
        return params.get(key);
    }

    public String getUrlParam() {
        String url = "";

        Iterator<Map.Entry<String, String>> it = this.params.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> entry = it.next();
            url += entry.getKey();
            url += "=";
            url += entry.getValue();
            if (it.hasNext()) {
                url += "&";
            }
        }
        if(this.end != null && this.end.length() > 0){
            url += this.end;
        }
        return url;
    }

    public void addEnd(String end) {
        this.end = end;
    }
}
