package genesis.familytree.server.modules.home.entity;

/**
 * Created by coderjiang@sina.com on 2016/7/26.
 */
public class HomeRelateType {

    private int id;

    /**
     * 关系类型，对应系统字典表
     */
    private int type;

    /**
     * 关系链构成
     */
    private String chain;

    /**
     * 关系标准名称
     */
    private String name;

    /**
     * 关系备注
     */
    private String remark;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getChain() {
        return chain;
    }

    public void setChain(String chain) {
        this.chain = chain;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public boolean equals(Object obj) {
        return ((HomeRelateType)obj).getId() == this.getId();
    }
}
