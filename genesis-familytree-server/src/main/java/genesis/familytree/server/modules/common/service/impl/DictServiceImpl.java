package genesis.familytree.server.modules.common.service.impl;

import genesis.familytree.server.common.utils.StringUtils;
import genesis.familytree.server.modules.common.dao.IDictDao;
import genesis.familytree.server.modules.common.entity.Dict;
import genesis.familytree.server.modules.common.exception.ParamNullException;
import genesis.familytree.server.modules.common.service.IDictService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by coderjiang@sina.com on 2016/8/4.
 */
@Transactional(readOnly = true)
@Service
public class DictServiceImpl implements IDictService{

    @Resource
    private IDictDao dao;

    @Override
    public List<Dict> getDictListByType(String relate_type) throws ParamNullException {
        if(StringUtils.isBlank(relate_type)){
            throw new ParamNullException("relate_type 不能为空");
        }
        List<Dict> dicts = this.dao.getDictListByType(relate_type);
        return dao.getDictListByType(relate_type);
    }

}
