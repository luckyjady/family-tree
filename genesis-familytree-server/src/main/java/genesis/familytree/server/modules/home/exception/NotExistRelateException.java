package genesis.familytree.server.modules.home.exception;

import genesis.familytree.server.modules.common.exception.BaseException;

/**
 * Created by coderJiang@sina.com on 2016/8/8.
 */
public class NotExistRelateException extends BaseException {

    public NotExistRelateException(){
        super();
    }

    public NotExistRelateException(String errMessage){
        super();
        this.setErrMessage(errMessage);
    }

}
