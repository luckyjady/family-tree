package genesis.familytree.server.modules.common.dao;

import genesis.familytree.server.modules.common.entity.Dict;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by coderjiang@sina.com on 2016/8/4.
 */
@Mapper
public interface IDictDao {
    List<Dict> getDictListByType(@Param("relateType") String relate_type) ;
}
