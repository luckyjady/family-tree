package genesis.familytree.server.modules.home.entity;

/**
 * Created by Administrator on 2016/7/27.
 */
public class HomeRelateTypeTemplate {

    /**
     * ID
     */
    private int id;

    /**
     * 本节点
     */
    private HomeRelateType relate;

    /**
     * 上级节点
     */
    private HomeRelateType parent;

    /**
     * 性别 1 男 2 女
     */
    private int gender;

    /**
     * 备注
     */
    private String remark;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public HomeRelateType getRelate() {
        return relate;
    }

    public void setRelate(HomeRelateType relate) {
        this.relate = relate;
    }

    public HomeRelateType getParent() {
        return parent;
    }

    public void setParent(HomeRelateType parent) {
        this.parent = parent;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public boolean equals(Object obj) {
        return ((HomeRelateTypeTemplate)obj).getId() == this.getId();
    }
}
