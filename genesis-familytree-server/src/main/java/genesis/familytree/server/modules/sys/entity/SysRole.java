/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package genesis.familytree.server.modules.sys.entity;


import genesis.familytree.server.common.persistence.DataEntity;

/**
 * 角色Entity
 *
 * @author ThinkGem
 * @version 2013-12-05
 */
public class SysRole extends DataEntity<SysRole> {

    private String code;
    private String name;    // 角色名称
    private Integer isSystem;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIsSystem() {
        return isSystem;
    }

    public void setIsSystem(Integer isSystem) {
        this.isSystem = isSystem;
    }
}
