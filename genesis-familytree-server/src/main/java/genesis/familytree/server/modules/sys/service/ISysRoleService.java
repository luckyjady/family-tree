package genesis.familytree.server.modules.sys.service;

import genesis.familytree.server.common.persistence.Page;
import genesis.familytree.server.modules.sys.entity.SysRole;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Cheng on 2016/05/09.
 */
public interface ISysRoleService {
    SysRole getById(int id);

    SysRole getByCode(String code);

    List<SysRole> getListByUser(int userId);

    Page<SysRole> getPage(int pageNumber, int pageSize, SysRole sysRole);

    void saveOne(SysRole sysRole);

    void deleteOneById(int roleId);

    @Transactional(readOnly = false)
    void saveRoleMenuList(SysRole role);

    List<SysRole> getAll();
}
