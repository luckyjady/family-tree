package genesis.familytree.server.modules.common.entity;

import java.util.Date;

/**
 * Created by coderjiang@sina.com on 2016/8/4.
 */
public class Dict {

    private int id;

    /**
     * 字典类型
     */
    private String dictType;

    /**
     * 字典的标签
     */
    private String dictLabel;

    /**
     * 字典的值
     */
    private String dictValue;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建者
     */
    private int createBy;

    /**
     * 创建时间
     */
    private Date createDate;

    /**
     * 最后更新者
     */
    private int updateBy;

    /**
     * 最后更新时间
     */
    private Date updateDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDictType() {
        return dictType;
    }

    public void setDictType(String dictType) {
        this.dictType = dictType;
    }

    public String getDictLabel() {
        return dictLabel;
    }

    public void setDictLabel(String dictLabel) {
        this.dictLabel = dictLabel;
    }

    public String getDictValue() {
        return dictValue;
    }

    public void setDictValue(String dictValue) {
        this.dictValue = dictValue;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public int getCreateBy() {
        return createBy;
    }

    public void setCreateBy(int createBy) {
        this.createBy = createBy;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public int getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(int updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}
