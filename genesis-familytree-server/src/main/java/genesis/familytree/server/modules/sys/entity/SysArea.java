/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package genesis.familytree.server.modules.sys.entity;

import genesis.familytree.server.common.persistence.DataEntity;
import org.hibernate.validator.constraints.Length;

/**
 * 区域Entity
 *
 * @author ThinkGem
 * @version 2013-05-15
 */
public class SysArea extends DataEntity<SysArea> {

    private static final long serialVersionUID = 1L;
    private SysArea parent;
    private String code;    // 区域编码
    private String type;    // 区域类型（1：国家；2：省份、直辖市；3：地市；4：区县）
	private String name;

    public SysArea() {
        super();
    }

    public SysArea(Integer id) {
        super(id);
    }

    public SysArea getParent() {
        return parent;
    }

    public void setParent(SysArea parent) {
        this.parent = parent;
    }

    @Length(min = 1, max = 1)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Length(min = 0, max = 100)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    @Override
    public String toString() {
        return name;
    }
}