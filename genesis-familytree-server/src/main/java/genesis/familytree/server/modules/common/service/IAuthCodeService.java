package genesis.familytree.server.modules.common.service;

import genesis.familytree.server.modules.common.exception.ParamNullException;
import genesis.familytree.server.modules.common.exception.AuthCodeErrorException;
import genesis.familytree.server.modules.common.exception.needImageAuthCodeException;

/**
 * Created by coderjiang@sina.com on 2016/7/21.
 */

public interface IAuthCodeService {

    /**
     * 检测手机验证码是否需要图片验证码
     * @param mobile
     * @return true 需要验证码  false 不需要验证码
     */
    boolean checkSendMessage(String mobile) throws ParamNullException;

    /**
     * 发送手机验证码
     * @param mobile 手机号
     * @param needCode 是否需要验证码
     * @param code 用户输入的图形验证码
     * @param sessionImageCode session中存储的图形验证码
     * @return
     */
    String sendSMS(String mobile, Boolean needCode, String code, String sessionImageCode) throws AuthCodeErrorException, ParamNullException, needImageAuthCodeException;
}
