package genesis.familytree.server.modules.sys.model;

import genesis.familytree.server.common.model.TreeModel;
import genesis.familytree.server.modules.sys.entity.SysMenu;

/**
 *
 */
public class TreeMenu extends TreeModel<SysMenu> {

    public TreeMenu(SysMenu data, String name, int level, boolean isShow) {
        super(data, name, level, isShow);
    }

    public Integer getSort() {
        return this.getData().getSort();
    }

    public String getHref() {
        return this.getData().getHref();
    }

    public String getPermission() {
        return this.getData().getPermission();
    }
}
