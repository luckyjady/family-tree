package genesis.familytree.server.modules.home.dao;

import genesis.familytree.server.modules.home.entity.HomeBaseRelateType;
import genesis.familytree.server.modules.home.entity.HomeRelate;
import genesis.familytree.server.modules.home.entity.HomeRelateType;
import genesis.familytree.server.modules.home.entity.HomeRelateTypeTemplate;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by coderjiang@sina.com on 2016/7/27.
 */
@Mapper
public interface IHomeRelateTypeDao {

    List<HomeRelateTypeTemplate> getHomeRelateTypeByUserWithTemplate();

    List<HomeRelate> getHomeRelateTypeByTypeID(@Param("typeID") int typeID);

    HomeRelateType getHomeRelateTypeByID(@Param("relationTypeID") int relationTypeID);

    /**
     * 通过关系标志，获取基础属性
     * @param sign
     * @return
     */
    HomeBaseRelateType getBaseRelateTypeBySign(@Param("sign") String sign);

    /**
     * 通过关系链获取关系类型
     * @param relateChain
     * @return
     */
    HomeRelateType getReactHomeRelateTypeBySignChain(@Param("chain") String relateChain);

}
