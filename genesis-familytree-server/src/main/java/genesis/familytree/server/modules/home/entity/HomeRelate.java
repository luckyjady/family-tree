package genesis.familytree.server.modules.home.entity;

/**
 * Created by CoderJiang@sina.com on 2016/7/26.
 */
public class HomeRelate {

    private int id;

    /**
     * 申请用户
     */
    private int fromUserID;

    /**
     * 绑定用户
     */
    private int toUserID;

    /**
     * 绑定的用户名称
     */
    private String toUserName;

    /**
     * 邀请关系
     */
    private HomeBaseRelateType familyType;

    /**
     * 邀请时关系名称
     */
    private String familyTypeName;

    /**
     * 备注
     */
    private String remarks;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFromUserID() {
        return fromUserID;
    }

    public void setFromUserID(int fromUserID) {
        this.fromUserID = fromUserID;
    }

    public int getToUserID() {
        return toUserID;
    }

    public void setToUserID(int toUserID) {
        this.toUserID = toUserID;
    }

    public String getToUserName() {
        return toUserName;
    }

    public void setToUserName(String toUserName) {
        this.toUserName = toUserName;
    }

    public HomeBaseRelateType getFamilyType() {
        return familyType;
    }

    public void setFamilyType(HomeBaseRelateType familyType) {
        this.familyType = familyType;
    }

    public String getFamilyTypeName() {
        return familyTypeName;
    }

    public void setFamilyTypeName(String familyTypeName) {
        this.familyTypeName = familyTypeName;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Override
    public int hashCode() {
        return this.id;
    }

    @Override
    public boolean equals(Object obj) {

        return ((HomeRelate)obj).getId() == this.id;
    }
}
