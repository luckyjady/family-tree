package genesis.familytree.server.modules.home.dao;

import genesis.familytree.server.modules.home.entity.HomeUserWechat;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created by coderjiang@sina.com on 2016/7/20.
 */
@Mapper
public interface IHomeUserWechatDao {

    HomeUserWechat getHomeUserWeChatByOpenID(String openid);

    int saveOne(HomeUserWechat homeUserWechat);
}
