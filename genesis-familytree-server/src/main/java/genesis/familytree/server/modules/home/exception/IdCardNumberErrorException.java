package genesis.familytree.server.modules.home.exception;

import genesis.familytree.server.modules.common.exception.BaseException;

/**
 * Created by CoderJiang@Sina.com on 2016/7/26.
 */
public class IdCardNumberErrorException extends BaseException {
    public IdCardNumberErrorException(String 身份证号码格式不正确) {
    }
}
