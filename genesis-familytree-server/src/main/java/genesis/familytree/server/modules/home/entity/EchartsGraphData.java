package genesis.familytree.server.modules.home.entity;

/**
 * Created by coderJiang@sina.com on 2016/7/26.
 */
public class EchartsGraphData {

    /**
     * 节点名称
     */
    private String name;

    /**
     * 节点数值
     */
    private String value;
    public EchartsGraphData(){ }
    public EchartsGraphData(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
