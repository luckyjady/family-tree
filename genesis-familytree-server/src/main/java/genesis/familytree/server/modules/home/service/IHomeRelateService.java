package genesis.familytree.server.modules.home.service;

import genesis.familytree.server.modules.home.entity.HomeRelate;
import genesis.familytree.server.modules.home.entity.HomeRelateType;
import genesis.familytree.server.modules.home.entity.HomeRelateTypeTemplate;
import genesis.familytree.server.modules.home.entity.HomeUser;

import java.util.List;

/**
 * Created by coderJiang@Sina.com on 2016/7/26.
 */
public interface IHomeRelateService {
    /**
     * 根据用户获取关系
     *
     * @param userID
     * @return
     */
    List<HomeRelate> getAllRelatesByUserID(int userID);

    /**
     * 自己添加关系
     * @param uid 添加人id
     * @param fullname 被添加人全名
     * @param idcard    被添加人身份证
     * @param mobile    被添加人手机
     * @param relationID 被添加人与添加人的关系
     */
    void addRelation(int uid, String fullname, String idcard, String mobile, int relationID);

    /**
     * 根绝用户id获取角色关系
     * @param uid
     * @param baseRelate
     * @return
     */
    List<HomeRelate> getBaseRelate(int uid, String baseRelate);

    /**
     * 获取以某个用户开始的关系
     * @param userID
     * @return
     */
    List<HomeRelate> getRelatesFromUser(int userID);

    /**
     *  获取用户上一级关系的名称
     * @param relateChain    关系类型 chain
     * @param userID    用户
     * @return
     */
    String getUpperRelateName(String relateChain, int userID);
}

