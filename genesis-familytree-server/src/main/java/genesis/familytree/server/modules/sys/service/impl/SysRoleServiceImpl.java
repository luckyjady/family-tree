package genesis.familytree.server.modules.sys.service.impl;

import genesis.familytree.server.common.exception.GenesisException;
import genesis.familytree.server.common.persistence.Page;
import genesis.familytree.server.common.service.BaseService;
import genesis.familytree.server.modules.sys.dao.ISysRoleDao;
import genesis.familytree.server.modules.sys.entity.SysMenu;
import genesis.familytree.server.modules.sys.entity.SysRole;
import genesis.familytree.server.modules.sys.service.ISysMenuService;
import genesis.familytree.server.modules.sys.service.ISysRoleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 *
 */
@Transactional(readOnly = true)
@Service
public class SysRoleServiceImpl extends BaseService implements ISysRoleService{

    @Resource private ISysRoleDao roleDao;
    @Resource private ISysMenuService sysMenuService;

    @Override
    public SysRole getById(int id) {
        return roleDao.getById(id);
    }

    @Override
    public SysRole getByCode(String code) {
        return roleDao.getByCode(code);
    }

    @Override
    public List<SysRole> getListByUser(int userId) {
        return this.roleDao.findListByUser(userId);
    }

    @Override
    public Page<SysRole> getPage(int pageNumber, int pageSize, SysRole sysRole) {
        Page<SysRole> page = new Page<>(pageNumber, pageSize);
        sysRole.setPage(page);
        List<SysRole> roles = this.roleDao.findList(sysRole);
        page.setList(roles);

        return page;
    }

    @Override
    @Transactional(readOnly = false)
    public void saveOne(SysRole vo) {
        SysRole po;
        boolean isNew = vo.getId() == null;
        if (isNew) {
            po = new SysRole();
            po.setIsSystem(0);
            po.setCreateBy(vo.getCreateBy());
            po.setCreateDate(vo.getCreateDate());
        }else {
            po = this.roleDao.getById(vo.getId());
            if (null == po) {
                throw new GenesisException("记录不存在");
            }
        }

        if (po.getIsSystem() == 0) {
            String roleCode = vo.getCode();

            SysRole temp = this.roleDao.getByCode(roleCode);
            if (temp != null && !Objects.equals(temp.getId(), po.getId())) {
                throw new GenesisException("重复的角色编号");
            }

            po.setCode(roleCode);
            po.setName(vo.getName());
        }

        po.setUpdateBy(vo.getUpdateBy());
        po.setUpdateDate(vo.getUpdateDate());

        if (isNew) {
            roleDao.saveOne(po);
        } else {
            roleDao.updateOne(po);
        }
    }

    @Override
    @Transactional(readOnly = false)
    public void deleteOneById(int roleId) {
        roleDao.deleteById(roleId);

        roleDao.deleteRoleMenuByRole(roleId);
    }

    @Override
    @Transactional(readOnly = false)
    public void saveRoleMenuList(SysRole role) {
        roleDao.saveRoleMenuList(role);
    }

    @Override
    public List<SysRole> getAll() {
        throw new UnsupportedOperationException();
    }

}
