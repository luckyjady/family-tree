package genesis.familytree.server.modules.home.entity;

import java.sql.Date;

/**
 * Created by CoderJiang@Sina.com on 2016/7/22.
 */
public class CommonAuthCode {

    /**
     * ID
     */
    private int id;

    /**
     * 发送目标
     */
    private String target;

    /**
     * 发送内容
     */
    private String content;

    /**
     * 发送用户ID
     */
    private int userID;

    /**
     * 发送时间
     */
    private Date addTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }
}
