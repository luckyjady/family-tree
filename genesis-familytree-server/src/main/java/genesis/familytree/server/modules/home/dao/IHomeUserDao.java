package genesis.familytree.server.modules.home.dao;

import genesis.familytree.server.modules.home.entity.HomeUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * Created by coderJiang@Sina.com on 2016/7/12.
 */
@Mapper
public interface IHomeUserDao {

    /**
     * 通过OpenID获取用户对象
     *
     * @param openID
     * @return
     */
    HomeUser getHomeUserByOpenID(@Param("openID") String openID);

    /**
     * 添加一个用户
     *
     * @param homeUser
     * @return
     */
    void addHomeUser(HomeUser homeUser);

    /**
     * 根据用户ID拿用户
     *
     * @param userID
     * @return
     */
    HomeUser getHomeUserByUserID(@Param("userID") int userID);

    /**
     * 通过身份证拿用户
     * @param idcard
     * @return
     */
    HomeUser getHomeUserByIdcard(@Param("idcard") String idcard);

    /**
     * 通过手机获取用户
     * @param mobile
     * @return
     */
    HomeUser getHomeUserByMobile(@Param("mobile") String mobile);
}
