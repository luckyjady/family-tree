package genesis.familytree.server.modules.home.service.impl;

import genesis.familytree.server.common.utils.DevUtils;
import genesis.familytree.server.common.utils.StringUtils;
import genesis.familytree.server.modules.home.dao.IHomeRelateDao;
import genesis.familytree.server.modules.home.entity.HomeBaseRelateType;
import genesis.familytree.server.modules.home.entity.HomeRelate;
import genesis.familytree.server.modules.home.entity.HomeRelateType;
import genesis.familytree.server.modules.home.entity.HomeUser;
import genesis.familytree.server.modules.home.service.IHomeRelateService;
import genesis.familytree.server.modules.home.service.IHomeRelateTypeService;
import genesis.familytree.server.modules.home.service.IHomeUserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by coderjiang@sina.com on 2016/7/26.
 */
@Transactional(readOnly = true)
@Service
public class HomeRelateServiceImpl implements IHomeRelateService {

    @Resource
    private IHomeRelateDao dao;

    @Resource
    private IHomeRelateTypeService homeRelateTypeService;

    @Resource
    private IHomeUserService homeUserService;

    @Override
    public List<HomeRelate> getAllRelatesByUserID(int userID) {

        if (DevUtils.isDev()) {
            userID = 1;
        }

        List<HomeRelate> homeRelates = new ArrayList<HomeRelate>();

        this.recursiveGetRelatesByUserID(userID, homeRelates);

        return homeRelates;
    }

    @Override
    @Transactional(readOnly = false)
    public void addRelation(int uid, String fullname, String idcard, String mobile, int relationTypeID) {
        HomeRelateType homeRelateType = this.homeRelateTypeService.getHomeRelateTypeByID(relationTypeID);

        String[] baseRelates = homeRelateType.getChain().split(",");

        int curUserID = uid;
        // 获取关系链 并 创建没有的成员
        for(int i = 0; i < baseRelates.length; i++){
            if( i == baseRelates.length - 1){
                // 添加基础关系
                HomeRelate homeRelate = this.addBaseRelation(curUserID, fullname, idcard, mobile, baseRelates[i]);
            }else{
                List<HomeRelate> homeRelateList = this.getBaseRelate(curUserID, baseRelates[i]);
                if(homeRelateList == null || homeRelateList.size() == 0){
                    // 添加临时基础关系
                    HomeRelate homeRelate = this.addBaseRelation(curUserID, "", "", "", baseRelates[i]);
                    curUserID = homeRelate.getToUserID();
                }else{
                    // 已有关系
                    curUserID = homeRelateList.get(0).getToUserID();
                }
            }

        }



        System.out.println(homeRelateType.getChain());
        //F,XS,S&L

    }

    /**
     * 添加关系
     * @param curUserID
     * @param fullname
     * @param idcard
     * @param mobile
     * @param baseRelate
     * @return
     */
    @Transactional(readOnly = false)
    private HomeRelate addBaseRelation(int curUserID, String fullname, String idcard, String mobile, String baseRelate) {

        // 搜索是否已有用户
        HomeUser homeUser = this.homeUserService.getOne(idcard, mobile);
        if(homeUser == null){
            homeUser = this.homeUserService.addHomeUser(fullname, idcard, mobile);
        }

        // 获取基础关系关系
        HomeBaseRelateType homeBaseRelateType = this.homeRelateTypeService.getOneBaseRelateTypeBySign(baseRelate);

        //添加关系

        HomeRelate homeRelate = new HomeRelate();
        homeRelate.setFromUserID(curUserID);
        homeRelate.setToUserID(homeUser.getId());
        homeRelate.setFamilyType(homeBaseRelateType);

        this.dao.addOne(homeRelate);

        return homeRelate;
    }

    @Override
    public List<HomeRelate> getBaseRelate(int uid, String baseRelate) {
        switch (baseRelate){
            case "W":
            case "F":
            case "M":
                return this.dao.getBaseRelate(uid, baseRelate);
        default:
            throw new RuntimeException(baseRelate);
        }

    }

    @Override
    public List<HomeRelate> getRelatesFromUser(int userID) {
        return this.dao.getRelatesFromUser(userID);
    }

    @Override
    public String getUpperRelateName(String relateChain, int userID) {
        if(!relateChain.contains(",")){
            return "自己";
        }
        String[] baseRelates = relateChain.substring(0, relateChain.indexOf(",")).split(",");
        List<HomeRelate> homeRelateList = this.getAllRelatesByUserID(userID);

        HomeRelate targetHomeRelate = null;

        for(String baseRelateSign : baseRelates){

            for(HomeRelate homeRelate : homeRelateList){
                if(homeRelate.getFamilyType().getSign().equals(baseRelateSign) && homeRelate.getFromUserID() == userID){
                    targetHomeRelate = homeRelate;
                    break;
                }
            }
        }

        HomeUser homeUser = this.homeUserService.getHomeUserByUserID(targetHomeRelate.getToUserID());

        if(StringUtils.isNotBlank(homeUser.getRealName() )){
            return homeUser.getRealName();
        }

        HomeRelateType homeRelateType = this.homeRelateTypeService.getHomeRelateTypeByChain(relateChain.substring(0, relateChain.indexOf(",")));

        return homeRelateType.getName();
    }


    /**
     * 递归获取所有关系
     * @param userID    用户ID
     * @param homeRelates   关系列表
     */
    private void recursiveGetRelatesByUserID(int userID, List<HomeRelate> homeRelates) {

        List<HomeRelate> curRelates = this.dao.getRelatesByUserID(userID);

        // 退出循环
        boolean token = true;
        for(HomeRelate homeRelate : curRelates){
            if(!homeRelates.contains(homeRelate)){
                token = false;
                break;
            }
        }
        if(token){
            return;
        }

        // 添加不重复的
        for(HomeRelate homeRelate : curRelates){
            if(!homeRelates.contains(homeRelate)){
                homeRelates.add(homeRelate);
            }
        }

        // 递归
        for(HomeRelate homeRelate : curRelates){
            this.recursiveGetRelatesByUserID(homeRelate.getFromUserID(), homeRelates);
            this.recursiveGetRelatesByUserID(homeRelate.getToUserID(), homeRelates);
        }

    }
}
