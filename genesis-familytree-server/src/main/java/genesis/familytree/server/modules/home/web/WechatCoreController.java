package genesis.familytree.server.modules.home.web;

import genesis.familytree.server.modules.home.entity.HomeUser;
import genesis.familytree.server.modules.home.entity.HomeUserWechat;
import genesis.familytree.server.modules.home.service.IHomeUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.xml.sax.SAXException;
import wechat.controller.WechatSupport;
import wechat.vo.WeChatAccount;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

/**
 * Created by CoderJiang@Sina.com on 2016/6/27.
 */
@RequestMapping("/weixin")
@Controller
public class WechatCoreController extends WechatSupport {

    private static final Logger log = LoggerFactory.getLogger(WechatCoreController.class);
    private WeChatAccount acc = new WeChatAccount() ;

    protected WechatCoreController() throws ParserConfigurationException, SAXException {
    }

    @Resource private IHomeUserService service;

    @Override
    public void init() {
        log.info("====== 设置微信信息 =======");
        acc.setAppID("wxcccad6d519a9e846");
        acc.setAppSecret("c40d23856e2c9908c5b5fc82dd6aee0d");
        acc.setToken("weixin");

        weChatCore.setAccount(acc);
    }

    @RequestMapping("/checkSignature")
    public void wechatCore(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        if("GET".equals(req.getMethod())){
            log.debug("invoke checkSignature method");
            this.checkSignature(req, resp);
        }else{
            log.debug("invoke handle method");
            this.handle(req.getInputStream());
        }
    }

    @RequestMapping("/accesstoken")
    public void accessToken() throws Exception {
        log.info("accesstoken method");
        this.getUserAccessToken();
    }

    @RequestMapping("/register")
    public void register(HttpServletResponse resp) throws IOException {
        String url = this.register("snsapi_userinfo", "http://www.ftree.cn/weixin/registercallback");
        resp.sendRedirect(url);
    }

    @RequestMapping("/registercallback")
    public void registerCallback(HttpServletRequest req, HttpServletResponse resp, HttpSession session) throws IOException {
        HomeUserWechat homeUserWechat = this.service.registerFromWechat(this, req.getParameter("code"));
        session.setAttribute("homeUserWechat", homeUserWechat);

        HomeUser homeUser = this.service.findByOpenID(homeUserWechat.getOpenId());
        if(homeUser==null){
            session.setAttribute("nickname", homeUserWechat.getNickname());
            session.setAttribute("openID", homeUserWechat.getOpenId());
            resp.sendRedirect("/index#/register");
        }else{
            req.getSession().setAttribute("homeUser", homeUser);
            resp.sendRedirect("/index");
        }
    }

    @RequestMapping("/test")
    public void test(){
        this.service.getHomeUserByOpenID("openid");
    }
}
