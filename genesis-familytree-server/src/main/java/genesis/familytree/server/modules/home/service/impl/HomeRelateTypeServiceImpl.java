package genesis.familytree.server.modules.home.service.impl;

import genesis.familytree.server.common.utils.DevUtils;
import genesis.familytree.server.common.utils.StringUtils;
import genesis.familytree.server.modules.home.dao.IHomeRelateTypeDao;
import genesis.familytree.server.modules.home.entity.HomeBaseRelateType;
import genesis.familytree.server.modules.home.entity.HomeRelate;
import genesis.familytree.server.modules.home.entity.HomeRelateType;
import genesis.familytree.server.modules.home.entity.HomeRelateTypeTemplate;
import genesis.familytree.server.modules.home.exception.NotExistRelateException;
import genesis.familytree.server.modules.home.service.IHomeRelateService;
import genesis.familytree.server.modules.home.service.IHomeRelateTypeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

/**
 * Created by coderjiang@sina.com on 2016/7/27.
 */
@Transactional(readOnly = true)
@Service
public class HomeRelateTypeServiceImpl implements IHomeRelateTypeService {


    @Resource
    private IHomeRelateTypeDao dao;

    @Resource
    private IHomeRelateService relateService;

    @Override
    public List<HomeRelateTypeTemplate> getHomeRelateTypeByUserWithTemplate() {

        if (DevUtils.isDev()) {
            return this.dao.getHomeRelateTypeByUserWithTemplate();
        }

        return this.dao.getHomeRelateTypeByUserWithTemplate();
    }

    @Override
    public List<HomeRelate> getHomeRelateTypeByTypeID(int id) {
        return this.dao.getHomeRelateTypeByTypeID(id);
    }

    @Override
    public HomeRelateType getHomeRelateTypeByID(int relationTypeID) {
        return dao.getHomeRelateTypeByID(relationTypeID);
    }

    @Override
    public HomeBaseRelateType getOneBaseRelateTypeBySign(String sign) {
        // LB OB XB B&L (M,XB,D&O) 表姐

        // 修正 X项
        if (sign.startsWith("X")) {
            sign = "O" + sign.substring(1);
        }

        // 砍掉&后面
        if (sign.contains("&")) {
            sign = sign.split("&")[0];
        }

        return this.dao.getBaseRelateTypeBySign(sign);
    }

    @Override
    public HomeRelateType getReactHomeRelateTypeByUserId(int userID, int targetUserID) {

        List<HomeRelate> homeRelateList = this.relateService.getAllRelatesByUserID(userID);

        String relateChain = recursiveGetRelateType(userID, homeRelateList, "", targetUserID);

        relateChain.replaceAll("X", "_");   // 替换所有X
        if(relateChain.startsWith(",")){
            relateChain = relateChain.substring(1);
        }

        return this.dao.getReactHomeRelateTypeBySignChain(relateChain);
    }

    @Override
    public HomeRelateType getHomeRelateTypeByChain(String chain) {
        return this.dao.getReactHomeRelateTypeBySignChain(chain);
    }

    /**
     * 递归查找关系链
     * @param curUserID    当前用户ID
     * @param homeRelateList    可能的关系列表
     * @param curBaseRelate    当前关系链
     * @param targetUserID      目前关系用户ID
     * @return
     * @throws NotExistRelateException
     */
    private String recursiveGetRelateType(int curUserID, List<HomeRelate> homeRelateList, String curBaseRelate, int targetUserID){
        for(HomeRelate relate : homeRelateList) {

            if(relate.getToUserID() == targetUserID && relate.getFromUserID() == curUserID){
                // 找到
                return curBaseRelate + "," + relate.getFamilyType().getSign();

            }else if(relate.getFromUserID() == curUserID){
                // 继续找
                String recursiveRelate = this.recursiveGetRelateType(relate.getToUserID(), homeRelateList, curBaseRelate + "," + relate.getFamilyType().getSign(), targetUserID);
                if(StringUtils.isNotBlank(recursiveRelate)){
                    return recursiveRelate;
                }
            }
        }
        return "";
    }


}
