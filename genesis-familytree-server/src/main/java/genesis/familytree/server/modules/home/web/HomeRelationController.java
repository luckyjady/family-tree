package genesis.familytree.server.modules.home.web;

import genesis.familytree.server.common.JsonResult;
import genesis.familytree.server.modules.common.entity.Dict;
import genesis.familytree.server.modules.common.exception.ParamNullException;
import genesis.familytree.server.modules.common.service.IDictService;
import genesis.familytree.server.modules.home.entity.HomeRelate;
import genesis.familytree.server.modules.home.service.IHomeRelateTypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 *
 */
@Controller
public class HomeRelationController {
    private static final Logger LOG = LoggerFactory.getLogger(HomeRelationController.class);

    @Resource
    private IDictService dictService;

    @Resource
    private IHomeRelateTypeService homeRelateTypeService;

    @ResponseBody
    @RequestMapping("ajax/relationtype.json")
    public Object relationType(){

        JsonResult result = JsonResult.create();

        List<Dict> dictList = null;
        try {
            dictList = dictService.getDictListByType("relate_type");

            result.success("data", dictList);

        } catch (ParamNullException e) {
            LOG.warn(e.getErrMessage());
            result.error(-1, e.getErrMessage());
        }

        return result;
    }

    @ResponseBody
    @RequestMapping("ajax/relationSubType.json")
    public Object relationSubType(int id) {

        JsonResult result = JsonResult.create();

        List<HomeRelate>  homeRelateList = this.homeRelateTypeService.getHomeRelateTypeByTypeID(id);

        result.success("data", homeRelateList);

        return result;
    }


}
