package genesis.familytree.server.modules.common.service;

import genesis.familytree.server.modules.common.entity.Dict;
import genesis.familytree.server.modules.common.exception.ParamNullException;

import java.util.List;

/**
 * Created by coderjiang@sina.com on 2016/8/4.
 */

public interface IDictService {

    /**
     * 根绝类型获取字典列表
     * @param relate_type
     * @return
     */
    List<Dict> getDictListByType(String relate_type) throws ParamNullException;
}
