package genesis.familytree.server.modules.common.exception;

/**
 * Created by Administrator on 2016/7/22.
 */
public class ParamNullException extends BaseException {

    public ParamNullException(String errMessage) {
        super();
        this.setErrMessage(errMessage);
    }

}
