package genesis.familytree.server.modules.home.service;

import genesis.familytree.server.modules.home.entity.*;
import genesis.familytree.server.modules.home.exception.NotExistRelateException;

import java.util.List;

/**
 * Created by coderJiang@sina.com on 2016/7/27.
 */
public interface IHomeRelateTypeService {

    List<HomeRelateTypeTemplate> getHomeRelateTypeByUserWithTemplate();

    /**
     * 通过关系类型获取关系列表
     * @param id
     * @return
     */
    List<HomeRelate> getHomeRelateTypeByTypeID(int id);

    /**
     * 通过ID获取关系类型
     * @param relationTypeID
     * @return
     */
    HomeRelateType getHomeRelateTypeByID(int relationTypeID);

    /**
     * 通过基础关系标识，获取关系
     * @param baseRelate
     * @return
     */
    HomeBaseRelateType getOneBaseRelateTypeBySign(String baseRelate);

    /**
     * 获取后面用户与前面用户的关系
     * @param userID
     * @param toUserID
     * @return
     */
    HomeRelateType getReactHomeRelateTypeByUserId(int userID, int toUserID);

    /**
     * 通过关系链获取关系类型
     * @param chain
     * @return
     */
    HomeRelateType getHomeRelateTypeByChain(String chain);
}
