package genesis.familytree.server.modules.home.dao;

import genesis.familytree.server.modules.home.entity.HomeRelate;
import genesis.familytree.server.modules.home.entity.HomeRelateTypeTemplate;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by coderJiang@sina.com on 2016/7/26.
 */
@Mapper
public interface IHomeRelateDao {

    /**
     * 通过用户ID获取关系信息
     * @param userID
     * @return
     */
    List<HomeRelate> getRelatesByUserID(@Param("userID") int userID);

    /**
     * 根据用户ID和关系获取关系列表
     * @param uid
     * @param baseRelate
     * @return
     */
    List<HomeRelate> getBaseRelate(@Param("userID") int uid, @Param("relate") String baseRelate);

    void addOne(HomeRelate homeRelate);

    /**
     * 获取从某个用户开始的关系列表
     * @param userID
     * @return
     */
    List<HomeRelate> getRelatesFromUser(@Param("userID") int userID);
}
