package genesis.familytree.server.modules.sys.service;

import genesis.familytree.server.modules.sys.entity.SysMenu;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 *
 */
public interface ISysMenuService {

    SysMenu getById(Integer id);

    List<SysMenu> getListByRole(int roleId);

    void saveOne(SysMenu sysMenu);

    void deleteOneById(int id);

    List<SysMenu> getAll();

    List<SysMenu> getByUserId(int userId);
}
