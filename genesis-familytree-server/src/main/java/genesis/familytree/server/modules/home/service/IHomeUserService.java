package genesis.familytree.server.modules.home.service;

import genesis.familytree.server.modules.common.exception.AuthCodeErrorException;
import genesis.familytree.server.modules.common.exception.ParamNullException;
import genesis.familytree.server.modules.home.entity.HomeRelate;
import genesis.familytree.server.modules.home.entity.HomeUser;
import genesis.familytree.server.modules.home.entity.HomeUserWechat;
import genesis.familytree.server.modules.home.exception.IdCardNumberErrorException;
import wechat.controller.WechatSupport;
import wechat.vo.UserAccessTokenResponse;
import wechat.vo.UserInfo;
import wechat.vo.UserRefreshTokenResponse;

import java.util.List;

/**
 * Created by coderjiang@sina.com on 2016/7/8.
 */
public interface IHomeUserService {

    /**
     * 通过微信回调注册并获取用户信息
     * @param support 控制器包含wechat core
     * @param code 用户授权code
     */
    public HomeUserWechat registerFromWechat(WechatSupport support, String code);

    /**
     * 根据OpenID获取用户信息
     * @param openID 微信openid
     * @return
     */
    HomeUser getHomeUserByOpenID(String openID);

    HomeUserWechat getHomeUserWechatByOpenID(String openid);

    HomeUserWechat addHomeUserWeChat(UserAccessTokenResponse userAccessToken, UserRefreshTokenResponse refreshTokenResponse, UserInfo userInfo);

    /**
     * 插入一个对象
     * @param homeUserWechat
     * @return
     */
    HomeUserWechat addHomeUserWechat(HomeUserWechat homeUserWechat);

    /**
     * 根绝openID查询用户
     * @param openId
     * @return
     */
    HomeUser findByOpenID(String openId);

    /**
     * 新建用户
     * @param nickname  昵称
     * @param mobile    手机
     * @param fullname  全名
     * @param idcard    身份证
     * @param code      短信验证码
     * @param smsCode   session短信验证码
     * @param openID OpenID
     * @return  用户
     */
    HomeUser addHomeUser(String nickname, String mobile, String fullname, String idcard, String code, String smsCode, String openID) throws AuthCodeErrorException, IdCardNumberErrorException;

    /**
     * 根绝用户ID拿用户信息
     * @param userID
     * @return
     */
    HomeUser getHomeUserByUserID(int userID);

    /**
     * 根据不同信息定位一个人
     * @param idcard
     * @param mobile
     * @return
     */
    HomeUser getOne(String idcard, String mobile);

    /**
     * 添加用户
     * @param fullname
     * @param idcard
     * @param mobile
     * @return
     */
    HomeUser addHomeUser(String fullname, String idcard, String mobile);
}
