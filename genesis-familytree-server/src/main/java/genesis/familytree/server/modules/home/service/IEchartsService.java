package genesis.familytree.server.modules.home.service;

import genesis.familytree.server.modules.home.entity.EchartsGraphData;
import genesis.familytree.server.modules.home.entity.EchartsGraphMap;
import genesis.familytree.server.modules.home.entity.HomeRelate;
import genesis.familytree.server.modules.home.entity.HomeUser;

import java.util.List;

/**
 * Created by coderjiang@sina.com on 2016/7/26.
 */
public interface IEchartsService {


    /**
     * 根据用户ID获取图数据
     * @param id
     * @return
     */
    EchartsGraphMap getEchartsGraphMapByUser(int userID);
}
