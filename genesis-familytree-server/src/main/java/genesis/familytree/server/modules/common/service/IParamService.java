package genesis.familytree.server.modules.common.service;

import genesis.familytree.server.common.exception.GenesisException;
import genesis.familytree.server.common.persistence.Page;
import genesis.familytree.server.modules.common.entity.Param;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

/**
 *
 */
public interface IParamService {

    Param getById(int id);

    Param getByCode(String paramCode);

    /**
     *
     * @param paramCode
     * @return
     */
    String getString(String paramCode);

    /**
     *
     * @param paramCode
     * @param defaultValue
     * @return
     */
    String getString(String paramCode, String defaultValue);

    /**
     *
     * @param paramCode
     * @return
     */
    Double getDouble(String paramCode);

    double getDouble(String paramCode, double defaultValue);

    Integer getInteger(String paramCode);

    int getInteger(String paramCode, int defaultValue);

    Boolean getBoolean(String paramCode);

    boolean getBoolean(String paramCode, boolean defaultValue);

    BigDecimal getBigDecimal(String paramCode);

    BigDecimal getBigDecimal(String paramCode, BigDecimal defaultValue);

    /**
     * 格式 yyyy-MM-dd
     * @param paramCode
     * @return 如果不存在返回null
     *
     * @see #getDate(String, LocalDate)
     */
    LocalDate getDate(String paramCode);

    /**
     * 格式 yyyy-MM-dd
     * @param paramCode
     * @param defaultValue
     * @return 如果不存在返回 defaultValue
     */
    LocalDate getDate(String paramCode, LocalDate defaultValue);

    /**
     * 格式 HH:mm:ss
     * @param paramCode
     * @return 如果不存在返回null
     *
     * @see #getTime(String, LocalTime)
     */
    LocalTime getTime(String paramCode);

    /**
     * 格式 HH:mm:ss
     * @param paramCode
     * @param defaultValue
     * @return 如果不存在返回 defaultValue
     */
    LocalTime getTime(String paramCode, LocalTime defaultValue);

    /**
     * 格式 yyyy-MM-dd HH:mm:ss
     * @param paramCode
     * @return 如果不存在返回null
     *
     * @see #getDateTime(String, LocalDateTime)
     */
    LocalDateTime getDateTime(String paramCode);

    /**
     * 格式 yyyy-MM-dd HH:mm:ss
     * @param paramCode
     * @param defaultValue
     * @return 如果不存在返回 defaultValue
     *
     * @see #getDateTime(String, LocalDateTime)
     */
    LocalDateTime getDateTime(String paramCode, LocalDateTime defaultValue);

    Page<Param> getPage(int pageNumber, int pageSize, Param param);

    void saveOne(Param param) throws GenesisException;

    void deleteOneById(int id);
}
