package genesis.familytree.server.modules.common.exception;

/**
 * Created by Administrator on 2016/7/25.
 */
public class AuthCodeErrorException extends BaseException {

    public AuthCodeErrorException(String errMsg) {
        super();
        this.setErrMessage(errMsg);
    }
}
