package genesis.familytree.server.modules.sys.web;

import genesis.familytree.server.common.JsonResult;
import genesis.familytree.server.common.exception.GenesisException;
import genesis.familytree.server.common.web.BaseAdminController;
import genesis.familytree.server.modules.sys.entity.SysMenu;
import genesis.familytree.server.modules.sys.model.TreeMenu;
import genesis.familytree.server.modules.sys.service.ISysMenuService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * Created by Cheng on 2016/05/17.
 */
@Controller
@RequestMapping("${familytree.admin-path}/menu")
public class SysMenuController extends BaseAdminController {

    @Resource private ISysMenuService sysMenuService;

    /**
     * 后台用户管理主页
     * @return
     */
    @RequiresPermissions("sys:menu:view")
    @RequestMapping("/index")
    public Object index() {
        return "redirect:/" + adminPath + "/list";
    }

    /**
     * 查看后台用户
     * @return
     */
    @RequiresPermissions("sys:menu:view")
    @RequestMapping(value = "/list", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    public Object viewList() {
        return "/modules/sys/SysMenu/SysMenu_list";
    }

    @ResponseBody
    @RequiresPermissions("sys:menu:view")
    @RequestMapping(value = "/list", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getList() {
        JsonResult result = JsonResult.create();
        List<SysMenu> all = this.sysMenuService.getAll();

        Map<Integer, TreeMenu> map = new HashMap<>();

        TreeMenu root = new TreeMenu(new SysMenu(0), "ROOT", 0, true);
        map.put(0, root);

        for (SysMenu menu : all) {
            SysMenu parent = menu.getParent();

            TreeMenu p = map.get(parent.getId());
            if (null == p) {
                continue;
            }

            TreeMenu m = new TreeMenu(menu, menu.getName(), menu.getLevel(), menu.getIsShow() == 1);
            p.addChild(m);

            map.put(m.getId(), m);
        }

        return result.success("data", Arrays.asList(root));
    }

    @RequiresPermissions("sys:menu:create")
    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public Object viewCreate(@RequestParam("pid") int parentId,
                             Model model) {

        SysMenu menu = new SysMenu();
        SysMenu parent = this.sysMenuService.getById(parentId);
        if (null == parent) {
            throw new GenesisException("找不到父菜单");
        }

        menu.setParent(parent);
        model.addAttribute("entity", menu);
        return "/modules/sys/SysMenu/SysMenu_edit";
    }

    @ResponseBody
    @RequiresPermissions("sys:menu:create")
    @RequestMapping(value = "/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object create(SysMenu menu,
                         HttpServletRequest request) {
        JsonResult result = JsonResult.create();

        try {
            if (!checkCaptcha(request, result, false)) {
                return result;
            }

            this.sysMenuService.saveOne(menu);
        } catch (GenesisException ex) {
            result.error(ex.getMessage());
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            result.error();
        }

        return result;
    }

    @RequiresPermissions("sys:menu:edit")
    @RequestMapping(value = "/update", method = RequestMethod.GET)
    public Object viewUpdate(@RequestParam("id") int id,
                             Model model) {

        SysMenu entity = this.sysMenuService.getById(id);

        model.addAttribute("entity", entity);

        return "/modules/sys/SysMenu/SysMenu_edit";
    }

    @ResponseBody
    @RequiresPermissions("sys:menu:edit")
    @RequestMapping(value = "/update", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object update() {
        JsonResult result = JsonResult.create();


        return result;
    }
}
