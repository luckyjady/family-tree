package genesis.familytree.server.modules.sys.web;

import genesis.familytree.server.common.JsonResult;
import genesis.familytree.server.common.persistence.Page;
import genesis.familytree.server.common.web.BaseAdminController;
import genesis.familytree.server.modules.sys.entity.SysUser;
import genesis.familytree.server.modules.sys.service.ISysUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 *
 */
@Controller
@RequestMapping("${familytree.admin-path}/manager")
public class SysUserController extends BaseAdminController {

    @Resource private ISysUserService sysUserService;

    /**
     * 后台用户管理主页
     * @return
     */
    @RequiresPermissions("sys:user:view")
    @RequestMapping("/index")
    public Object index() {
        return "redirect:/" + adminPath + "/user/list";
    }

    /**
     * 查看后台用户
     * @return
     */
    @RequiresPermissions("sys:user:view")
    @RequestMapping(value = "/list", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    public Object viewList() {
        return "/modules/sys/SysUser/SysUser_list";
    }

    @ResponseBody
    @RequiresPermissions("sys:user:view")
    @RequestMapping(value = "/list", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getList(@RequestParam("page") int pageNumber,
                          @RequestParam("rows") int pageSize) {
        JsonResult result = JsonResult.create();

        //int pageSize = Constants.System.DEFAULT_PAGE_SIZE;

        Page<SysUser> list = this.sysUserService.getPage(pageNumber, pageSize, new SysUser());
        result.success("data", list);
        return result;
    }

    @RequiresPermissions("sys:user:create")
    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public Object viewCreate() {
        return "/modules/sys/SysUser/SysUser_edit";
    }

    @ResponseBody
    @RequiresPermissions("sys:user:create")
    @RequestMapping(value = "/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object create() {
        JsonResult result = JsonResult.create();


        return result;
    }

    @RequiresPermissions("sys:user:edit")
    @RequestMapping(value = "/update", method = RequestMethod.GET)
    public Object viewUpdate(@RequestParam("userId") int userId) {


        return "/modules/sys/SysUser/SysUser_edit";
    }

    @ResponseBody
    @RequiresPermissions("sys:user:edit")
    @RequestMapping(value = "/update", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object update() {
        JsonResult result = JsonResult.create();


        return result;
    }
}
