package genesis.familytree.server.modules.common.entity;

import genesis.familytree.server.common.persistence.DataEntity;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

/**
 * 系统参数
 */
public class Param extends DataEntity<Param> {

    /**
     * 参数编码
     */
    @NotBlank
    private String paramCode;

    /**
     * 参数的名字
     */
    @NotBlank
    private String paramName;

    /**
     * 参数值
     */
    @NotNull
    private String paramValue;

    /**
     * 参数分组，方便获取
     */
    private String paramGroup;

    /**
     * 是否系统参数，系统参数只能修改paramValue
     */
    private Integer isSystem;

    public String getParamCode() {
        return paramCode;
    }

    public void setParamCode(String paramCode) {
        this.paramCode = paramCode;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    public String getParamGroup() {
        return paramGroup;
    }

    public void setParamGroup(String paramGroup) {
        this.paramGroup = paramGroup;
    }

    public Integer getIsSystem() {
        return isSystem;
    }

    public void setIsSystem(Integer isSystem) {
        this.isSystem = isSystem;
    }
}
