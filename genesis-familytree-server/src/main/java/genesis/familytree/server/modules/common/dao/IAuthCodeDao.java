package genesis.familytree.server.modules.common.dao;

import genesis.familytree.server.modules.common.entity.PageEntity;
import genesis.familytree.server.modules.home.entity.CommonAuthCode;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * Created by CoderJiang@Sina.com on 2016/7/22.
 */
@Mapper
public interface IAuthCodeDao {

    /**
     * 根据条件查询
     * @param page
     * @return
     */
    List<CommonAuthCode> searchByMobile(PageEntity page);

}
