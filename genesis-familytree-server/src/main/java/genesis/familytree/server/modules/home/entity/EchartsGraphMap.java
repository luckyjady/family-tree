package genesis.familytree.server.modules.home.entity;

import java.util.List;

/**
 * Created by coderjiang@sina.com on 2016/7/27.
 */
public class EchartsGraphMap {

    /**
     * 节点数据
     */
    private List<EchartsGraphData> echartsGraphDatas;

    /**
     * 线条数据
     */
    private List<EchartsGraphLine> echartsGraphLines;

    public List<EchartsGraphData> getEchartsGraphDatas() {
        return echartsGraphDatas;
    }

    public void setEchartsGraphDatas(List<EchartsGraphData> echartsGraphDatas) {
        this.echartsGraphDatas = echartsGraphDatas;
    }

    public List<EchartsGraphLine> getEchartsGraphLines() {
        return echartsGraphLines;
    }

    public void setEchartsGraphLines(List<EchartsGraphLine> echartsGraphLines) {
        this.echartsGraphLines = echartsGraphLines;
    }
}
