package genesis.familytree.server.modules.home.service.impl;

import genesis.familytree.server.common.utils.DevUtils;
import genesis.familytree.server.common.utils.StringUtils;
import genesis.familytree.server.modules.home.entity.*;
import genesis.familytree.server.modules.home.service.IEchartsService;
import genesis.familytree.server.modules.home.service.IHomeRelateService;
import genesis.familytree.server.modules.home.service.IHomeRelateTypeService;
import genesis.familytree.server.modules.home.service.IHomeUserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by coderJiang@Sina.com on 2016/7/26.
 */
@Transactional(readOnly = true)
@Service
public class EchartsServiceImpl implements IEchartsService {

    @Resource
    private IHomeRelateTypeService homeRelateTypeService;

    @Resource
    private IHomeRelateService homeRelateService;

    @Resource
    private IHomeUserService homeUserService;

    @Override
    public EchartsGraphMap getEchartsGraphMapByUser(int userID) {

        EchartsGraphMap echartsGraphMap = new EchartsGraphMap();

        if (DevUtils.isDev()) {
            userID = 1;
        }

        // 获取关系模板
        List<HomeRelateTypeTemplate> homeRelateTemplates = this.homeRelateTypeService.getHomeRelateTypeByUserWithTemplate();

        // 获取用户已有关系
        List<HomeRelate> homeRelates = this.homeRelateService.getAllRelatesByUserID(userID);

        // 比对两个数据，并构建echats节点和线条数据
        buildEchartsData(echartsGraphMap, homeRelateTemplates, homeRelates, userID);

        return echartsGraphMap;
    }

    /**
     * 组装echarts数据节点
     *
     * @param echartsGraphMap
     * @param homeRelateTemplates
     * @param homeRelates
     */
    private void buildEchartsData(EchartsGraphMap echartsGraphMap, List<HomeRelateTypeTemplate> homeRelateTemplates, List<HomeRelate> homeRelates, int userID) {
        // 节点
        List<EchartsGraphData> echartsGraphDatas = new ArrayList<EchartsGraphData>();
        // 线条
        List<EchartsGraphLine> echartsGraphLines = new ArrayList<EchartsGraphLine>();

        EchartsGraphData self = new EchartsGraphData();
        self.setName("自己");
        self.setValue("0");
        echartsGraphDatas.add(self);

        List<HomeRelateType> selfHomeRelate = new ArrayList<HomeRelateType>();


        // 遍历家族模板
        for (HomeRelateTypeTemplate homeRelateTypeTemplate : homeRelateTemplates) {

            // 如果自己有木板关系就加入 selfHomeRelate 数组
            if (this.getRelate(homeRelates, homeRelateTypeTemplate, userID)) {
                selfHomeRelate.add(homeRelateTypeTemplate.getParent());
            }

            // 如果有自己的关系就用自己的关系替换木板的关系名称
            for (HomeRelateType relate : selfHomeRelate) {
                if(homeRelateTypeTemplate.getParent().equals(relate)){
                    homeRelateTypeTemplate.setParent(relate);

                }
                if(homeRelateTypeTemplate.getRelate().equals(relate)){
                    homeRelateTypeTemplate.setRelate(relate);
                }
            }

            echartsGraphDatas.add(new EchartsGraphData(homeRelateTypeTemplate.getParent().getName(), homeRelateTypeTemplate.getParent().getName()));
            echartsGraphLines.add(new EchartsGraphLine(homeRelateTypeTemplate.getRelate().getName(), homeRelateTypeTemplate.getParent().getName()));

        }

        // 遍历模板中不存在的家族关系
        for(HomeRelate relate : homeRelates){
            HomeRelateType homeRelateType = this.homeRelateTypeService.getReactHomeRelateTypeByUserId(userID, relate.getToUserID());
            if(homeRelateType != null && !selfHomeRelate.contains(homeRelateType)){
                // 模板之外的关系
                HomeUser homeuser = this.homeUserService.getHomeUserByUserID(relate.getToUserID());
                String chartName = homeuser.getRealName();
                if(StringUtils.isBlank(chartName)){
                    chartName = homeRelateType.getName();
                }

                String lineSrc = this.homeRelateService.getUpperRelateName(homeRelateType.getChain(), userID);

                echartsGraphDatas.add(new EchartsGraphData(chartName, chartName));
                echartsGraphLines.add(new EchartsGraphLine(lineSrc, chartName));

            }
        }

        echartsGraphMap.setEchartsGraphDatas(echartsGraphDatas);
        echartsGraphMap.setEchartsGraphLines(echartsGraphLines);

    }

    // 根据关系列表
    private boolean getRelate(List<HomeRelate> homeRelates, HomeRelateTypeTemplate homeRelateTypeTemplate, int userID) {
        boolean relateExist = false;

        // 转换目标关系为基础关系元素
        String[] relateChain = homeRelateTypeTemplate.getParent().getChain().split(",");

        HomeRelate curHomeRelate = null;
        int curUserID = userID;
        int matchTimes = 0;
        // 按次序迭代目标关系基础关系元素
        for (String baseRelate : relateChain) {

            // 匹配自己关系是否满足目标关系
            for (HomeRelate homeRelate : homeRelates) {
                if (homeRelate.getFromUserID() == curUserID && homeRelate.getFamilyType().getSign().equals(baseRelate)) {
                    curHomeRelate = homeRelate;
                    curUserID = homeRelate.getToUserID();
                    matchTimes++;
                    break;
                }
            }

        }

        HomeUser homeUser = this.homeUserService.getHomeUserByUserID(curHomeRelate.getToUserID());

        if (relateChain.length == matchTimes) {
            homeRelateTypeTemplate.getParent().setName(homeUser.getRealName());
            relateExist = true;
        }

        return relateExist;
    }
}
