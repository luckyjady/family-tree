package genesis.familytree.server.modules.home.entity;

/**
 * Created by coderjiang@sina.com on 2016/7/28.
 */
public class HomeBaseRelateType {

    /**
     * ID
     */
    private int id;

    /**
     * 关系标记
     */
    private String sign;

    /**
     * 辈份级别
     */
    private int level;

    /**
     * 备注
     */
    private String remark;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
