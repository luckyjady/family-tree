package genesis.familytree.server.modules.home.web;

import genesis.familytree.server.common.JsonResult;
import genesis.familytree.server.common.utils.DevUtils;
import genesis.familytree.server.modules.common.exception.AuthCodeErrorException;
import genesis.familytree.server.modules.home.entity.EchartsGraphData;
import genesis.familytree.server.modules.home.entity.EchartsGraphMap;
import genesis.familytree.server.modules.home.entity.HomeRelateTypeTemplate;
import genesis.familytree.server.modules.home.entity.HomeUser;
import genesis.familytree.server.modules.home.exception.IdCardNumberErrorException;
import genesis.familytree.server.modules.home.service.IEchartsService;
import genesis.familytree.server.modules.home.service.IHomeRelateService;
import genesis.familytree.server.modules.home.service.IHomeRelateTypeService;
import genesis.familytree.server.modules.home.service.IHomeUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 *
 */
@Controller
public class HomeUserController {
    private static final Logger LOG = LoggerFactory.getLogger(HomeUserController.class);

    @Resource
    private IHomeUserService service;

    @Resource
    private IEchartsService echartsService;

    @Resource
    private IHomeRelateService relateService;

    @ResponseBody
    @RequestMapping("/ajax/registerSubmit.json")
    public Object registerSubmit(HttpServletRequest req, HttpSession session, String fullname, String idcard, String mobile, String code, String openID) {

        JsonResult result = JsonResult.create();

        HomeUser homeUser = null;
        try {
            if(DevUtils.isDev()) {
                homeUser = this.service.addHomeUser("coderjiang", "13927441616", fullname, idcard, code, (String) session.getAttribute("smsCode"), "1");
            }else {
                homeUser = this.service.addHomeUser((String) session.getAttribute("nickname"), (String) session.getAttribute("mobile"), fullname, idcard, code, (String) session.getAttribute("smsCode"), (String) session.getAttribute("openID"));
            }
            session.setAttribute("homeUser", homeUser);

            result.success("注册成功");

        } catch (AuthCodeErrorException e) {
            result.error(-1, e.getErrMessage());
            LOG.info(e.getErrMessage(), e);
        } catch (IdCardNumberErrorException e){
            result.error(-2, e.getErrMessage());
            LOG.info(e.getErrMessage(), e);
        }

        return result;

    }

    @ResponseBody
    @RequestMapping("ajax/getHomeMap.json")
    public Object getHomeMap(HttpSession session){

        JsonResult result = JsonResult.create();

        HomeUser homeUser = (HomeUser) session.getAttribute("homeUser");
        EchartsGraphMap echartsGraphMap = null;
        if(DevUtils.isDev()) {;
            echartsGraphMap = this.echartsService.getEchartsGraphMapByUser(1);
        }else{
            echartsGraphMap = this.echartsService.getEchartsGraphMapByUser(homeUser.getId());
        }

        result.success("data", echartsGraphMap.getEchartsGraphDatas());
        result.success("line", echartsGraphMap.getEchartsGraphLines());

        return result;
    }

    @ResponseBody
    @RequestMapping("/ajax/userInfo.json")
    public Object userInfo(HttpSession session){
        HomeUser homeUser = null;
        if(DevUtils.isDev()){
            homeUser = this.service.getHomeUserByUserID(1);
        }else {
            homeUser = this.service.getHomeUserByUserID(((HomeUser) session.getAttribute("homeUser")).getId());
        }
        session.setAttribute("homeUser", homeUser);

        JsonResult result = JsonResult.create();
        result.success("data", homeUser);

        return result;
    }


    @ResponseBody
    @RequestMapping("/ajax/addRelation.json")
    public Object addRelation(HttpSession session, String fullname, String idcard, String mobile, int relationID){
        HomeUser homeUser = null;
        if(DevUtils.isDev()){
            homeUser = this.service.getHomeUserByUserID(1);
        }else {
            homeUser = this.service.getHomeUserByUserID(((HomeUser) session.getAttribute("homeUser")).getId());
        }

        this.relateService.addRelation(homeUser.getId(), fullname, idcard, mobile, relationID);

        JsonResult result = JsonResult.create();

        result.success();

        return result;
    }
}
