package genesis.familytree.server.modules.common.exception;

/**
 * Created by CoderJiang@sina.com
 */
public abstract class BaseException extends Exception {

    private int errorCode = -1;

    private String errMessage = "系统错误请联系管理员";

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrMessage() {
        return errMessage;
    }

    public void setErrMessage(String errMessage) {
        this.errMessage = errMessage;
    }
}
