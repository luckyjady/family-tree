package genesis.familytree.server.modules.home.entity;

/**
 * Created by coderjiang@sina.com on 2016/7/27.
 */

public class EchartsGraphLine {


    /**
     * 线条起点
     */
    private String source;


    /**
     * 线条终点
     */
    private String target;

    public EchartsGraphLine(){

    }
    public EchartsGraphLine(String source, String target) {
        this.source = source;
        this.target = target;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }
}
