/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package genesis.familytree.server.modules.sys.dao;

import genesis.familytree.server.modules.sys.entity.SysRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 角色DAO接口
 *
 * @author ThinkGem
 * @version 2013-12-05
 */
@Mapper
public interface ISysRoleDao {

    SysRole getById(@Param("id") int roleId);

    SysRole getByCode(String code);

    /**
     * 维护角色与菜单权限关系
     */
    int deleteRoleMenuByRole(@Param("roleId") int roleId);

    int saveRoleMenuList(SysRole sysRole);

    List<SysRole> findListByUser(@Param("userId") int userId);

    int deleteById(@Param("id") int roleId);

    List<SysRole> findList(SysRole sysRole);

    int saveOne(SysRole role);

    int updateOne(SysRole sysRole);

}
