package genesis.familytree.server.modules.common.service.impl;

import genesis.familytree.server.common.utils.FormterUtils;
import genesis.familytree.server.common.utils.PropertiesLoader;
import genesis.familytree.server.modules.common.dao.IAuthCodeDao;
import genesis.familytree.server.modules.common.entity.PageEntity;
import genesis.familytree.server.modules.common.exception.ParamNullException;
import genesis.familytree.server.modules.common.exception.AuthCodeErrorException;
import genesis.familytree.server.modules.common.exception.needImageAuthCodeException;
import genesis.familytree.server.modules.common.service.IAuthCodeService;
import genesis.familytree.server.modules.home.entity.CommonAuthCode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2016/7/21.
 */
@Transactional(readOnly = true)
@Service
public class AuthCodeServiceImpl implements IAuthCodeService {


    @Resource
    private IAuthCodeDao dao;

    @Override
    public boolean checkSendMessage(String mobile) throws ParamNullException {

        if (!FormterUtils.isMobileNO(mobile)){
            throw new ParamNullException("手机号码不能符合要求：" + mobile);
        }

        PageEntity page = new PageEntity();
        page.setPage(0);
        page.setSize(5);
        Map<String, String> params = new HashMap<String, String>();
        params.put("mobile", mobile);
        page.setParams(params);
        page.setOrderColumn("ID");
        page.setOrderTurn("DESC");

        List<CommonAuthCode> authCodes = this.dao.searchByMobile(page);

        return calcSendTimes(authCodes);
    }

    @Override
    public String sendSMS(String mobile, Boolean needCode, String code, String sessionImageCode) throws AuthCodeErrorException, ParamNullException, needImageAuthCodeException {

        if (!FormterUtils.isMobileNO(mobile)){
            throw new ParamNullException("手机号码不能符合要求：" + mobile);
        }else if(!code.equals(sessionImageCode) && needCode != null && needCode ){
            throw new AuthCodeErrorException("图形验证码错误");
        }

        PageEntity page = new PageEntity();
        page.setPage(0);
        page.setSize(5);
        Map<String, String> params = new HashMap<String, String>();
        params.put("mobile", mobile);
        page.setParams(params);
        page.setOrderColumn("ID");
        page.setOrderTurn("DESC");

        List<CommonAuthCode> authCodes = this.dao.searchByMobile(page);

        if(calcSendTimes(authCodes) && !code.equals(sessionImageCode)){
            throw new needImageAuthCodeException("需要图形验证码");
        }

        String smsCode = "111111";

        return smsCode;
    }

    /**
     * 根绝发送时间判断是否需要显示验证码
     *
     * @param authCodes
     * @return
     */
    private boolean calcSendTimes(List<CommonAuthCode> authCodes) {

        PropertiesLoader propertiesLoader = new PropertiesLoader("classpath:application.properties");

        int sent = 0;
        for (CommonAuthCode code : authCodes) {
            if (new Date().getTime() - code.getAddTime().getTime() <= propertiesLoader.getInteger("auth.code.sendtime") * 60 * 1000) {
                sent++;
            }
        }

        if (sent >= propertiesLoader.getInteger("auth.code.times")) {
            return true;
        }
        return false;
    }
}
