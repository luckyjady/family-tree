package genesis.familytree.server.common.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Administrator on 2016/7/22.
 */
public class DevUtils {

    public static boolean isDev() {
        PropertiesLoader propertiesLoader = new PropertiesLoader("classpath:application.properties");
        return propertiesLoader.getProperty("famliytree.mode").equals("development");
    }

}
