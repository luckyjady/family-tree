package genesis.familytree.server.common.code;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * 参数集合
 */
public class Params extends HashMap<String, Object> {

    public Params(Map<String, Object> params) {

        Properties properties = System.getProperties();
        for (Object o : properties.keySet()) {
            String key = o.toString();
            if (containsKey(key)) {
                continue;
            }

            Object obj = System.getProperty(key);
            put(key, obj);
        }

        for (String key : params.keySet()) {
            if (containsKey(key)) {
                continue;
            }

            Object obj = params.get(key);

            put(key, obj);
        }

        Map<String, String> getenv = System.getenv();
        for (String key : getenv.keySet()) {
            if (containsKey(key)) {
                continue;
            }

            Object obj = getenv.get(key);

            put(key, obj);
        }

    }

}
