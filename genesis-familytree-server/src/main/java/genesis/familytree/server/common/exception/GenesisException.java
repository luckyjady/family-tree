package genesis.familytree.server.common.exception;

/**
 * Created by Cheng on 2016/05/10.
 */
public class GenesisException extends RuntimeException {

    public GenesisException(String message, Throwable cause) {
        super(message, cause);
    }

    public GenesisException(String message) {
        super(message);
    }
}
