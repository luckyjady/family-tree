package genesis.familytree.server.common.code.builder;

import genesis.familytree.server.common.code.*;
import genesis.familytree.server.common.exception.ExpressionException;

/**
 *
 */
public class EnvExprBuilder implements DataTypeBuilder, DataNameBuilder {


    @Override
    public String build(String name, String format, Params params) throws ExpressionException {
        Object value = params.get(format);
        return value + "";
    }

    @Override
    public Attribute build(Expression expr, int startIndex, int endIndex, String expression) {
        int index = expression.indexOf(":");
        if (index == -1) {
            return null;
        }

        String name = expression.substring(0, index);
        if (!"env".equals(name)) {
            return null;
        }
        String format = expression.substring(index + 1);
        return new Attribute(expr, startIndex, endIndex, "env", "env", format);
    }

}
