package genesis.familytree.server.common;


public class Constants {

    public interface System {
        int DEFAULT_PAGE_SIZE =  15;
    }

    public interface ParamCode {

        /**
         * 后台是否全局开启验证码
         */
        String CAPTCHA_SYSTEM_ENABLED = "CAPTCHA_SYSTEM_ENABLED";

        /**
         * 后台是否全局开启验证码
         */
        String CAPTCHA_HOME_ENABLED = "CAPTCHA_HOME_ENABLED";

        /**
         * 后台登录是否开启验证码
         */
        String CAPTCHA_URL_ADMIN_LOGIN_ENABLED = "CAPTCHA_URL_ADMIN_LOGIN_ENABLED";
    }
}
