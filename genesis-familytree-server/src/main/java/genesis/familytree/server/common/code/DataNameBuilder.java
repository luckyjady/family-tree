package genesis.familytree.server.common.code;


/**
 * 获取表达式
 */
public interface DataNameBuilder {


    /**
     *
     * @param expr
     * @param startIndex
     * @param endIndex
     * @param expression
     * @return
     */
    Attribute build(Expression expr, int startIndex, int endIndex, String expression);

}
