package genesis.familytree.server.common.captcha;

import com.google.code.kaptcha.GimpyEngine;
import com.google.code.kaptcha.NoiseProducer;
import com.google.code.kaptcha.text.WordRenderer;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 *
 */
@ConfigurationProperties(prefix = KaptchaProperties.KAPTCHA_PREFIX)
public class KaptchaProperties {

    public static final String KAPTCHA_PREFIX = "kaptcha";

    private boolean border; // yes or no
    private String borderColor; // 105,179,90

    private int imageWidth; // 100
    private int imageHeight; // 40

    private String textProducerFontColor; // black
    private int textProducerFontSize; // 23
    private String textProducerCharLength; // 5
    private String textProducerCharString; // 0123456789
    private String textProducerFontNames; // 宋体,楷体,微软雅黑
    private Class<GimpyEngine> obscurificatorImpl; // genesis.familytree.server.common.captcha.SimpleShadowGimpy
    private Class<NoiseProducer> noiseImpl; // com.google.code.kaptcha.impl.NoNoise
    private Class<WordRenderer> wordRendererImpl; // genesis.familytree.server.common.captcha.SimpleWordRenderer

    public boolean getBorder() {
        return border;
    }

    public void setBorder(boolean border) {
        this.border = border;
    }

    public String getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(String borderColor) {
        this.borderColor = borderColor;
    }

    public int getImageWidth() {
        return imageWidth;
    }

    public void setImageWidth(int imageWidth) {
        this.imageWidth = imageWidth;
    }

    public int getImageHeight() {
        return imageHeight;
    }

    public void setImageHeight(int imageHeight) {
        this.imageHeight = imageHeight;
    }

    public String getTextProducerFontColor() {
        return textProducerFontColor;
    }

    public void setTextProducerFontColor(String textProducerFontColor) {
        this.textProducerFontColor = textProducerFontColor;
    }

    public int getTextProducerFontSize() {
        return textProducerFontSize;
    }

    public void setTextProducerFontSize(int textProducerFontSize) {
        this.textProducerFontSize = textProducerFontSize;
    }

    public String getTextProducerCharLength() {
        return textProducerCharLength;
    }

    public void setTextProducerCharLength(String textProducerCharLength) {
        this.textProducerCharLength = textProducerCharLength;
    }

    public String getTextProducerCharString() {
        return textProducerCharString;
    }

    public void setTextProducerCharString(String textProducerCharString) {
        this.textProducerCharString = textProducerCharString;
    }

    public String getTextProducerFontNames() {
        return textProducerFontNames;
    }

    public void setTextProducerFontNames(String textProducerFontNames) {
        this.textProducerFontNames = textProducerFontNames;
    }

    public Class<GimpyEngine> getObscurificatorImpl() {
        return obscurificatorImpl;
    }

    public void setObscurificatorImpl(Class<GimpyEngine> obscurificatorImpl) {
        this.obscurificatorImpl = obscurificatorImpl;
    }

    public Class<NoiseProducer> getNoiseImpl() {
        return noiseImpl;
    }

    public void setNoiseImpl(Class<NoiseProducer> noiseImpl) {
        this.noiseImpl = noiseImpl;
    }

    public Class<WordRenderer> getWordRendererImpl() {
        return wordRendererImpl;
    }

    public void setWordRendererImpl(Class<WordRenderer> wordRendererImpl) {
        this.wordRendererImpl = wordRendererImpl;
    }
}
