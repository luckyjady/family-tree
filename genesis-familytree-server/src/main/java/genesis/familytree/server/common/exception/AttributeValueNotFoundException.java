package genesis.familytree.server.common.exception;

/**
 *
 */
public class AttributeValueNotFoundException extends ExpressionException {

    public AttributeValueNotFoundException(String message) {
        super(message);
    }

    public AttributeValueNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

}
