package genesis.familytree.server.common.code.builder;

import genesis.familytree.server.common.code.*;
import genesis.familytree.server.common.exception.ExpressionException;

/**
 *
 */
public class StringExprBuilder extends AbstractDataTypeBuilder implements DataTypeBuilder, DataNameBuilder {


    @Override
    public String build(String name, String format, Params params) throws ExpressionException {
        Object obj = getValue(name, params);
        if (null != obj) {
            return obj.toString();
        }
        return name;
    }


    @Override
    public Attribute build(Expression expr, int startIndex, int endIndex, String expression) {
        return new Attribute(expr, startIndex, endIndex, expression, "string", null);
    }

}
