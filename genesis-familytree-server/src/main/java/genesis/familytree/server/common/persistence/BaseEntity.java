/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package genesis.familytree.server.common.persistence;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.Map;

/**
 * Entity支持类
 *
 * @author ThinkGem
 * @version 2014-05-16
 */
public abstract class BaseEntity<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 实体编号（唯一标识）
     */
    protected Integer id;

    /**
     * 当前实体分页对象
     */
    @JsonIgnore
    protected Page<T> page;

    /**
     * 自定义SQL（SQL标识，SQL内容）
     */
    @JsonIgnore
    protected Map<String, Object> sqlMap;

    public BaseEntity() {

    }

    public BaseEntity(Integer id) {
        this();
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Map<String, Object> getSqlMap() {
        return sqlMap;
    }

    public void setSqlMap(Map<String, Object> sqlMap) {
        this.sqlMap = sqlMap;
    }

    public Page<T> getPage() {
        return page;
    }

    public void setPage(Page<T> page) {
        this.page = page;
    }
}
