package genesis.familytree.server.common.code;

/**
 * 表达式的占位符信息
 */
public class Attribute {
    private Expression expression = null;

    private int startIndex;
    private int endIndex;

    private String code = null;
    private String type = null;
    private String format = null;

    public Attribute(Expression expr,
                     int startIndex, int endIndex,
                     String code, String type, String format) {
        this.expression = expr;
        this.code = code;
        this.type = type;
        this.format = format;
        this.startIndex = startIndex;
        this.endIndex = endIndex;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public int getEndIndex() {
        return endIndex;
    }

    public Expression getExpression() {
        return expression;
    }

    public String getExpr() {
        return expression.getExpression().substring(startIndex, endIndex);
    }

    public String getCode() {
        return code;
    }

    public String getFormat() {
        return format;
    }

    public String getType() {
        return type;
    }
}


