package genesis.familytree.server.common.utils;

import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 */
public class CaptchaUtils {
    public static final String SESSION_KEY_CAPTCHA_HUB = "SESSION_KEY_CAPTCHA_HUB";
    public static final String PARAM_NAME_DEFAULT_CAPTCHA = "_captcha";
    private static final String SESSION_KEY_CAPTCHA_ = "SESSION_KEY_CAPTCHA_";

    public static String[] buildKeysByRequestUrl(String url) {
        String[] keys = url.split("/");
        return clearKeys(keys);
    }

    public static String[] clearKeys(String... keys) {
        return Arrays.asList(keys).stream().filter(e -> !e.isEmpty()).toArray(String[]::new);
    }

    /**
     * 保存验证码到Session中
     *
     * @param session HttpSession
     * @param captcha 验证码
     * @param keys    用于生成SessionKey
     * @see #buildCaptchaKey(String...)
     */
    public static void putCaptchaInSession(HttpSession session, String captcha, String... keys) {
        String key = buildCaptchaKey(keys);

        getHub(session).put(key, captcha);
    }

    /**
     * 从Session中获取到验证码并删除Session的值
     *
     * @param session 用于生成SessionKey
     * @param keys  用于生成SessionKey
     * @return 验证码
     * @see #buildCaptchaKey(String...)
     */
    public static String pollCaptchaInSession(HttpSession session, String... keys) {
        String value = getCaptchaInSession(session, keys);
        putCaptchaInSession(session, null, keys);
        return value;
    }

    /**
     * 获取验证码
     *
     * @param session HttpSession
     * @param keys  用于生成SessionKey
     * @return 验证码
     * @see #buildCaptchaKey(String...)
     */
    public static String getCaptchaInSession(HttpSession session, String... keys) {
        String key = buildCaptchaKey(keys);
        return getHub(session).get(key);
    }

    private static Map<String, String> getHub(HttpSession session) {
        Object obj = session.getAttribute(SESSION_KEY_CAPTCHA_HUB);
        if (null == obj) {
            Map<String, String> map = new TreeMap<String, String>(){
                @Override
                public String put(String key, String value) {
                    if (size() > 100) {
                        clear();
                    }
                    return super.put(key, value);
                }
            };
            session.setAttribute(SESSION_KEY_CAPTCHA_HUB, map);
            return map;
        }

        return (Map) obj;
    }

    /**
     * 生成验证码存放的SessionKey
     *
     * @param keys 用于生成SessionKey
     * @return SessionKey
     */
    private static String buildCaptchaKey(String... keys) {
        return (SESSION_KEY_CAPTCHA_ + StringUtils.join(keys, "_")).toUpperCase();
    }

    /**
     * 获取验证码 并 删除
     *
     * @param session HttpSession
     * @param keys  用于生成SessionKey
     * @return 验证码
     * @see #buildCaptchaKey(String...)
     */
    public static String getAndRemoveCaptchaInSession(HttpSession session, String[] keys) {
        String key = buildCaptchaKey(keys);
        String res = getHub(session).get(key);
        getHub(session).remove(key);
        return res;
    }
}
