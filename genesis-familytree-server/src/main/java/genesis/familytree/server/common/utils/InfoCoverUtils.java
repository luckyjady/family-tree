package genesis.familytree.server.common.utils;

/**
 * Created by coderjiang@sina.com on 2016/8/4.
 *
 * 隐藏部分信息
 */
public class InfoCoverUtils {


    /**
     * 显示第一个字母和最后一个字母
     * @param idCard
     * @return
     */
    public static String coverIdCard(String idCard) {
        char[] res = idCard.toCharArray();
        final char cover = 'X';

        for(int i = 0; i < idCard.length(); i++){
            if(i != 0 && i != idCard.length() -1 ){
                res[i] = cover;
            }
        }
        return new String(res);
    }


    public static String coverMobile(String mobile) {
        char[] res = mobile.toCharArray();
        final char cover = 'X';

        for(int i = 0; i < mobile.length(); i++){
            if(i > 3  && i < mobile.length() -1 ){
                res[i] = cover;
            }
        }
        return new String(res);
    }
}
