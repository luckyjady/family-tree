package genesis.familytree.server.common.web;

import static genesis.familytree.server.common.Constants.*;

/**
 *
 */
public abstract class BaseAdminController extends BaseController {

    @Override
    protected boolean isCaptchaEnabled() {
        return this.paramService.getBoolean(ParamCode.CAPTCHA_SYSTEM_ENABLED, false);
    }
}
