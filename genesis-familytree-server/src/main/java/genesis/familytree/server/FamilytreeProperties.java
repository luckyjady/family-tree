package genesis.familytree.server;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Created by Tailong on 2016/6/15.
 */
@ConfigurationProperties(prefix = FamilytreeProperties.PROP_PREFIX)
public class FamilytreeProperties {

    public static final String PROP_PREFIX = "familytree";

    private String adminPath;

}
