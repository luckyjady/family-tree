package genesis.familytree.server;

import org.springframework.boot.autoconfigure.web.WebMvcAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 *
 */
@EnableTransactionManagement
@EnableWebMvc
@Configuration
@EnableConfigurationProperties(FamilytreeProperties.class)
public class FamilytreeAutoConfiguration extends WebMvcAutoConfiguration.WebMvcAutoConfigurationAdapter {

    @Bean
    public LocalValidatorFactoryBean localValidatorFactoryBean() {
        return new LocalValidatorFactoryBean();
    }
}
