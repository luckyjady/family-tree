//import com.B;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;
import java.security.spec.KeySpec;

/**
 *
 */
@Ignore
public class ClassTest {


//    @Test
//    public void testClass(){
//        new B();
//    }

    @Test
    public void testDesc() throws Exception {
        String pwd = "123456";
        String key = "DtCms";

        byte[] pwdBytes = pwd.getBytes("utf-8");
        byte[] keyBytes = key.getBytes("utf-8");

        String md5 = DigestUtils.md5Hex(keyBytes);
        byte[] ivBytes = md5.toUpperCase().substring(0, 8).getBytes("utf-8");

        KeySpec keySpec = new DESKeySpec(ivBytes);
        SecretKeyFactory factory = SecretKeyFactory.getInstance("DES");
        SecretKey secretKey = factory.generateSecret(keySpec);
        IvParameterSpec iv = new IvParameterSpec(ivBytes);

        Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey, iv);
        byte[] result = cipher.doFinal(pwdBytes);
        String resultString = Hex.encodeHexString(result).toUpperCase();
        Assert.assertEquals("D061F9F1A06E88BF", resultString);
    }
}
