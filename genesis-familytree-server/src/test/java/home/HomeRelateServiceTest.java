package home;

import genesis.familytree.server.Application;
import genesis.familytree.server.common.utils.FormterUtils;
import genesis.familytree.server.modules.common.exception.AuthCodeErrorException;
import genesis.familytree.server.modules.home.entity.HomeRelate;
import genesis.familytree.server.modules.home.entity.HomeUser;
import genesis.familytree.server.modules.home.entity.HomeUserWechat;
import genesis.familytree.server.modules.home.exception.IdCardNumberErrorException;
import genesis.familytree.server.modules.home.service.IHomeRelateService;
import genesis.familytree.server.modules.home.service.IHomeUserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by coderjiang@sina.com on 16-7-13.
 */
@RunWith(SpringJUnit4ClassRunner.class)     //SpringJUnit支持，由此引入Spring-Test框架支持！
@SpringApplicationConfiguration(classes = Application.class)        //指定我们SpringBoot工程的Application启动类
@WebAppConfiguration          //由于是Web项目，Junit需要模拟ServletContext，因此我们需要给我们的测试类加上@WebAppConfiguration
public class HomeRelateServiceTest {

    @Resource
    IHomeRelateService relateService;

    @Test
    public void getRelatesByUser() {

        List<HomeRelate> homeRelateList = relateService.getAllRelatesByUserID(1);

        for(HomeRelate homeRelate : homeRelateList){
            System.out.println(homeRelate.getId());
        }

        Assert.assertNotNull(homeRelateList);
    }

    @Test
    public void getBaseRelate(){

        List<HomeRelate> homeRelates = relateService.getBaseRelate(1, "W");

        Assert.assertEquals(0, homeRelates.size());

    }

    @Test
    public void addBaseRelation(){
        relateService.addRelation(1, "韦明", "", "13800000000", 249);
    }


    @Test
    public void getRelatesFromUser(){
        List<HomeRelate> homeRelateList = relateService.getRelatesFromUser(1);
        for(HomeRelate relate : homeRelateList){
            System.out.println(relate.getRemarks());
        }
        Assert.assertTrue(homeRelateList.size() > 0);
    }

    @Test
    public void getUpperRelateName(){
        String res = this.relateService.getUpperRelateName("F,LB", 1);

        System.out.println(res);
    }

}
