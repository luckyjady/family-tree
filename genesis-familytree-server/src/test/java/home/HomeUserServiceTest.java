package home;

import genesis.familytree.server.Application;
import genesis.familytree.server.common.utils.FormterUtils;
import genesis.familytree.server.modules.common.exception.AuthCodeErrorException;
import genesis.familytree.server.modules.home.entity.HomeUser;
import genesis.familytree.server.modules.home.entity.HomeUserWechat;
import genesis.familytree.server.modules.home.exception.IdCardNumberErrorException;
import genesis.familytree.server.modules.home.service.IHomeUserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;

/**
 * Created by coderjiang@sina.com on 16-7-13.
 */
@RunWith(SpringJUnit4ClassRunner.class)     //SpringJUnit支持，由此引入Spring-Test框架支持！
@SpringApplicationConfiguration(classes = Application.class)        //指定我们SpringBoot工程的Application启动类
@WebAppConfiguration          //由于是Web项目，Junit需要模拟ServletContext，因此我们需要给我们的测试类加上@WebAppConfiguration
public class HomeUserServiceTest {

    @Resource
    IHomeUserService homeUserService;

    @Test
    public void getHomeUserByOpenIDTest() {
        String openID = "abcd";
        HomeUser homeUser = this.homeUserService.getHomeUserByOpenID(openID);
        Assert.assertEquals(homeUser.getNickname(), "coderjiang");
    }

    @Test
    public void getHomeUserWechatByOpenID(){
        String openID = "abcd";
        HomeUserWechat homeUserWechat = this.homeUserService.getHomeUserWechatByOpenID(openID);
        Assert.assertEquals(homeUserWechat.getNickname(), "coderjiang");
    }

    @Test
    public void addHomeUserWechat(){
        HomeUserWechat homeUserWechat = new HomeUserWechat();
        homeUserWechat.setOpenId("aaaa");
        homeUserWechat.setAccessToken("accesstoken");
        homeUserWechat.setExpiresIn(111);
        homeUserWechat.setScope("abcd");
        homeUserWechat.setRefreshToken("1111");
        homeUserWechat.setUnionid("unixxx");
        homeUserWechat.setNickname("11aaa");
        homeUserWechat.setSex(1);
        homeUserWechat.setProvince("吉林");
        homeUserWechat.setCity("长春");
        homeUserWechat.setCountry("中国");
        homeUserWechat.setHeadimgurl("www.baidu.com/adfsaf.dsafds.png");
        homeUserWechat.setPrivilege("aaaa");

        homeUserWechat = this.homeUserService.addHomeUserWechat(homeUserWechat);

        Assert.assertNotEquals(homeUserWechat.getId(), 0);
    }

    @Test
    public void addHomeUser() throws AuthCodeErrorException, IdCardNumberErrorException {

        HomeUser homeUser = this.homeUserService.addHomeUser("coderjiang","13927441616","姜","220","1234","1234","1");

        Assert.assertNotEquals(homeUser.getId(), 0);

    }

    @Test
    public void isIDCard(){
        String idcard = "220104198104174418";
        boolean isIDCard = FormterUtils.isIDCard(idcard);

        Assert.assertTrue(isIDCard);
    }

    @Test
    public void getHomeUserByUserID(){
        int userID = 1;

        HomeUser homeUser = this.homeUserService.getHomeUserByUserID(userID);

        Assert.assertNotNull(homeUser);

    }
}
