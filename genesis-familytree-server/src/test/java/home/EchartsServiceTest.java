package home;

import genesis.familytree.server.Application;
import genesis.familytree.server.modules.home.entity.HomeRelate;
import genesis.familytree.server.modules.home.service.IEchartsService;
import genesis.familytree.server.modules.home.service.IHomeRelateService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by coderjiang@sina.com on 16-7-13.
 */
@RunWith(SpringJUnit4ClassRunner.class)     //SpringJUnit支持，由此引入Spring-Test框架支持！
@SpringApplicationConfiguration(classes = Application.class)        //指定我们SpringBoot工程的Application启动类
@WebAppConfiguration          //由于是Web项目，Junit需要模拟ServletContext，因此我们需要给我们的测试类加上@WebAppConfiguration
public class EchartsServiceTest {

    @Resource
    IEchartsService echartsService;

    @Test
    public void getRelatesByUser() {

        this.echartsService.getEchartsGraphMapByUser(1);

    }
}
