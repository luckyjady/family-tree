package common;

import genesis.familytree.server.Application;
import genesis.familytree.server.modules.common.exception.ParamNullException;
import genesis.familytree.server.modules.common.service.IAuthCodeService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;

/**
 * Created by coderjiang@sina.com on 16-7-13.
 */
@RunWith(SpringJUnit4ClassRunner.class)     //SpringJUnit支持，由此引入Spring-Test框架支持！
@SpringApplicationConfiguration(classes = Application.class)        //指定我们SpringBoot工程的Application启动类
@WebAppConfiguration          //由于是Web项目，Junit需要模拟ServletContext，因此我们需要给我们的测试类加上@WebAppConfiguration
public class AuthCodeServiceTest {

    @Resource
    private IAuthCodeService service;

    @Test
    public void checkSendMessage() throws ParamNullException {
        String mobile = "13927441616";

        boolean res = this.service.checkSendMessage(mobile);

        Assert.assertFalse(res);

    }


}
