#ftree-web

安装各种环境依赖

npm install


启动服务器
npm start

Linux环境没有问题，window下webpack-dev-server无法热加载

目录结构：
根目录-
     |--ajax ajax请求的静态数据

     |--bundle js打包后的存放目录

     |--cdn 一些需要用cdn加速的公共js库

     |--css css样式

     |   |--fonts

     |--node_modules 开发环境依赖的各种框架

     |--src jsx源文件

         |--common 公共组件

         |--pages  页面对应的组件

