/**
 * BY: haole zheng
 * http://passer-by.com
 */
(function(window){
	//简写
	var _filter = [
		/* 表亲 */
		{//表亲的关系
			exp:/^(.+)&o([^#].+)&o/g,
			str:'$1$2&o'
		},
		{//表亲的关系
			exp:/^(.+)&l([^#].+)&l/g,
			str:'$1$2&l'
		},
		{//表亲的关系
			exp:/^(.+)&o(.+)&l/g,
			str:'$1$2'
		},
		{//表亲的关系
			exp:/^(.+)&l(.+)&o/g,
			str:'$1$2'
		},
		/* 父母 */
		{//母亲的丈夫是自己的父亲
			exp:/m,h/g,
			str:'f'
		},
		{//父亲的妻子是自己的母亲
			exp:/f,w/g,
			str:'m'
		},
		{//兄弟的父母就是自己的父母
			exp:/,[xol][sb](,[mf])/g,
			str:'$1'
		},
		/* 父母的子女 */
		{//父母的女儿年龄判断是哥哥还是弟弟
			exp:/,[mf],d&([ol])/,
			str:',$1s'
		},
		{//父母的女儿年龄判断是姐姐还是妹妹
			exp:/,[mf],s&([ol])/,
			str:',$1b'
		},
		{//如果自己是男性,父母的儿子是自己或者兄弟
			con:/(,[fhs]|([olx]b)),[mf],s/,
			exp:/^(.+)?,[mf],s(.+)?$/,
			str:'$1$2#$1,xb$2'
		},
		{//如果自己是女性,父母的女儿是自己或者姐妹
			con:/(,[mwd]|([olx]s)),[mf],d/,
			exp:/^(.+)?,[mf],d(.+)?$/,
			str:'$1$2#$1,xs$2'
		},
		{//如果自己是女性,父母的儿子是自己兄弟
			con:/(,[mwd]|([olx]s)),[mf],s/,
			exp:/,[mf],s/g,
			str:',xb'
		},
		{//如果自己是男性,父母的女儿是自己姐妹
			con:/(,[fhs]|([olx]b)),[mf],d/,
			exp:/,[mf],d/g,
			str:',xs'
		},
		{//父母的儿子是自己或兄妹
			exp:/^,[mf],s(.+)?$/,
			str:'$1#,xb$1'
		},
		{//父母的女儿是自己或者姐妹
			exp:/^,[mf],d(.+)?$/,
			str:'$1#,xs$1'
		},
		/* 兄弟姐妹 */
		{//哥哥姐姐的哥哥姐姐还是自己的哥哥姐姐(年龄判断)
			exp:/,o[sb](,o[sb])/,
			str:'$1'
		},
		{//弟弟妹妹的弟弟妹妹还是自己的弟弟妹妹(年龄判断)
			exp:/,l[sb](,l[sb])/,
			str:'$1'
		},
		{//如果自己是男性,兄弟姐妹的兄弟就是自己的兄弟或自己
			con:/(,[fhs]|([olx]b)),[olx][sb],[olx]b/,
			exp:/^(.+)?,[olx][sb],[olx]b(.+)?$/,
			str:'$1,xb$2#$1$2'
		},
		{//如果自己是女性,兄弟姐妹的姐妹就是自己的姐妹或自己
			con:/(,[mwd]|([olx]s)),[olx][sb],[olx]s/,
			exp:/^(.+)?,[olx][sb],[olx]s(.+)?$/,
			str:'$1,xs$2#$1$2'
		},
		{//如果自己是女性,兄弟姐妹的兄弟就是自己的兄弟
			con:/(,[mwd]|([olx]s)),[olx][sb],[olx]b/,
			exp:/,[olx][sb],[olx]b/g,
			str:',xb'
		},
		{//如果自己是男性,兄弟姐妹的姐妹就是自己的姐妹
			con:/(,[fhs]|([olx]b)),[olx][sb],[olx]s/,
			exp:/,[olx][sb],[olx]s/g,
			str:',xs'
		},
		{//不知道性别，兄弟姐妹的兄弟是自己或兄弟
			exp:/^,[olx][sb],[olx]b(.+)?$/,
			str:',$1#,xb$1'
		},
		{//不知道性别，兄弟姐妹的姐妹是自己或姐妹
			exp:/^,[olx][sb],[olx]s(.+)?$/,
			str:',$1#,xs$1'
		},
		/* 孩子 */
		{//孩子的姐妹是自己的女儿(年龄判断)
			exp:/,[ds]&o,ob/g,
			str:',s&o'
		},
		{//孩子的姐妹是自己的女儿(年龄判断)
			exp:/,[ds]&o,os/g,
			str:',d&o'
		},
		{//孩子的兄弟是自己的儿子(年龄判断)
			exp:/,[ds]&l,lb/g,
			str:',s&l'
		},
		{//孩子的兄弟是自己的儿子(年龄判断)
			exp:/,[ds]&l,ls/g,
			str:',d&l'
		},
		{//孩子的姐妹是自己的女儿
			exp:/,[ds](&[ol])?,[olx]s/g,
			str:',d'
		},
		{//孩子的兄弟是自己的儿子
			exp:/,[ds](&[ol])?,[olx]b/g,
			str:',s'
		},
		/* 夫妻 */
		{//自己是女性，女儿或儿子的妈妈是自己
			exp:/(,[mwd](&[ol])?|([olx]s)),[ds](&[ol])?,m/g,
			str:'$1'
		},
		{//自己是女性，女儿或儿子的爸爸是自己的丈夫
			exp:/(,[mwd](&[ol])?|([olx]s)),[ds](&[ol])?,f/g,
			str:'$1,h'
		},
		{//自己是男性，女儿或儿子的爸爸是自己
			exp:/(,[fhs](&[ol])?|([olx]b)),[ds](&[ol])?,f/g,
			str:'$1'
		},
		{//自己是男性，女儿或儿子的妈妈是自己的妻子
			exp:/(,[fhs](&[ol])?|([olx]b)),[ds](&[ol])?,m/g,
			str:'$1,w'
		},
		{//不知道性别，子女的妈妈是自己或妻子
			exp:/^,[ds],m(.+)?$/,
			str:'$1#,w$1'
		},
		{//不知道性别，子女的爸爸是自己或丈夫
			exp:/^,[ds],f(.+)?$/,
			str:'$1#,h$1'
		},
		{//夫妻的孩子就是自己的孩子
			exp:/,[wh](,[ds])/g,
			str:'$1'
		},
		{//夫妻的对方是自己
			exp:/(,w,h)|(,h,w)/g,
			str:''
		}
	];

	VAR _DATA = {
		'':['自己','我'],
		//外家
		'M,M':['外婆','姥姥'],
		'M,F':['外公','姥爷'],
		'M,M,M':['太姥姥'],
		'M,M,F':['太姥爷'],
		'M,M,XS':['姨姥姥','姨婆'],
		'M,M,XS,W':['姨姥爷'],
		'M,M,XB':['舅姥爷','舅外祖父','舅外公','舅公'],
		'M,M,XB,W':['舅姥姥'],
		'M,F,M':['太姥姥','外太祖父'],
		'M,F,F':['太姥爷','外太祖母'],
		'M,F,XS':['姑姥姥','外太姑母'],
		'M,F,XS,H':['姑姥爷','外太姑父'],
		'M,F,OB':['大姥爷','外伯祖'],
		'M,F,OB,W':['大姥姥','外姆婆'],
		'M,F,LB':['小姥爷','外叔祖'],
		'M,F,LB,W':['小姥姥','外姆婆'],
		'M,F,XB':['XX姥爷','二姥爷','三姥爷'],
		'M,F,XS,S':['表舅'],
		'M,F,XS,S,W':['表舅妈'],
		'M,M,XS,S':['表舅'],
		'M,M,XS,S,W':['表舅妈'],
		'M,M,XB,S':['表舅'],
		'M,M,XB,S,W':['表舅妈'],
		'M,F,XB,S':['堂舅'],
		'M,F,XB,S,W':['堂舅妈'],
		'M,M,XS,D':['表姨'],
		'M,M,XS,D,H':['表姨丈'],
		'M,M,XB,D':['表姨'],
		'M,M,XB,D,H':['表姨丈'],
		'M,F,XS,D':['表姨'],
		'M,F,XS,D,H':['表姨丈'],
		'M,F,XB,D':['堂姨'],
		'M,F,XB,D,H':['堂姨丈'],
		//舅家
		'M,XB':['舅舅','舅父','舅','娘舅','二舅','三舅'],
		'M,XB,W':['舅妈','舅母','妗妗','二舅妈','三舅妈'],
		'M,XB,S&O':['表哥(舅家)','表哥'],
		'M,XB,S&O,W':['表嫂(舅家)','表嫂'],
		'M,XB,S&L':['表弟(舅家)','表弟'],
		'M,XB,S&L,W':['表弟媳(舅家)','表弟媳'],
		'M,XB,S,S':['表侄子'],
		'M,XB,S,D':['表侄女'],
		'M,XB,D&O':['表姐(舅家)','表姐'],
		'M,XB,D&O,H':['表姐夫(舅家)','表姐夫','表姐丈'],
		'M,XB,D&L':['表妹(舅家)','表妹'],
		'M,XB,D&L,H':['表妹夫(舅家)','表妹夫'],
		'M,XB,D,S':['表外甥'],
		'M,XB,D,D':['表外甥女'],
		'M,OB':['大舅'],
		'M,OB,W':['大舅妈'],
		'M,LB':['小舅'],
		'M,LB,W':['小舅妈'],
		//姨家
		'M,XS':['姨妈','姨母','姨姨','姨娘','姨','二姨','三姨'],
		'M,XS,H':['姨丈','姨父','二姨父','三姨父'],
		'M,XS,S&O':['表哥(姨家)','表哥'],
		'M,XS,S&O,W':['表嫂(姨家)','表嫂'],
		'M,XS,S&L':['表弟(姨家)','表弟'],
		'M,XS,S&L,W':['表弟媳(姨家)','表弟媳'],
		'M,XS,S,S':['表侄子'],
		'M,XS,S,D':['表侄女'],
		'M,XS,D&O':['表姐(姨家)','表姐'],
		'M,XS,D&O,H':['表姐夫(姨家)','表姐夫','表姐丈'],
		'M,XS,D&L':['表妹(姨家)','表妹'],
		'M,XS,D&L,H':['表妹夫(姨家)','表妹夫'],
		'M,XS,D,S':['表外甥'],
		'M,XS,D,D':['表外甥女'],
		'M,OS':['大姨','大姨妈'],
		'M,OS,H':['大姨父','大姨丈','大姨夫'],
		'M,LS':['小姨','小姨妈'],
		'M,LS,H':['小姨父','小姨丈','小姨夫'],
		//姑家
		'F,XS':['姑妈','姑母','姑姑','姑'],
		'F,XS,H':['姑丈','姑父'],
		'F,XS,S&O':['表哥(姑家)','表哥'],
		'F,XS,S&O,W':['表嫂(姑家)','表嫂'],
		'F,XS,S&L':['表弟(姑家)','表弟'],
		'F,XS,S&L,W':['表弟媳(姑家)','表弟媳'],
		'F,XS,S,S':['表侄子'],
		'F,XS,S,D':['表侄女'],
		'F,XS,D&O':['表姐(姑家)','表姐'],
		'F,XS,D&O,H':['表姐夫(姑家)','表姐夫','表姐丈'],
		'F,XS,D&L':['表妹(姑家)','表妹'],
		'F,XS,D&L,H':['表妹夫(姑家)','表妹夫'],
		'F,XS,D,S':['表外甥'],
		'F,XS,D,D':['表外甥女'],
		'F,OS':['姑母'],
		'F,LS':['姑姐'],
		//本家
		'F,XB,S&O':['堂哥','堂兄'],
		'F,XB,S&O,W':['堂嫂'],
		'F,XB,S&L':['堂弟'],
		'F,XB,S&L,W':['堂弟媳'],
		'F,XB,S,S':['堂侄子'],
		'F,XB,S,D':['堂侄女'],
		'F,XB,D&O':['堂姐'],
		'F,XB,D&O,H':['堂姐夫'],
		'F,XB,D&L':['堂妹'],
		'F,XB,D&L,H':['堂妹夫'],
		'F,XB,D,S':['堂外甥'],
		'F,XB,D,D':['堂外甥女'],
		'F,OB':['伯父','伯伯','大伯','二伯','三伯'],
		'F,OB,W':['伯母','大娘'],
		'F,LB':['叔叔','叔父','叔','二叔','三叔'],
		'F,LB,W':['婶婶','婶母','婶'],
		'F,F,XS,S&O,':['表伯'],
		'F,F,XS,S&L,':['表叔'],
		'F,F,XS,S,W':['表婶'],
		'F,M,XS,S&O':['表伯'],
		'F,M,XS,S&L':['表叔'],
		'F,M,XS,S,W':['表婶'],
		'F,M,XB,S&O':['表伯'],
		'F,M,XB,S&L':['表叔'],
		'F,M,XB,S,W':['表婶'],
		'F,F,XB,S&O':['堂伯'],
		'F,F,XB,S&L':['堂叔'],
		'F,F,XB,S,W':['堂婶'],
		'F,F,XS,D':['表姑'],
		'F,F,XS,D,H':['表姑丈'],
		'F,M,XS,D':['表姑'],
		'F,M,XS,D,H':['表姑丈'],
		'F,M,XB,D':['表姑'],
		'F,M,XB,D,H':['表姑丈'],
		'F,F,XB,D':['堂姑'],
		'F,F,XB,D,H':['堂姑丈'],
		//岳家
		'W,M':['岳母','丈母娘'],
		'W,F':['岳父','老丈人','丈人','泰山','妻父'],
		'W,F,OB':['伯岳'],
		'W,F,OB,W':['伯岳母'],
		'W,F,LB':['叔岳'],
		'W,F,LB,W':['叔岳母'],
		'W,F,F':['太岳父'],
		'W,F,M':['太岳母'],
		'W,F,F,OB':['太伯岳'],
		'W,F,F,OB,W':['太伯岳母'],
		'W,F,F,LB,':['太叔岳'],
		'W,F,F,LB,W':['太叔岳母'],
		'W,F,F,XB,S&O':['姻伯'],
		'W,F,F,XB,S&O,W':['姻姆'],
		'W,F,F,XB,S&L':['姻叔'],
		'W,F,F,XB,S&L,W':['姻婶'],
		'W,OB':['大舅哥','大舅子','内兄'],
		'W,OB,W':['嫂子','大妗子','内嫂'],
		'W,LB':['小舅子','内弟'],
		'W,LB,W':['弟媳妇','小妗子'],
		'W,F,XB,S&O':['姻家兄'],
		'W,F,XB,S&L':['姻家弟'],
		'W,XB,S':['内侄','妻侄'],
		'W,XB,D':['内侄女','妻侄女'],
		'W,OS':['大姨子','大姨姐','妻姐'],
		'W,OS,H':['大姨夫','襟兄','连襟'],
		'W,LS':['小姨子','小姨姐','妻妹','小妹儿'],
		'W,LS,H':['小姨夫','襟弟','连襟'],
		'W,XS,S':['妻外甥'],
		'W,XS,D':['妻外甥女'],
		//婆家
		'H,M':['婆婆'],
		'H,F':['公公'],
		'H,F,OB':['伯翁'],
		'H,F,OB,W':['伯婆'],
		'H,F,LB':['叔翁'],
		'H,F,LB,W':['叔婆'],
		'H,F,F':['祖翁'],
		'H,F,M':['祖婆'],
		'H,F,F,F':['太公翁'],
		'H,F,F,M':['太奶亲'],
		'H,F,XB,S&O':['堂哥','堂兄'],
		'H,F,XB,S&O,W':['堂嫂'],
		'H,F,XB,S&L':['堂弟'],
		'H,F,XB,S&L,W':['堂小弟'],
		'H,OB':['大伯子','夫兄'],
		'H,OB,W':['大婶子','大伯娘','大嫂','夫兄嫂','妯娌'],
		'H,LB':['小叔子'],
		'H,LB,W':['小婶子','妯娌'],
		'H,XB,S':['侄子'],
		'H,XB,S,W':['侄媳','侄媳妇'],
		'H,XB,S,S':['侄孙','侄孙子'],
		'H,XB,S,S,W':['侄孙媳'],
		'H,XB,S,D':['侄孙女'],
		'H,XB,D':['侄女'],
		'H,XB,D,H':['侄女婿'],
		'H,XB,D,S':['外侄孙'],
		'H,XB,D,D':['外侄孙女'],
		'H,OS':['大姑子','大姑','大娘姑'],
		'H,OS,H':['大姑夫','姊丈'],
		'H,LS':['小姑子','小姑'],
		'H,LS,H':['小姑夫'],
		'H,XS,S':['姨甥'],
		'H,XS,D':['姨甥女'],
		'H,M,XB':['舅公'],
		'H,M,XB,W':['舅婆'],
		'H,M,XS':['姨婆'],
		'H,M,XS,H':['姨公'],
		//旁支
		'XB':['兄弟'],
		'XS':['姐妹'],
		'OB':['哥哥','哥','兄','大哥','大佬'],
		'OB,W':['嫂子','嫂'],
		'OB,W,F':['姻伯父'],
		'OB,W,M':['姻伯母'],
		'LB':['弟弟','弟'],
		'LB,W':['弟妹','弟媳','弟媳妇'],
		'LB,W,F':['姻叔父'],
		'LB,W,M':['姻叔母'],
		'XB,S':['侄子'],
		'XB,S,W':['侄媳','侄媳妇'],
		'XB,S,S':['侄孙','侄孙子'],
		'XB,S,S,W':['侄孙媳'],
		'XB,S,D':['侄孙女'],
		'XB,D':['侄女'],
		'XB,D,H':['侄女婿'],
		'XB,D,S':['外侄孙子'],
		'XB,D,D':['外侄孙女'],
		'OS':['姐姐','姐'],
		'OS,H':['姐夫','姊夫','姊婿'],
		'LS':['妹妹','妹'],
		'LS,H':['妹夫','妹婿'],
		'XS,H,F':['亲家爷','亲爹','亲伯'],
		'XS,H,M':['亲家娘','亲娘'],
		'XS,S':['外甥'],
		'XS,S,W':['外甥媳妇'],
		'XS,S,S':['甥孙'],
		'XS,S,D':['甥孙女'],
		'XS,D':['外甥女','甥女'],
		'XS,D,H':['外甥女婿'],
		'XS,D,S':['甥外孙'],
		'XS,D,S,W':['甥外孙媳妇'],
		'XS,D,D':['甥外孙女'],
		'XS,D,D,H':['甥外孙女婿'],
		//内家
		'F,F,F,F':['高祖父'],
		'F,F,F,M':['高祖母'],
		'F,F,F':['曾祖父'],
		'F,F,M':['曾祖母'],
		'F,M':['奶奶','祖母'],
		'F,F':['爷爷','祖父'],
		'F,F,F':['太爷爷'],
		'F,F,M':['太奶奶'],
		'F,F,XB':['XX爷爷','二爷爷','三爷爷','堂祖父'],
		'F,F,XB,W':['XX奶奶','堂祖母'],
		'F,F,OB':['大爷爷','大爷','堂祖父'],
		'F,F,OB,W':['大奶奶','堂祖母'],
		'F,F,LB':['小爷爷','堂祖父'],
		'F,F,LB,W':['小奶奶','堂祖母'],
		'F,F,XS':['姑奶奶'],
		'F,F,XS,H':['姑爷爷'],
		'F,M,F':['太爷爷'],
		'F,M,M':['太奶奶'],
		'F,M,XS':['姨奶奶','姨婆'],
		'F,M,XS,H':['姨爷爷','姨公','姨爷'],
		'F,M,XB':['舅爷爷','舅公','舅爷','舅祖','舅奶爷','舅祖父'],
		'F,M,XB,W':['舅奶奶','舅婆'],
		//自家
		'M':['妈妈','母亲','老妈','老母','娘','娘亲','妈咪'],
		'F':['爸爸','父亲','老爸','老窦','爹','爹地','老爷子'],
		'W':['老婆','妻子','太太','媳妇','夫人','女人','婆娘','妻','内人','娘子','爱人'],
		'H':['老公','丈夫','先生','官人','男人','汉子','夫','夫君','爱人'],
		'S':['儿子'],
		'S,W':['儿媳妇','儿媳'],
		'S,W,XB':['姻侄'],
		'S,W,XS':['姻侄女'],
		'S,S':['孙子'],
		'S,S,W':['孙媳妇','孙媳'],
		'S,S,S':['曾孙'],
		'S,S,S,W':['曾孙媳妇'],
		'S,S,S,S':['玄孙'],
		'S,S,D':['曾孙女'],
		'S,D':['孙女'],
		'S,D,H':['孙女婿'],
		'S,D,S':['曾外孙'],
		'S,D,D':['曾外孙女'],
		'D':['女儿','千金'],
		'D,H':['女婿'],
		'D,H,XB':['姻侄'],
		'D,H,XS':['姻侄女'],
		'D,S':['外孙'],
		'D,S,W':['外孙媳'],
		'D,S,S':['外曾孙','重外孙'],
		'D,S,D':['外曾孙女','重外孙女'],
		'D,D':['外孙女'],
		'D,D,H':['外孙女婿'],
		'D,D,S':['外曾外孙'],
		'D,D,D':['外曾外孙女'],
		//其他
		'S,W,M':['亲家母'],
		'S,W,F':['亲家公','亲家翁'],
		'S,W,F,OB':['姻兄'],
		'S,W,F,LB':['姻弟'],
		'S,W,F,F':['太姻翁'],
		'S,W,F,F':['太姻姆'],
		'D,H,M':['亲家母'],
		'D,H,F':['亲家公','亲家翁'],
		'D,H,F,OB':['姻兄'],
		'D,H,F,LB':['姻弟'],
		'D,H,F,F':['太姻翁'],
		'D,H,F,F':['太姻姆']
	};

	//数组去重
	var unique = function(arr) {
		var result = [], hash = {};
		for (var i = 0, elem; (elem = arr[i]) != null; i++) {
			if (!hash[elem]) {
				result.push(elem);
				hash[elem] = true;
			}
		}
		return result;
	};

	//分词解析
	function getSelectors(str){
		var lists = str.split('的');
		var result = [];						//所有可能性
		var match = true;
		while(lists.length){
			var name = lists.shift();			//当前匹配词
			var arr = [];						//当前匹配词可能性
			var has = false;
			for(var i in _data){
				var value = _data[i];
				if(value.indexOf(name)>-1){		//是否存在该关系
					if(i||!lists.length){		//对‘我’的优化
						arr.push(i);
					}
					has = true;
				}
			}
			if(!has){
				match = false;
			}
			if(result.length){					//当前匹配词与之前可能性组合
				var res = [];
				for(var i=0;i<result.length;i++){
					for(var j=0;j<arr.length;j++){
						res.push(result[i] +','+arr[j]);
					}
				}
				result = res;
			}else{
				for(var i=0;i<arr.length;i++){
					result.push(','+arr[i]);
				}
			}
		}
		return match?result:[];
	}

	//简化选择器
	function selector2id(selector){
		var result = [];
		var hash = {};
		var sex1 = sex2 = -1;//自己或对方性别:-1未知,0女性,1男性
		if(selector.indexOf(',w')==0){
			sex1 = 1;
		}else if(selector.indexOf(',h')==0){
			sex1 = 0;
		}
		sex2 = selector.match(/,[mw]|([olx]s)|(d(&[ol]))$/)?0:1;
		var getId = function(selector){
			var s='';
			if(!hash[selector]){
				hash[selector] = true;
				var status = true;
				do{
					s = selector;
					for(var i in _filter){
						var item = _filter[i];
						if(item['con']){
							if(selector.match(item['con'])){
								selector = selector.replace(item['exp'],item['str']);
							}
						}else{
							selector = selector.replace(item['exp'],item['str']);
						}
						if(selector.indexOf('#')>-1){
							var arr = selector.split('#');
							for(var i=0;i<arr.length;i++){
								getId(arr[i]);
							}
							status=false;
							break;
						}
					}
					// console.log('selector#',selector);
				}while(s!=selector);
				if(status){
					if(selector==''&&sex1>-1&&sex1!=sex2){
					}else{
						selector = selector.substr(1); 	//去前面逗号
						result.push(selector);
					}
				}
			}
		}
		getId(selector);
		return result;
	}

	//获取数据
	function getDataById(id){
		var result = [];
		var filter = /&[olx]/g;			//忽略属性查找数据
		for(var i in _data){
			if(i.replace(filter,'')==id){
				result.push(_data[i]);
			}
		}
		return result;
	}

	function relationship(str){
		var selectors = getSelectors(str);
		// console.log('selectors#',selectors);
		var result = [];							//匹配结果
		for(var i = 0;i<selectors.length;i++){		//遍历所有可能性
			var ids = selector2id(selectors[i]);
			// console.log('ids#',ids);
			for(var j=0;j<ids.length;j++){
				var id = ids[j];
				if(_data[id]){							//直接匹配称呼
					result.push(_data[id][0]);
				}else{									//高级查找
					var data = getDataById(id);			//忽略属性查找
					if(!data.length){					//当无精确数据时，忽略年龄条件查找
						id = id.replace(/&[ol]/g,'');
						data = getDataById(id);
					}
					if(!data.length){
						id = id.replace(/[ol]/g,'x');
						data = getDataById(id);
					}
					if(!data.length){
						var l = id.replace(/x/g,'l');
						data = getDataById(l);
						var o = id.replace(/x/g,'o');
						data = data.concat(getDataById(o));
					}
					for(var d=0;d<data.length;d++){
						result.push(data[d][0]);
					}
				}
			}
		}
		return unique(result);
	}

	window.relationship = relationship;
})(window);

// console.log(relationship('我的大爷'));
//老公的老婆的儿子的爸爸的老婆的儿子的爸爸
//我的三舅的儿子的爸爸的妹妹的儿子的叔叔的哥哥
//老婆的外孙的姥姥