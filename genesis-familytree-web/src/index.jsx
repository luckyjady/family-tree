var React = require('react');

var ReactDOMRender = require('react-dom').render;

var Router = require('react-router').Router,
    Route = require('react-router').Route,
    Link = require('react-router').Link,
    IndexRoute = require('react-router').IndexRoute,
    hashHistory = require('react-router').hashHistory,
    withRouter = require('react-router').withRouter;

var Container = require('amazeui-touch').Container,
    TabBar = require('amazeui-touch').TabBar;

const App = React.createClass({
    curIndex: 0,
    unreadMessageCntUrl: 'ajax/unreadMessageCnt.json',
    laodUnreadMessageCnt(){
        var t = this;
        $.ajax({
            type: "GET",
            url: this.unreadMessageCntUrl,
            success: function (res) {
                if(res.state == 0){
                    t.setState({
                        unreadMessageCnt: res.value
                    });
                }else{
                    console.log(res);
                }
            },
            error: function (res) {
                console.error(res);
            },
            dataType: 'json'
        });
    },
    handleClick(key, e){
        this.laodUnreadMessageCnt();
        if (this.curIndex > key) {
            this.setState({
                transition: 'sfl'
            });
        } else {
            this.setState({
                transition: 'sfr'
            });
        }
        this.curIndex = key;
    },
    getInitialState() {
        return {
            transition: 'sfr',
            unreadMessageCnt: 0
        };
    },
    componentDidMount(){
        this.laodUnreadMessageCnt();
    },
    getFooter(){
        const {
            router
        } = this.props;
        if(this.props.children.props.withFooter) {
            return (<TabBar
                amStyle="primary"
                onAction={this.handleClick}
                ref="tabBar"
            >
                <TabBar.Item
                    component={Link}
                    icon="home"
                    title="我的家族"
                    selected={router.isActive('/', true)}
                    to="/"
                />
                <TabBar.Item
                    component={Link}
                    icon="person"
                    title="用户中心"
                    selected={router.isActive('/usercenter', true)}
                    to="/usercenter"
                />
                <TabBar.Item
                    component={Link}
                    icon="person"
                    title="注册"
                    selected={router.isActive('/register', true)}
                    to="/register"
                />
                <TabBar.Item
                    component={Link}
                    icon="person"
                    title="邀请家人"
                    selected={router.isActive('/invite', true)}
                    to="/invite"
                />
                <TabBar.Item
                    component={Link}
                    icon="list"
                    title="消息中心"
                    badge={this.state.unreadMessageCnt}
                    selected={router.isActive('/messagecenter', true)}
                    to="/messagecenter"
                />
            </TabBar>);
        }else{
            return "";
        }
    },
    render() {
        const {
            location,
            children
        } = this.props;

        return (
            <Container direction="column" id="sk-container">
                <div className="views">
                    <div className="view" id="app-index" id="app-index">
                        <Container
                            transition={this.state.transition}
                        >
                            {React.cloneElement(children, {key: location.key})}
                        </Container>
                    </div>
                </div>
                {this.getFooter()}
            </Container>
        );
    },
});

document.addEventListener('DOMContentLoaded', () => {
    ReactDOMRender(routes, document.getElementById('root'));
});

const routes = (
    <Router history={hashHistory}>
        <Route path="/" component={withRouter(App)}>
            <IndexRoute getComponent={ (nextState, callback) =>{
                require.ensure( [ ], (require) => {
                    callback(null, require("./pages/HomeCenter").default)
                })
            } }/>
            <Route path="/usercenter" getComponent={ (nextState, callback) =>{
                require.ensure( [ ], (require) => {
                    callback(null, require("./pages/UserCenter").default)
                })
            } }/>
            <Route path="/usercenter/:id" getComponent={ (nextState, callback) =>{
                require.ensure( [ ], (require) => {
                    callback(null, require("./pages/UserCenter").default)
                })
            } }/>
            <Route path="/register" getComponent={ (nextState, callback) =>{
                require.ensure( [ ], (require) => {
                    callback(null, require("./pages/RegisterForm").default)
                })
            } }/>
            <Route path="/invite" getComponent={ (nextState, callback) =>{
                require.ensure( [ ], (require) => {
                    callback(null, require("./pages/Invite").default)
                })
            } }/>
            <Route path="/messagecenter" getComponent={ (nextState, callback) =>{
                require.ensure( [ ], (require) => {
                    callback(null, require("./pages/MessageCenter").default)
                })
            } }/>
            <Route path="/messagecontent/:id" getComponent={ (nextState, callback) =>{
                require.ensure( [ ], (require) => {
                    callback(null, require("./pages/MessageContent").default)
                })
            } }/>
        </Route>
    </Router>
);


