var React = require('react')

var Container = require('amazeui-touch').Container,
    Notification = require('amazeui-touch').Notification;

const NotificationTop = React.createClass({
    message: '这是一个默认信息',
    autoCloseTime: 3000,
    getInitialState() {
        return {
            visible: false,
            amStyle: 'success',
        };
    },

    openNotification() {
        this.setState({
            visible: true,
        });
        setTimeout(this.closeNotification, this.autoCloseTime);
    },

    closeNotification() {
        this.setState({
            visible: false
        });
    },

    render() {
        return (
            <Container {...this.props}>
                <Notification
                    title="测试标题"
                    amStyle={this.state.amStyle}
                    visible={this.state.visible}
                    animated
                    onDismiss={this.closeNotification}
                >
                    {this.message}
                </Notification>
            </Container>
        );
    }
});

export default NotificationTop;