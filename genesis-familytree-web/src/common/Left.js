var React = require('react')

//var Link = require('react-router').Link;

var Container = require('amazeui-touch').Container,
    Group = require('amazeui-touch').Group,
    Button = require('amazeui-touch').Button,
    OffCanvas = require('amazeui-touch').OffCanvas,
    OffCanvasTrigger = require('amazeui-touch').OffCanvasTrigger,
    List = require('amazeui-touch').List;

const Left = React.createClass({
    onDismiss(e) {
        // 从 OffCanvas 内部元素关闭 OffCanvas
        // 紧耦合，需要重构 OffCanvas
        this.refs.oct.close();
    },

    renderOC() {
        return (
            <OffCanvas>
                <div>
                    <h3 className="margin">OffCanvas 内容</h3>
                    <p className="margin-h">在 OffCanvas 内放置 React Router Link 演示</p>
                    <List>
                        <List.Item
                            linkComponent={Link}
                            linkProps={{
                to: `/`,
                onClick: this.onDismiss,
              }}
                            title="组件"
                        />
                        <List.Item
                            linkComponent={Link}
                            linkProps={{
                to: '/about',
                onClick: this.onDismiss,
              }}
                            title="关于"
                        />
                        <List.Item
                            href="https://github.com/amazeui/amazeui-touch"
                            title="GitHub"
                            target="_blank"
                            onClick={this.onDismiss}
                        />
                    </List>
                </div>
            </OffCanvas>
        )
    },

    render() {
        return (
            <Container {...this.props}>

                <Group
                    header="右侧显示 OffCanvas"
                >
                    <OffCanvasTrigger
                        animation="push"
                        pageContainer="#root"
                        offCanvas={<OffCanvas><p>OffCanvas 内容</p></OffCanvas>}
                    >
                        <Button amStyle="primary">显示左侧 Offcanvas</Button>
                    </OffCanvasTrigger>
                </Group>

            </Container>
        );
    }
});

export default Left;