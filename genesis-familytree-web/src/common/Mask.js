var React = require('react')

var Loader = require('amazeui-touch').Loader,
    Container = require('amazeui-touch').Container,
    Modal = require('amazeui-touch').Modal;


const Mask = React.createClass({
    timeId: 0,
    getInitialState() {
        return {
            amStyle: 'primary',
            rounded: false,
            style: {display: 'none'},
            isModalOpen: false,
        };
    },
    openMask(){
        var t = this;
        this.timeId = setTimeout(function () {
            t.setState({
                style: {
                    display: 'block'
                }
            });
            t.openModal();
        }, 300);
    },
    closeMask(){
        clearTimeout(this.timeId);
        this.setState({
            style: {
                display: 'none'
            }
        });
        this.closeModal();
    },
    openModal() {
        this.setState({
            isModalOpen: true,
        })
    },
    closeModal() {
        this.setState({
            isModalOpen: false,
        });
    },
    render() {
        return (
            <Container {...this.props}>
                <Modal
                    isOpen={this.state.isModalOpen}
                    onDismiss={this.closeModal}
                    onOpen={this.onOpen}
                    onClosed={this.onClosed}
                >
                    <div style={this.state.style}>
                        <Loader {...this.state} />
                    </div>
                </Modal>
            </Container>

        );
    }
});

export default Mask;