var React = require('react');

var Container = require('amazeui-touch').Container,
    Modal = require('amazeui-touch').Modal;

const Alert = React.createClass({
    message:'',
    getInitialState() {
        return {
            isModalOpen: false,
        };
    },
    openModal(message) {
        this.message = message;
        this.setState({
            isModalOpen: true,
        })
    },
    closeModal() {
        this.setState({
            isModalOpen: false,
        });
    },
    render() {
        return (
            <Container {...this.props}>
                <Modal
                    isOpen={this.state.isModalOpen}
                    onDismiss={this.closeModal}
                    onOpen={this.onOpen}
                    onClosed={this.onClosed}
                    role="alert"
                >
                    {this.message}
                </Modal>
            </Container>
        );
    }
});

export default Alert;