var React = require('react');

var Container = require('amazeui-touch').Container,
    Modal = require('amazeui-touch').Modal,
    Field = require('amazeui-touch').Field;

const VerifyCodeModal = React.createClass({
    getInitialState() {
        return {
            isModalOpen: false,
            verifyCodeUrl: '/ajax/captcha',
        };
    },
    openModal() {
        this.setState({
            isModalOpen: true,
        })
    },
    closeModal() {
        this.setState({
            isModalOpen: false,
        });
    },
    getCode(){
        return this.state.code;
    },
    handleAction(data) {
        // data 为 true 时为 `确定`
        // `prompt` 类型支持通过返回值控制是否关闭 Modal

        // 点击取消时 data 的值为 null
        if( data == null){
            return;
        }

        // 简单的验证逻辑
        // 仅适用于一个输入框的场景，多个输入框的 data 值为 `['', '', ...]`
        if (data === '') {
            console.error('清输入验证码');
            return false; // 点击确定时不关闭 Modal
        }
        this.props.onSubmit(data);
        this.closeModal();
        return true; // 点击确定时关闭 Modal
    },
    render() {
        return (
            <Container {...this.props}>
                <Modal
                    isOpen={this.state.isModalOpen}
                    onDismiss={this.closeModal}
                    onOpen={this.onOpen}
                    onClosed={this.closeModal}
                    onAction={this.handleAction}
                    role="prompt"
                >
                    <img src={this.state.verifyCodeUrl}/>
                    <Field placeholder="请您输入验证码"/>
                </Modal>
            </Container>
        );
    }
});

export default VerifyCodeModal;