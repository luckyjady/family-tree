var React = require('react');

var Container = require('amazeui-touch').Container,
    Field = require('amazeui-touch').Field;

var $ = require('jquery');

import VerifyCodeModal from "../common/VerifyCodeModal";
import NotificationTop from "../common/NotificationTop";
import Mask from "../common/Mask";
import Alert from "../common/Alert";


const InviteAddForm = React.createClass({
    sendSMSUrl: 'ajax/sendSMS.json',
    checkMobileUrl: 'ajax/checkMobile.json',
    verifyCode: '',
    curMobile: '',
    getDefaultProps() {
        return {
            headerTitle: '注册中心',
            withFooter: true
        };
    },
    getInitialState() {
        return {
            isVerifyCode: true,
        };
    },
    sendSMS() {
        var data = {};
        data.moble = this.refs.mobileInput.getValue();
        data.code = this.verifyCode;
        if (this.state.isVerifyCode) {
            this.refs.verifyCode.openModal();
            return;
        }
        var t = this;
        $.ajax({
            type: "GET",
            url: this.sendSMSUrl,
            data: data,
            success: function (res) {
                if (res.state == 0) {
                    t.refs.notifytop.openNotification();
                } else {
                    console.error(res);
                }
            },
            error: function (res) {
                console.error(res);
            }
        });
    },
    getFormData(){
        return {
            fullname: this.refs.fullnameInput.getValue(),
            idcard: this.refs.idcardInput.getValue(),
            mobile: this.refs.mobileInput.getValue(),
        };
    },
    codeSubmit(data){
        this.verifyCode = data;
        this.state.isVerifyCode = false;
        this.sendSMS();
    },
    render() {
        return (
            <Container {...this.props} ref="form">
                <VerifyCodeModal ref="verifyCode" onSubmit={this.codeSubmit}/>
                <NotificationTop ref="notifytop"/>
                <Alert ref="alert"/>
                <Mask ref="mask" />
                <Field
                    ref="fullnameInput"
                    label="他（她）的全名"
                    placeholder="全名将会显示在族谱中"
                    type="email"/>
                <Field
                    ref="idcardInput"
                    label="他（她）的身份证"
                    placeholder="身份证有助于更好找到亲人（选填）"
                    type="email"
                />
                <Field
                    label="他（她）的手机号码"
                    placeholder="手机号码有助于找到亲人（选填）"
                    ref="mobileInput"
                />
                <Field
                    value="添加"
                    type="submit"
                    amStyle="secondary"
                    block
                    onClick={this.props.submit}
                />
            </Container>

        );
    }
});

export default InviteAddForm;
