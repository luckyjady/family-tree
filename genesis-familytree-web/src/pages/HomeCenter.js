var React = require('react');

var Container = require('amazeui-touch').Container;

var Link = require('react-router').Link;

var $ = require('jquery');

// 引入 ECharts 主模块
var echarts = require('echarts/lib/echarts');

// 引入图
require('echarts/lib/chart/graph');
// 引入提示框
require('echarts/lib/component/tooltip');
// 引入标题组件
require('echarts/lib/component/title');


// 生成临时数据 //
var data1 = [];
var line1 = [];
var random = function (max) {
    return (Math.random() * max).toFixed(3);
};
var curName = null;
for (var i = 0; i < 15; i++) {
    //data1.push([random(15), random(10), random(1)]);
    var name = random(1);

    if (curName != null) {
        var obj = {
            source: curName,
            target: name
        }
        line1.push(obj);
    }

    curName = name;

    var obj = {
        name: name,
        value: random(15)
    };
    data1.push(obj);
}
console.log(data1);
console.log(line1);
// 生成临时数据 //


const UserCenter = React.createClass({
    getDefaultProps() {
        return {
            headerTitle: '家族中心',
            withFooter: true
        };
    },
    activeClickList(){
        $(".container .container-fill a").on("touchstart", function () {
            window.location.href = $(this).attr("href");
        });
    },
    initCharts(){
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('home'));
        myChart.showLoading();

        var t = this;
        myChart.setOption({
            title: {
                show: true,
                text: '我的族谱',
                subtext: '科技改变生活',
                top: 'top',
                left: 'center'
            },
            tooltip: {
                enterable: true,       // 鼠标是否可进入提示框浮层中，默认为false，如需详情内交互，如添加链接，按钮，可设置为 true。
                formatter: function (params, ticket, callback) {
                    $.ajax({
                        type: "GET",
                        url: '/ajax/homeUserInfo.json?name=' + params.name,
                        success: function (res) {
                            callback(ticket, res);
                            t.activeClickList();
                        },
                        error: function (res) {
                            console.error(res);
                        },
                        dataType: 'html'
                    });
                    return 'Loading';
                }
            },
            animation: true,
            series: [
                {
                    name: '家族成员',
                    type: 'graph',
                    symbol: 'circle',
                    layout: 'force',
                    draggable: true,    // 允许鼠标拖动
                    //data: data1,
                    //data: graph.nodes,
                    //categories: categories,
                    //links: line1,
                    roam: true,
                    label: {
                        normal: {
                            position: 'inside',
                            show: true,
                            textStyle: {
                                color: '#000'
                            }
                        },
                        emphasis: {
                            position: 'top',
                            textStyle: {
                                fontWeight: 'bold',
                                color: '#000'
                            }
                        }
                    },
                    itemStyle: {
                        normal: {
                            color: '#fff',
                            borderColor: '#fff',
                            borderWidth: 5
                        },
                        emphasis: {
                            color: '#000',
                            borderColor: '#000',
                            borderWidth: 10,
                            shadowBlur: 10,
                            shadowColor: '#000'
                        }
                    },
                    force: {
                        repulsion: 100
                    }
                }
            ]
        });

        myChart.showLoading();
        $.get('ajax/getHomeMap.json', function (res) {
           myChart.hideLoading();

           myChart.setOption({
               series: [{
                   data: res.data,
                   links: res.line
               }]
           });
        }, 'json');
    },
    getInitialState() {
        return {};
    },
    componentDidMount(){
        this.initCharts();
    },
    render() {
        return (
            <Container {...this.props} className="container-fill" id="home">
            </Container>
        );
    }
});

export default UserCenter;
