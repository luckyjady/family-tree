var React = require('react');

var Container = require('amazeui-touch').Container,
    Card = require('amazeui-touch').Card,
    NavBar = require('amazeui-touch').NavBar,
    Icon = require('amazeui-touch').Icon,
    Field = require('amazeui-touch').Field,
    Group = require('amazeui-touch').Group;

var Link = require('react-router').Link;

var $ = require('jquery');

import Mask from "../common/Mask";
import Alert from "../common/Alert";

const MessageContent = React.createClass({
        messageContentUrl: 'ajax/messageContent.json',
        userFavoriteUrl: 'ajax/userFavoriteUrl.json',
        userCommentUrl: 'ajax/userCommentUrl.json',
        backNav: {
            component: Link,
            icon: 'left-nav',
            title: '返回',
            to: '/messagecenter',
        },
        loadRelationship()
        {
            var t = this;
            var data = {
                id: this.state.messageID
            };
            this.refs.mask.openMask();
            $.ajax({
                type: "GET",
                url: this.messageContentUrl,
                data: data,
                success: function (res) {
                    t.refs.mask.closeMask();
                    if (res.state == 0) {
                        t.setState({
                            messageContent: res.data,
                            title: res.title
                        });
                    } else {
                        t.refs.alert.openModal(res.message);
                    }
                },
                error: function (res, error) {
                    t.refs.mask.closeMask();
                    console.error(res);
                    console.error(error);
                },
                dataType: 'json'
            });
        }
        ,
        getInitialState()
        {
            return {
                messageContent: [],
                messageID: this.props.params.id,
                title: ''
            };
        },
        componentDidMount()
        {
            this.loadRelationship();
        },
        favorite(obj){
            // 获取点击的元素
            var objID = obj.target.id;
            var t = this;
            var data = {};
            data.id = objID;
            this.refs.mask.openMask();
            $.ajax({
                type: "GET",
                data: data,
                url: t.userFavoriteUrl,
                success: function (res) {
                    t.refs.mask.closeMask();
                    if (res.state == 0) {
                        t.loadRelationship();
                    } else {
                        t.refs.alert.openModal(res.message)
                    }
                },
                error: function (res, status, error) {
                    t.refs.mask.closeMask();
                    console.error(res);
                    console.error(status);
                    console.error(error);
                },
                dataType: "json"
            });
        },
        getMessageFooter(message)
        {
            if (message.withReply) {
                return (
                    <Card.Child role="footer">
                        <Icon ref="fav"
                              name={message.isFavorite ? 'star-filled' : 'star'}
                              ref={'message_' + message.id}
                              id={message.id}
                              onClick={this.favorite}/>
                        <a href="#/">Comment</a>
                    </Card.Child>
                );
            } else {
                return (
                    <Card.Child role="footer">
                        <Icon ref="fav"
                              name={message.isFavorite ? 'star-filled' : 'star'}
                              ref={'message_' + message.id}
                              id={message.id}
                              onClick={this.favorite}/>
                    </Card.Child>
                );
            }
        },
        submitComment(){
            var t = this;
            var data = {};
            data.messageID = this.props.params.id;
            data.comment = this.refs.commentField.getValue();
            if(data.comment == ''){
                this.refs.alert.openModal("清填写您的评论");
                return;
            }
            this.refs.mask.openMask();
            $.ajax({
                type: "GET",
                data: data,
                url: t.userCommentUrl,
                success: function (res) {
                    t.refs.mask.closeMask();
                    if (res.state == 0) {
                        t.loadRelationship();
                        t.refs.commentField.getFieldDOMNode().value = '';
                    } else {
                        t.refs.alert.openModal(res.message)
                    }
                },
                error: function (res, status, error) {
                    t.refs.mask.closeMask();
                    console.error(res);
                    console.error(status);
                    console.error(error);
                },
                dataType: "json"
            });
        },
        render()
        {
            return (
                <Container {...this.props} className="container-fill container-scrollable">
                    <Mask ref="mask"/>
                    <Alert ref="alert"/>
                    <NavBar
                        title={this.state.title}
                        leftNav={[this.backNav]}
                        amStyle="primary"
                        className="message-content-header"
                    />
                    {this.state.messageContent.map((message, i) => {
                        return (
                            <Card
                                key={i}
                                title={message.name}
                                footer={this.getMessageFooter(message)}
                            >
                                {message.desc}
                            </Card>
                        );
                    })}
                    <div className="message-content-footerBlock"/>
                    <Group className="message-content-footer">
                        <Field
                            placeholder="清留下您的评论"
                            ref="commentField"
                        />
                        <Field
                            value="评论"
                            type="submit"
                            amStyle="secondary"
                            block
                            onClick={this.submitComment}
                        />
                    </Group>
                </Container>

            );
        }
    })
    ;

export default MessageContent;
