var React = require('react');

var Container = require('amazeui-touch').Container,
    Grid = require('amazeui-touch').Grid,
    Col = require('amazeui-touch').Col,
    Group = require('amazeui-touch').Group,
    Field = require('amazeui-touch').Field,
    Tabs = require('amazeui-touch').Tabs;

var $ = require('jquery');

import Mask from "../common/Mask";
import Alert from "../common/Alert";
import RelationSelect from "./RelationSelect";
import InviteAddForm from "./InviteAddForm";

const Invite = React.createClass({
    submitFormUrl: 'ajax/addRelation.json',
    getDefaultProps() {
        return {
            headerTitle: '邀请家人',
            withFooter: true

        };
    },
    getInitialState() {
        return {
            activeTab: 0
        };
    },
    submitInvestForm(){
        var inviteData = {};
        inviteData = this.refs.inviteAddForm.getFormData();
        inviteData.relationID = this.refs.relationSelect.getRelationData();
        if(inviteData.fullname.length == 0){
            this.refs.alert.openModal('全名不能为空');
            return;
        }
        if(inviteData.relationID.length == 0){
            this.refs.alert.openModal('请选择家人的关系');
            return;
        }

        this.refs.mask.openMask();
        var t = this;
        $.ajax({
            type: "GET",
            url: this.submitFormUrl,
            data: inviteData,
            success: function (res) {
                t.refs.mask.closeMask();
                if (res.state == 0) {
                    location.href = './usercenter.html';
                } else {
                    t.refs.alert.openModal(res.message);
                }
            },
            error: function (res) {
                t.refs.mask.closeMask();
                console.error(res);
            },
            dataType: 'json'
        });

    },
    componentDidMount(){
    },
    render() {
        return (
            <Container {...this.props} ref="form" className="container-fill container-scrollable">
                <Mask ref="mask"/>
                <Alert ref="alert"/>
                <Group>
                    <Tabs
                        defaultActiveKey={this.state.activeTab}
                    >
                        <Tabs.Item
                            title="邀请家人"
                        >
                            <RelationSelect />
                            <Grid>
                                <Col cols={6}>
                                    <Field
                                        value="立刻邀请"
                                        type="submit"
                                        amStyle="secondary"
                                        block
                                        onClick={this.submitInvestForm}
                                    />
                                </Col>
                            </Grid>
                        </Tabs.Item>
                        <Tabs.Item
                            title="添加家人"
                        >
                            <RelationSelect ref="relationSelect" />
                            <InviteAddForm ref="inviteAddForm" submit={this.submitInvestForm} />
                        </Tabs.Item>
                    </Tabs>
                </Group>
            </Container>
        );
    }
});

export default Invite;
