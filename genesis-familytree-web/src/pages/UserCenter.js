var React = require('react');

var Container = require('amazeui-touch').Container,
    Grid = require('amazeui-touch').Grid,
    Col = require('amazeui-touch').Col,
    Group = require('amazeui-touch').Group,
    List = require('amazeui-touch').List;

var $ = require('jquery');

import Mask from "../common/Mask";
import Alert from "../common/Alert";


function handleSwitch() {
    console.log(this.refs.field.checked);
}
const UserCenter = React.createClass({
    userInfoUrl: 'ajax/userInfo.json',
    params: {
        uid: 1
    },
    fields: [{
        label: '姓名',
        key: 'realName',
    }, {
        label: '性别',
        key: 'homeUserWechat.sex',
    }, {
        label: '出生日期',
        key: 'birthday',
    }, {
        label: '电话',
        key: 'mobile',
    }, {
        label: '省',
        key: 'province',
    }, {
        label: '市',
        key: 'city',
    }],
    getDefaultProps() {
        return {
            headerTitle: '用户中心',
            withFooter: true
        };
    },
    loadData() {
        var t = this;
        var data = {}
        if(this.props.params.id){
            data.uid = this.props.params.id;
        }
        $.ajax({
            type: "GET",
            url: this.userInfoUrl,
            data: data,
            success: function (res) {
                if (res.code == 0) {
                    switch(res.data.gender){
                        case 0:
                            res.data.gender = '保密';
                            break;
                        case 1:
                            res.data.gender = '男';
                            break;
                        case 2:
                            res.data.gender = '女';
                            break;
                    }
                    t.setState({
                        userInfo: res.data,
                        homeUserWechat: res.data.homeUserWechat,
                    });
                } else {
                    t.refs.alert.openModal(res.message)
                }
            },
            error: function (res, status, error) {
                console.error(res);
                console.error(status);
                console.error(error);
            },
            dataType: "json",
        });
    },
    getInitialState() {
        this.loadData();
        return {
            isVerifyCode: true,
            userInfo: {},
            homeUserWechat: {}
        };
    },
    listClick(obj) {
        console.log(obj);
    },
    render() {
        return (
            <Container {...this.props} className="container-fill container-scrollable">
                <Alert ref="alert"/>
                <Mask ref="mask"/>
                <Group>
                    <Grid align="center">
                        <Col cols={2}>
                            <img src="http://demo.sc.chinaz.com/Files/pic/icons/5264/11.png"/>
                        </Col>
                    </Grid>
                    <Group>
                        <List>
                            <List.Item
                                title="姓名"
                                href="#"
                                onClick={this.listClick}
                                after={this.state.userInfo.realName}
                                nested="input"
                            >
                            </List.Item>

                            <List.Item
                                title="性别"
                                href="#"
                                onClick={this.listClick}
                                after={this.state.userInfo.gender}
                                nested="input"
                            >
                            </List.Item>

                            <List.Item
                                title="电话"
                                href="#"
                                onClick={this.listClick}
                                after={this.state.userInfo.mobile}
                                nested="input"
                            >
                            </List.Item>

                            <List.Item
                                title="证件"
                                href="#"
                                onClick={this.listClick}
                                after={this.state.userInfo.idCard}
                                nested="input"
                            >
                            </List.Item>

                            <List.Item
                                title="市"
                                href="#"
                                onClick={this.listClick}
                                after={this.state.homeUserWechat.city}
                                nested="input"
                            >
                            </List.Item>

                            <List.Item
                                title="省"
                                href="#"
                                onClick={this.listClick}
                                after={this.state.homeUserWechat.province}
                                nested="input"
                            >
                            </List.Item>

                            <List.Item
                                title="国家"
                                href="#"
                                onClick={this.listClick}
                                after={this.state.homeUserWechat.country}
                                nested="input"
                            >
                            </List.Item>

                        </List>
                    </Group>
                </Group>
            </Container>

        );
    }
});

export default UserCenter;
