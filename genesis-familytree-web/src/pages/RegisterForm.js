var React = require('react');

var Container = require('amazeui-touch').Container,
    Grid = require('amazeui-touch').Grid,
    Col = require('amazeui-touch').Col,
    Group = require('amazeui-touch').Group,
    Field = require('amazeui-touch').Field,
    Button = require('amazeui-touch').Button;

var $ = require('jquery');

import VerifyCodeModal from "../common/VerifyCodeModal";
import NotificationTop from "../common/NotificationTop";
import Mask from "../common/Mask";
import Alert from "../common/Alert";


const RegisterForm = React.createClass({
    sendSMSUrl: 'ajax/sendSMS.json',
    checkMobileUrl: 'ajax/checkMobile.json',
    submitFormUrl: 'ajax/registerSubmit.json',
    verifyCode: '',
    curMobile: '',
    getDefaultProps() {
        return {
            headerTitle: '注册中心',
            withFooter: true
        };
    },
    getInitialState() {
        return {
            isVerifyCode: false
        };
    },
    sendSMS() {
        var data = {};
        data.mobile = this.refs.mobileInput.getValue();
        data.code = this.verifyCode;

        var t = this;
        $.ajax({
            type: "GET",
            url: this.sendSMSUrl,
            data: data,
            success: function (res) {
                if (res.code == -3) {
                    t.refs.verifyCode.openModal();
                } else if (res.code == -2) {
                    t.refs.alert.openModal(res.message);
                } else if (res.code == -1) {
                    t.refs.verifyCode.openModal();
                } else if (res.code == 0) {
                    t.refs.alert.openModal(res.message);
                } else {
                    console.error(res);
                }
            },
            error: function (res) {
                console.error(res);
            }
        });
    },
    submitForm(){
        var t = this;
        var data = {
            fullname: this.refs.fullnameInput.getValue(),
            idcard: this.refs.idcardInput.getValue(),
            mobile: this.refs.mobileInput.getValue(),
            code: this.refs.codeInput.getValue()
        };
        this.refs.mask.openMask();
        $.ajax({
            type: "GET",
            url: this.submitFormUrl,
            data: data,
            success: function (res) {
                t.refs.mask.closeMask();
                if (res.code == 0) {
                    location.href = './index';
                } else {
                    t.refs.alert.openModal(res.message);
                }
            },
            error: function (res) {
                t.refs.mask.closeMask();
                console.error(res);
            },
            dataType: 'json'
        });
    },
    codeSubmit(data){
        this.verifyCode = data;
        this.setState({
            isVerifyCode: false,
            isMobileChecked: true
        });
        this.sendSMS();
    },
    render() {
        return (
            <Container {...this.props} ref="form" className="container-fill container-scrollable">
                <VerifyCodeModal ref="verifyCode" onSubmit={this.codeSubmit}/>
                <NotificationTop ref="notifytop"/>
                <Alert ref="alert"/>
                <Group>
                    <Grid>
                        <Col cols={6}>
                            <Field
                                ref="fullnameInput"
                                labelBefore="您的全名"
                                placeholder="全名将会显示在族谱中"
                                type="email"/>
                        </Col>
                    </Grid>
                    <Grid>
                        <Col cols={6}>
                            <Field
                                ref="idcardInput"
                                labelBefore="身份证"
                                placeholder="您的身份证号码"
                                type="email"
                            />
                        </Col>
                    </Grid>
                    <Grid>
                        <Col cols={6}>
                            <Field
                                labelBefore="手机号"
                                placeholder="清输入您的手机号码"
                                //onBlur={this.checkMobile}
                                ref="mobileInput"
                                btnAfter={
                                    <Button
                                        onClick={this.sendSMS}
                                        amStyle="primary"
                                    >发送
                                    </Button>
                                }
                            />
                        </Col>
                    </Grid>

                    <Grid>
                        <Col cols={6}>
                            <Field
                                ref="codeInput"
                                labelBefore="验证码"
                                placeholder="短信验证码"
                            />
                        </Col>
                    </Grid>
                    <Mask ref="mask"/>
                    <Grid>
                        <Col cols={6}>
                            <Field
                                value="提交"
                                type="submit"
                                amStyle="secondary"
                                block
                                onClick={this.submitForm}
                            />
                        </Col>
                    </Grid>
                </Group>
            </Container>

        );
    }
});

export default RegisterForm;
