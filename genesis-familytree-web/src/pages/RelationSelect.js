var React = require('react');

var Container = require('amazeui-touch').Container,
    Grid = require('amazeui-touch').Grid,
    Col = require('amazeui-touch').Col,
    Field = require('amazeui-touch').Field;

var $ = require('jquery');

import Mask from "../common/Mask";
import Alert from "../common/Alert";

const RelationSelect = React.createClass({
    relationUrl: 'ajax/relationtype.json',
    relationSubUrl: 'ajax/relationSubType.json',
    getDefaultProps() {
        return {
            headerTitle: '邀请家人',
            withFooter: true
        };
    },
    loadRelationship() {
        this.refs.mask.openMask();
        var t = this;
        $.ajax({
            type: "GET",
            url: this.relationUrl,
            success: function (res) {
                t.refs.mask.closeMask();
                if (res.code == 0) {
                    t.setState({
                        relationSelect: res.data
                    });
                } else {
                    t.refs.alert.openModal(res.message);
                }
            },
            error: function (res) {
                t.refs.mask.closeMask();
                console.error(res);
            },
            dataType: 'json'
        });
    },
    getInitialState() {
        return {
            relationSelect: [],
            relationSubSelect: []
        };
    },
    componentDidMount(){
        this.loadRelationship();
    },
    relationSelectChange(){
        this.refs.mask.openMask();
        var t = this;
        var data = {};
        data.id = this.refs.relationSelect.getValue();
        $.ajax({
            type: "GET",
            data: data,
            url: this.relationSubUrl,
            success: function (res) {
                t.refs.mask.closeMask();
                if (res.code == 0) {
                    t.setState({
                        relationSubSelect: res.data,
                    });
                } else {
                    t.refs.alert.openModal(res.message);
                }
            },
            error: function (res) {
                t.refs.mask.closeMask();
                console.error(res);
            },
            dataType: 'json'
        });
    },
    getRelationData(){
        return this.refs.relationSubSelect.getValue()
    },
    render() {
        return (
            <Container {...this.props} ref="form">
                <Mask ref="mask"/>
                <Alert ref="alert"/>
                <Field
                    type="select"
                    label="家人类别"
                    onChange={this.relationSelectChange}
                    ref="relationSelect"
                >
                    {this.state.relationSelect.map((select, i) => {
                        return (
                            <option
                                key={i}
                                value={select.dictValue}
                            >
                                {select.dictLabel}
                            </option>
                        );
                    })}
                </Field>

                <Field
                    type="select"
                    label="家人的关系"
                    ref="relationSubSelect"
                >
                    {this.state.relationSubSelect.map((select, i) => {
                        return (
                            <option
                                key={i}
                                value={select.id}
                            >
                                {select.name}
                            </option>
                        );
                    })}
                </Field>
            </Container>

        );
    }
});

export default RelationSelect;
