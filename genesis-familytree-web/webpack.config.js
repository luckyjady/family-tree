var config = {
    entry: {
        'index': './src/index.jsx',
    },
    output: {
        path: './static/amazeui/js/',
        publicPath: "http://192.168.3.46:7777/static/amazeui/js/",
        //publicPath: "/static/amazeui/js/",
        filename: '[name].bundle.js',
    },

    devServer: {
        headers: { "Access-Control-Allow-Origin": "*" },
        inline: true,
        port: 7777,
    },

    module: {
        loaders: [{
            test: /\.jsx?$/,
            exclude: /node_modules/,
            loader: 'babel',
            query: {
                presets: ['es2015', 'react']
            }
        },{
            test: /\.json?$/,
            loader: 'url-loader?mimetype=application/json',
        }]
    },

    externals: {
        "react": "React",
        "react-dom": "ReactDOM",
        "amazeui-touch": "AMUITouch",
        "jquery": "window.jQuery",
    }

}

module.exports = config;
